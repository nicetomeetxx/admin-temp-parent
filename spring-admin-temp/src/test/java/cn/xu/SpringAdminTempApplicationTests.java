package cn.xu;

import cn.dev33.satoken.secure.BCrypt;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SpringAdminTempApplicationTests {

	@Test
	void test() {
		String hashpw = BCrypt.hashpw("123456");
		System.out.println(hashpw);
	}

	public static void main(String[] args) {
		String hashpw = BCrypt.hashpw("123456");
		System.out.println(hashpw);
	}

}
