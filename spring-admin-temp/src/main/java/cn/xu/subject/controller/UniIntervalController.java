package cn.xu.subject.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.config.utils.UserUtils;
import cn.xu.subject.entity.IntervalHis;
import cn.xu.subject.entity.ProSubject;
import cn.xu.subject.entity.UniInterval;
import cn.xu.subject.service.IIntervalHisService;
import cn.xu.subject.service.IProSubjectService;
import cn.xu.subject.service.IUniIntervalService;
import cn.xu.system.entity.SysUser;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 预约管理
 */
@RestController
@RequestMapping("/interval")
@RequiredArgsConstructor
public class UniIntervalController {

    private final IUniIntervalService iUniIntervalService;
    private final IIntervalHisService iIntervalHisService;
    private final IProSubjectService iProSubjectService;

    /**
     * 查询预约时间区间列表
     */
    @GetMapping("/page")
    public R<List<UniInterval>> list() {
        return R.ok(this.iUniIntervalService.list());
    }

    /**
     * 查询时间详情
     */
    @GetMapping("/{id}")
    public R<UniInterval> getById(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return R.ok(this.iUniIntervalService.getById(id));
    }

    /**
     * 新增预约时间区间
     */
    @Log(title = "预约时间区间", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Boolean> add(@RequestBody UniInterval entity) {
        UniInterval one = iUniIntervalService.getOne(new LambdaQueryWrapper<UniInterval>().eq(UniInterval::getTimeInterval, entity.getTimeInterval()));
        if (ObjectUtils.isNotEmpty(one)) {
            return R.ok("数据已存在");
        }
        return R.ok(iUniIntervalService.save(entity));
    }

    /**
     * 修改预约时间区间
     */
    @Log(title = "预约时间区间", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Boolean> edit(@RequestBody UniInterval entity) {
        UniInterval one = iUniIntervalService.getOne(new LambdaQueryWrapper<UniInterval>()
                .eq(UniInterval::getTimeInterval, entity.getTimeInterval()).ne(UniInterval::getId, entity.getId()));
        if (ObjectUtils.isNotEmpty(one)) {
            return R.error("数据已存在");
        }
        return R.ok(iUniIntervalService.updateById(entity));
    }

    /**
     * 删除预约时间区间
     *
     * @param idList 主键串
     */
    @Log(title = "预约时间区间", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        //判断是否时间区间下有预约
        Long id = idList.get(0);
        UniInterval interval = iUniIntervalService.getById(id);
        iIntervalHisService.remove(new LambdaQueryWrapper<IntervalHis>().eq(IntervalHis::getTimeInterval, interval.getTimeInterval()));
        return R.ok(iUniIntervalService.removeByIds(idList));
    }

    /**
     * 根据商品
     *
     * @return
     */
    @PostMapping("/history")
    public R<Page<IntervalHis>> getAppointPage(@RequestBody PageParam<IntervalHis> params) {
        IntervalHis history = params.getParam();
        LambdaQueryWrapper<IntervalHis> wrapper = new LambdaQueryWrapper<IntervalHis>()
                .like(ObjectUtils.isNotEmpty(history.getTitle()), IntervalHis::getTitle, history.getTitle())
                .eq(ObjectUtils.isNotEmpty(history.getAppDay()), IntervalHis::getAppDay, history.getAppDay())
                .eq(ObjectUtils.isNotEmpty(history.getTimeInterval()), IntervalHis::getTimeInterval, history.getTimeInterval())
                .like(ObjectUtils.isNotEmpty(history.getCreateUser()), IntervalHis::getCreateUser, history.getCreateUser())
                .orderBy(true, false, IntervalHis::getCreateTime);
        return R.ok(iIntervalHisService.page(params.build(), wrapper));
    }

    /**
     * 查询预约时间区间列表
     */
    @GetMapping("/wx/list")
    public R<List<UniInterval>> getIntervalList() {
        return R.ok(this.iUniIntervalService.list());
    }

    /**
     * 根据商品id和选择日期查询已被预约列表,页面日期置灰（zpaging分页）
     *
     * @return
     */
    @PostMapping("/wx/history/list")
    public R<List<IntervalHis>> getAppointList(@RequestBody IntervalHis history) {
        LambdaQueryWrapper<IntervalHis> wrapper = new LambdaQueryWrapper<IntervalHis>()
                .eq(IntervalHis::getSubjectId, history.getSubjectId())
                .eq(IntervalHis::getAppDay, history.getAppDay())
                .ne(IntervalHis::getAppointStatus, WxConstants.APPOINT_CAN);
        return R.ok(iIntervalHisService.list(wrapper));
    }


    /**
     * 小程序我的里查询我的预约列表
     *
     * @return
     */
    @PostMapping("/wx/mine")
    public R<Page<IntervalHis>> getAppointMine(@RequestBody PageParam<IntervalHis> params) {
        return R.ok(iIntervalHisService.pageList(params.build(), params.getParam()));
    }

    /**
     * 根据subjectId和appDay查询我的预约
     *
     * @return
     */
    @PostMapping("/wx/mineDay")
    public R getMineDayApp(@RequestBody IntervalHis history) {
        LambdaQueryWrapper<IntervalHis> wrapper = new LambdaQueryWrapper<IntervalHis>()
                .eq(IntervalHis::getUserCode, LoginUserUtils.getInstance().getUsername())
                .eq(IntervalHis::getAppDay, history.getAppDay())
                .ne(IntervalHis::getAppointStatus, WxConstants.APPOINT_CAN);
        List<IntervalHis> list = iIntervalHisService.list(wrapper);
        return R.ok(list);
    }

    /**
     * 根据商品id预约数据
     *
     * @return
     */
    @PostMapping("/wx/appoint")
    public R wxAppoint(@RequestBody IntervalHis history) {
        ProSubject subject = iProSubjectService.getById(history.getSubjectId());
        history.setTitle(subject.getTitle());
        SysUser sysUser = UserUtils.selectUserIdByUserName(LoginUserUtils.getInstance().getUsername());
        history.setCreateUser(sysUser.getName());
        history.setUserCode(sysUser.getUsername());
        iIntervalHisService.checkAppoint(history, sysUser);
        history.setAppointStatus(WxConstants.APPOINT_HAS);
        history.setCreateTime(new Date());
        history.insert();
        return R.ok();
    }

    /**
     * 根据预约id取消预约该数据
     *
     * @return
     */
    @GetMapping("/wx/cancel")
    public R cancelAppoint(@RequestParam Long id) {
        //判断取消人是不是申请人
        IntervalHis history = iIntervalHisService.getById(id);
        String username = LoginUserUtils.getInstance().getUsername();
        if (!StringUtils.equals(username, history.getUserCode())) {
            return R.error("无权限操作");
        }
        //设置为取消状态
        history.setAppointStatus(WxConstants.APPOINT_CAN);
        history.updateById();
        return R.ok();
    }

    /**
     * 根据预约id完成预约该数据
     *
     * @return
     */
    @GetMapping("/wx/complete")
    public R completeAppoint(@RequestParam Long id) {
        //判断完成人是不是申请人
        IntervalHis history = iIntervalHisService.getById(id);
        //设置为完成状态
        history.setAppointStatus(WxConstants.APPOINT_END);
        history.updateById();
        return R.ok();
    }
}
