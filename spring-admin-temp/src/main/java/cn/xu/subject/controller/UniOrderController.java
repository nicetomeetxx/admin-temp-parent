package cn.xu.subject.controller;

import cn.hutool.core.util.IdUtil;
import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.subject.entity.UniOrder;
import cn.xu.subject.entity.vo.WxOrderDotVo;
import cn.xu.subject.service.IUniOrderService;
import cn.xu.system.service.impl.SysAuthServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 这里是支付订单购买
 */
@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class UniOrderController {
    private final SysAuthServiceImpl stpInterface;
    private final IUniOrderService orderService;

    /**
     * 生成订单
     *
     * @param order
     * @return
     */
    @PostMapping("create")
    public R createOrder(@RequestBody UniOrder order) {
        //查询是否有相似订单
        LambdaQueryWrapper<UniOrder> wrapper = new LambdaQueryWrapper<UniOrder>().eq(UniOrder::getUsername, LoginUserUtils.getInstance().getUsername())
                .eq(UniOrder::getSubjectId, order.getSubjectId()).eq(UniOrder::getOrderStatus, WxConstants.TO_BE_PAID);
        List<UniOrder> one = orderService.list(wrapper);
        if (CollectionUtils.isNotEmpty(one)) {
            return R.error("订单已存在");
        }
        //1.生成订单编号
        order.setOrderNo(IdUtil.getSnowflakeNextIdStr()).setOrderStatus(WxConstants.TO_BE_PAID)
                .setUsername(LoginUserUtils.getInstance().getUsername()).setCreateTime(new Date());
        //2.新增到数据库
        order.insert();
        //3.根据环境判断是否需要调用IJpay
        //4.调用iJpay生成orderInfo
        //5.组装参数返回给前端
        return R.ok(order);
    }


    /**
     * 支付成功回调
     * 小程序直接调用微信支付接口付款成功的回调，来更新数据库里订单状态
     *
     * @param order
     * @return
     */
    @RequestMapping("pay")
    public R pay(@RequestBody UniOrder order) {
        order.setOrderStatus(WxConstants.TO_BE_SHIPPED);
        order.setPayTime(new Date());
        order.updateById();
        //修改subject售卖数量
        return R.ok();
    }


    /**
     * 取消订单
     *
     * @param order
     * @return
     */
    @RequestMapping("cancel")
    public R cancel(@RequestBody UniOrder order) {
        order.setOrderStatus(WxConstants.ORDER_CANCEL);
        order.setEndTime(new Date());
        order.updateById();
        return R.ok();
    }


    /**
     * 后台设置发货状态
     *
     * @param order
     * @return
     */
    @RequestMapping("shipped")
    public R toShipped(@RequestBody UniOrder order) {
        order.setOrderStatus(WxConstants.TO_GET_SHIPPED);
        order.setSendTime(new Date());
        order.updateById();
        return R.ok();
    }

    /**
     * 前端收货待评价
     *
     * @param order
     * @return
     */
    @RequestMapping("evaluated")
    public R toEvaluated(@RequestBody UniOrder order) {
        order.setOrderStatus(WxConstants.TO_BE_EVALUATED);
        order.setShippedTime(new Date());
        order.updateById();
        return R.ok();
    }

    /**
     * 评价，订单完成
     *
     * @param order
     * @return
     */
    @RequestMapping("completed")
    public R completed(@RequestBody UniOrder order) {
        order.setOrderStatus(WxConstants.ORDER_COMPLETED);
        order.setEvaluatedTime(new Date());
        order.setEndTime(new Date());
        //设置结束时间
        order.updateById();
        return R.ok();
    }

    /**
     * 根据状态(前后端标志，管理员标志,订单状态标志)查看订单列表（分页）
     *
     * @param params
     * @return
     */
    @PostMapping("page")
    public R<Page<UniOrder>> page(@RequestBody PageParam<UniOrder> params) {
        UniOrder order = params.getParam();
        String username = LoginUserUtils.getInstance().getUsername();
        //后台查询不是管理员，只查询自己的subjectId
        if (!WxConstants.MODE_VUE.equals(order.getModeType()) || !stpInterface.checkRoleAdmin(username)) {
            order.setUsername(username);
        }
        PageParam<UniOrder> page = orderService.listPage(params);
        return R.ok(page);
    }

    /**
     * 查询订单上的红点
     *
     * @return
     */
    @GetMapping("dot")
    public R<List<WxOrderDotVo>> listDot() {
        return R.ok(orderService.listDto());
    }

}
