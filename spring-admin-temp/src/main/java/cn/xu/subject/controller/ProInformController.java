package cn.xu.subject.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.subject.entity.ProInform;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxHistoryVo;
import cn.xu.subject.entity.vo.WxInformVo;
import cn.xu.subject.service.IProRecordService;
import cn.xu.subject.service.IProInformService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 通知管理
 */
@RestController
@RequestMapping("/wx/info")
@RequiredArgsConstructor
public class ProInformController {
    private final IProInformService proInformService;
    private final IProRecordService apiRecordService;

    /**
     * 我的收藏，我的点赞，我的评论，我的通知，我的公告信息
     *
     * @param params
     * @return
     */
    @PostMapping("message")
    public R mineMessage(@RequestBody PageParam<ProRecord> params) {
        R r = new R<>();
        //1.设置当前登录人
        ProRecord record = params.getParam();
        record.setCreateUser(LoginUserUtils.getInstance().getUsername());
        //2.根据条件判断查询数据内容
        switch (record.getLikeType()) {
            case WxConstants.COLLECT, WxConstants.LIKE, WxConstants.COMMENT -> {
                // 这里需要是多表联查，查询收藏点赞记录数据
                Page<WxHistoryVo> pageHis = apiRecordService.selectLikeOrCollectOrCommentList(params);
                r.put("data", pageHis);
            }
            default -> {
                // 这里查询的是公告信息和管理员回复的通知信息
                Page<WxInformVo> pageInf = proInformService.selectInformAndNotice(params);
                r.put("data", pageInf);
            }
        }
        return r;
    }

    /**
     * 设置已读状态
     *
     * @param id
     * @return
     */
    @GetMapping("status")
    @Log(title = "评论状态", businessType = BusinessType.UPDATE)
    public R readMessage(@RequestParam Long id) {
        ProInform inform = new ProInform();
        inform.setId(id).setInformStatus(WxConstants.READ).setReadTime(new Date());
        proInformService.updateById(inform);
        return R.ok();
    }

    /**
     * 查询系统消息(点赞评论收藏)列表
     */
    @PostMapping("/page")
    public R<Page<ProInform>> list(@RequestBody PageParam<ProInform> params) {
        ProInform param = params.getParam();
        LambdaQueryWrapper<ProInform> lqw = Wrappers.lambdaQuery();
        lqw.eq(ObjectUtils.isNotEmpty(param.getInformUser()), ProInform::getInformUser, param.getInformUser());
        lqw.eq(ObjectUtils.isNotEmpty(param.getSubjectId()), ProInform::getSubjectId, param.getSubjectId());
        lqw.eq(ObjectUtils.isNotEmpty(param.getInformContent()), ProInform::getInformContent, param.getInformContent());
        lqw.eq(ObjectUtils.isNotEmpty(param.getInfoType()), ProInform::getInfoType, param.getInfoType());
        lqw.eq(ObjectUtils.isNotEmpty(param.getInformStatus()), ProInform::getInformStatus, param.getInformStatus());
        lqw.eq(ObjectUtils.isNotEmpty(param.getCreateUser()), ProInform::getCreateUser, param.getCreateUser());
        lqw.eq(ObjectUtils.isNotEmpty(param.getReadTime()), ProInform::getReadTime, param.getReadTime());
        return R.ok(this.proInformService.page(params.build(), lqw));
    }


    /**
     * 获取系统消息(点赞评论收藏)详细信息
     *
     * @param id 主键
     */
    @GetMapping("/{id}")
    public R<ProInform> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return R.ok(proInformService.getById(id));
    }


    /**
     * 修改系统消息(点赞评论收藏)
     */
    @Log(title = "系统消息(点赞评论收藏)", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Boolean> edit(@RequestBody ProInform entity) {
        return R.ok(proInformService.updateById(entity));
    }

    /**
     * 删除系统消息(点赞评论收藏)
     *
     * @param idList 主键串
     */
    @Log(title = "系统消息(点赞评论收藏)", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        return R.ok(proInformService.removeByIds(idList));
    }

}
