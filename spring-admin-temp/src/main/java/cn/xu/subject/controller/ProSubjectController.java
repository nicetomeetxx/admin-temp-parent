package cn.xu.subject.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.ExcelUtils;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.ProSubject;
import cn.xu.subject.entity.vo.WxRecordVo;
import cn.xu.subject.entity.vo.WxSubjectVo;
import cn.xu.subject.service.IProSubjectService;
import cn.xu.tools.file.entity.SysFile;
import cn.xu.tools.file.entity.SysIndex;
import cn.xu.tools.file.service.impl.SysFileServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 首页业务
 *
 * @author ljxu
 * @date 2023-07-06
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/subject/subject")
public class ProSubjectController {

    private final IProSubjectService iProSubjectService;
    private final SysFileServiceImpl fileService;

    /**
     * 查询首页业务列表
     */
    @PostMapping("/page")
    public R<Page<ProSubject>> list(@RequestBody PageParam<ProSubject> params) {
        ProSubject param = params.getParam();
        LambdaQueryWrapper<ProSubject> lqw = Wrappers.lambdaQuery();
        lqw.eq(ObjectUtils.isNotEmpty(param.getTitle()), ProSubject::getTitle, param.getTitle());
        lqw.eq(ObjectUtils.isNotEmpty(param.getMainText()), ProSubject::getMainText, param.getMainText());
        lqw.eq(ObjectUtils.isNotEmpty(param.getType()), ProSubject::getType, param.getType());
        return R.ok(this.iProSubjectService.page(params.build(), lqw));
    }


    /**
     * 获取首页业务详细信息
     *
     * @param id 主键
     */
    @GetMapping("/{id}")
    public R<ProSubject> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        //这里需要查询图片数据
        ProSubject subject = iProSubjectService.getById(id);
        Long indexId = subject.getIndexId();
        //根据index查询file,3为富文本图片，不能展示在图片列表中
        List<SysFile> fileList = fileService.lambdaQuery().eq(SysFile::getIndexId, indexId).ne(SysFile::getFileType, "3").list();
        subject.setFileList(fileList);
        return R.ok(subject);
    }

    /**
     * 新增首页业务
     */
    @Log(title = "首页业务", businessType = BusinessType.INSERT)
    @Transactional(rollbackFor = Exception.class)
    @PostMapping()
    public R<Boolean> add(@RequestBody ProSubject entity) {
        if (ObjectUtils.isEmpty(entity.getUsername())) {
            entity.setUsername(LoginUserUtils.getInstance().getUsername());
        }
        //这里新增index
        new SysIndex().setId(entity.getIndexId()).setCreateBy(LoginUserUtils.getInstance().getUsername()).insert();
        return R.ok(iProSubjectService.save(entity));
    }

    /**
     * 修改首页业务
     */
    @Log(title = "首页业务", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Boolean> edit(@RequestBody ProSubject entity) {
        fileService.removeByIds(entity.getFileIdList());
        return R.ok(iProSubjectService.updateById(entity));
    }

    /**
     * 删除首页业务
     *
     * @param idList 主键串
     */
    @Log(title = "首页业务", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        return R.ok(iProSubjectService.removeByIds(idList));
    }

    /**
     * 导出首页业务列表
     */
    @SneakyThrows
    @PostMapping("/export")
    public void export(ProSubject entity, HttpServletResponse response) {
        LambdaQueryWrapper<ProSubject> lqw = Wrappers.lambdaQuery();
        lqw.eq(ObjectUtils.isNotEmpty(entity.getTitle()), ProSubject::getTitle, entity.getTitle());
        lqw.eq(ObjectUtils.isNotEmpty(entity.getMainText()), ProSubject::getMainText, entity.getMainText());
        lqw.eq(ObjectUtils.isNotEmpty(entity.getType()), ProSubject::getType, entity.getType());
        List<ProSubject> list = iProSubjectService.list(lqw);
        ExcelUtils.exportExcel(list, "首页业务数据表", "首页业务", ProSubject.class, "首页业务", response);
    }

    //***************************************下面接口是小程序查询接口，使用协同过滤算法显示首页排行*************************************

    /**
     * 小程序首页查询列表第一个接口（使用协同过滤查询随机8条）
     *
     * @return
     */
    @PostMapping("/noAuth/wx/subject")
    public R<List<WxSubjectVo>> getWxMainSubject() {
        return R.ok(iProSubjectService.getWxMainSubject());
    }

    /**
     * 小程序首页查询列表第二个接口（按照点赞量或者收藏量随机排序）
     *
     * @return
     */
    @PostMapping("/noAuth/wx/subject/page")
    public R<Page<WxSubjectVo>> getWxPageSubject(@RequestBody PageParam<WxSubjectVo> pageParam) {
        return R.ok(iProSubjectService.getWxPageSubject(pageParam));
    }

    /**
     * 根据id查询详情信息
     *
     * @return
     */
    @GetMapping("/wx/subject/detail")
    public R<WxSubjectVo> getWxSubjectDetail(@RequestParam Long id) {
        ProSubject subject = iProSubjectService.getById(id);
        //这里设置轮播图
        subject.setFileList(fileService.getFileListByIndexId(subject.getIndexId()));
        return R.ok(subject.subjectToVo(subject));
    }

    /**
     * 根据id查询评论列表
     *
     * @return
     */
    @GetMapping("/wx/subject/record")
    public R<List<WxRecordVo>> getWxSubjectRecord(@RequestParam Long id) {
        return R.ok(iProSubjectService.getWxSubjectRecord(id));
    }

    /**
     * 根据id查询我是否收藏点赞
     *
     * @return
     */
    @GetMapping("/wx/subject/mineLike")
    public R getMineLike(@RequestParam Long id) {
        return R.ok(iProSubjectService.getMineLike(id));
    }

    /**
     * 管理台回复评论内容
     *
     * @return
     */
    @GetMapping("/replay/comment")
    public R<List<WxRecordVo>> replayComment(@RequestBody ProRecord proRecord) {
        iProSubjectService.replayComment(proRecord);
        return R.ok();
    }
}
