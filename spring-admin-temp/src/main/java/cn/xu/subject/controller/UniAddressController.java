package cn.xu.subject.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.config.utils.StringUtils;
import cn.xu.subject.entity.UniAddress;
import cn.xu.subject.service.IUniAddressService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 这里是收货地址
 */
@RestController
@RequestMapping("address")
@RequiredArgsConstructor
public class UniAddressController {


    private final IUniAddressService iUniAddressService;

    /**
     * 查询收货地址列表
     */
    @PostMapping("/page")
    public R<Page<UniAddress>> page(@RequestBody PageParam<UniAddress> params) {
        LambdaQueryWrapper<UniAddress> lqw = new LambdaQueryWrapper<UniAddress>();
        lqw.eq(UniAddress::getUsername, LoginUserUtils.getInstance().getUsername());
        lqw.orderBy(true, false, UniAddress::getIsDefault, UniAddress::getCreateTime);
        return R.ok(this.iUniAddressService.page(params.build(), lqw));
    }

    /**
     * 查询我的收货地址列表
     */
    @PostMapping("/list")
    public R<List<UniAddress>> list() {
        LambdaQueryWrapper<UniAddress> lqw = new LambdaQueryWrapper<UniAddress>();
        lqw.eq(UniAddress::getUsername, LoginUserUtils.getInstance().getUsername());
        lqw.orderBy(true, false, UniAddress::getIsDefault, UniAddress::getCreateTime);
        return R.ok(this.iUniAddressService.list(lqw));
    }


    /**
     * 获取收货地址详细信息
     *
     * @param id 主键
     */
    @GetMapping("/{id}")
    public R<UniAddress> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return R.ok(iUniAddressService.getById(id));
    }

    /**
     * 获取默认收货地址
     */
    @GetMapping
    public R<UniAddress> getDefaultAddress() {
        UniAddress one = iUniAddressService.getOne(new LambdaQueryWrapper<UniAddress>()
                .eq(UniAddress::getUsername, LoginUserUtils.getInstance().getUsername()).eq(UniAddress::getIsDefault, "1"));
        if (ObjectUtils.isEmpty(one)) {
            List<UniAddress> list = iUniAddressService.list(new LambdaQueryWrapper<UniAddress>()
                    .eq(UniAddress::getUsername, LoginUserUtils.getInstance().getUsername()));
            if (CollectionUtils.isNotEmpty(list)) {
                one = list.get(0);
            }
        }
        return R.ok(one);//1为默认地址
    }

    /**
     * 新增收货地址
     */
    @Log(title = "收货地址", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Boolean> add(@RequestBody UniAddress entity) {
        entity.setUsername(LoginUserUtils.getInstance().getUsername());
        return R.ok(iUniAddressService.save(entity));
    }

    /**
     * 修改收货地址
     */
    @Log(title = "收货地址", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Boolean> edit(@RequestBody UniAddress entity) {
        return R.ok(iUniAddressService.updateById(entity));
    }

    /**
     * 设置默认收获地址
     */
    @Log(title = "收货地址", businessType = BusinessType.UPDATE)
    @PutMapping("{id}")
    public R<Boolean> editDefault(@PathVariable Long id) {
        UniAddress address = new UniAddress();
        address.setIsDefault("0");
        address.setUsername(LoginUserUtils.getInstance().getUsername());
        iUniAddressService.update(address, new LambdaUpdateWrapper<UniAddress>().eq(UniAddress::getUsername, LoginUserUtils.getInstance().getUsername()));
        UniAddress entity = new UniAddress();
        entity.setIsDefault("1");
        entity.setId(id);
        iUniAddressService.updateById(entity);
        return R.ok();
    }

    /**
     * 删除收货地址
     *
     * @param idList 主键串
     */
    @Log(title = "收货地址", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        return R.ok(iUniAddressService.removeByIds(idList));
    }


}
