package cn.xu.subject.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxSubjectVo;
import cn.xu.subject.service.IProRecordService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 这里是评论 点赞 收藏（及记录信息）
 *
 * @author ljxu
 * @date 2023-07-06
 */
@RestController
@RequestMapping("/wx/like")
@RequiredArgsConstructor
public class ProRecordController {
    private final IProRecordService apiRecordService;

    /**
     * 收藏，点赞. 评论  （subject）
     *
     * @return
     */
    @PostMapping("comment")
    @Log(title = "收藏/点赞/评论", businessType = BusinessType.INSERT)
    public R<Void> collectOrLikeOrCommentSubject(@RequestBody ProRecord proRecord) {
        apiRecordService.collectOrLikeOrCommentSubject(proRecord);
        return R.ok();
    }

    /**
     * 后台查询评论列表
     *
     * @return
     */
    @PostMapping("page")
    public R<Page<ProRecord>> selectCommentList(@RequestBody PageParam<ProRecord> pageParam) {
        return R.ok(apiRecordService.selectCommentList(pageParam));
    }

    /**
     * 回复评论
     *
     * @return
     */
    @PostMapping("reply")
    @Log(title = "回复评论", businessType = BusinessType.UPDATE)
    public R<Void> replyComment(@RequestBody ProRecord record) {
        apiRecordService.replyComment(record);
        return R.ok();
    }

    /**
     * 删除首页业务
     *
     * @param idList 主键串
     */
    @Log(title = "删除评论", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        return R.ok(apiRecordService.removeByIds(idList));
    }
}
