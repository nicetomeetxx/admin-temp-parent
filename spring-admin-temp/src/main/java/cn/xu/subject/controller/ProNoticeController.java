package cn.xu.subject.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.config.utils.UserUtils;
import cn.xu.subject.entity.ProNotice;
import cn.xu.subject.service.IProNoticeService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 这里是系统公告管理
 */
@RestController
@RequestMapping("/wx/notice")
@RequiredArgsConstructor
public class ProNoticeController {

    private final IProNoticeService proNoticeService;


    /**
     * 小程序查询系统通知列表
     *
     * @param params
     * @return
     */
    @PostMapping("message")
    public R mineNotice(@RequestBody PageParam<ProNotice> params) {
        Page<ProNotice> pageInf = proNoticeService.page(params.build(), new LambdaQueryWrapper<ProNotice>().orderBy(true, false, ProNotice::getCreateTime));
        return R.ok(pageInf);
    }

    /**
     * 查询系统公告信息列表
     */
    @PostMapping("/page")
    public R<Page<ProNotice>> list(@RequestBody PageParam<ProNotice> params) {
        ProNotice param = params.getParam();
        LambdaQueryWrapper<ProNotice> lqw = Wrappers.lambdaQuery();
        lqw.eq(ObjectUtils.isNotEmpty(param.getTitle()), ProNotice::getTitle, param.getTitle());
        lqw.eq(ObjectUtils.isNotEmpty(param.getContent()), ProNotice::getContent, param.getContent());
        lqw.eq(ObjectUtils.isNotEmpty(param.getCreateUser()), ProNotice::getCreateUser, param.getCreateUser());
        return R.ok(this.proNoticeService.page(params.build(), lqw));
    }


    /**
     * 获取系统公告信息详细信息
     *
     * @param id 主键
     */
    @GetMapping("/{id}")
    public R<ProNotice> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return R.ok(proNoticeService.getById(id));
    }

    /**
     * 新增系统公告信息
     */
    @Log(title = "系统公告信息", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Boolean> add(@RequestBody ProNotice entity) {
        entity.setCreateUser(UserUtils.selectUserIdByUserName(LoginUserUtils.getInstance().getUsername()).getName());
        return R.ok(proNoticeService.save(entity));
    }

    /**
     * 修改系统公告信息
     */
    @Log(title = "系统公告信息", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Boolean> edit(@RequestBody ProNotice entity) {
        return R.ok(proNoticeService.updateById(entity));
    }

    /**
     * 删除系统公告信息
     *
     * @param idList 主键串
     */
    @Log(title = "系统公告信息", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        return R.ok(proNoticeService.removeByIds(idList));
    }

    /**
     * 护球消息数量和公告信息数量
     */
    @GetMapping("num")
    public R getMessageNum() {
        return R.ok(proNoticeService.getMessageNum());
    }

}
