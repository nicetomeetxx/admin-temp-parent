package cn.xu.subject.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 点赞，评论，收藏记录表(ProRecord)表实体类
 *
 * @author ljxu
 * @since 2023-07-21 15:18:47
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ProRecord extends Model<ProRecord> {

    @TableId(type = IdType.AUTO)
    private Long id;
    //主题id
    private Long subjectId;
    //收藏0；点赞1；评论2
    private String likeType;
    //评论内容
    private String comment;
    //创建人
    private String createUser;
    //创建时间
    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;
    //归属人
    private String subjectUser;
    //评论回复
    private String commentReply;
    //回复时间
    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date replyTime;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

