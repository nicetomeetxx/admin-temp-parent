package cn.xu.subject.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * 这里是消息通知的公告和通知
 */
@Data
@EqualsAndHashCode
public class WxInformVo implements Serializable {

    private Long id;
    //缩略主图
    private String mainImage;
    //消息创建人
    private String createUser;
    //消息主题
    private String title;
    //subjectId
    private Long subjectId;
    //类型：区分是公告还是通知
    private String infoType;
    //消息状态：0未读，1已读
    private String informStatus;
    //创建时间
    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;

}
