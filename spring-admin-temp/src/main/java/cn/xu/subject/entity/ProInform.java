package cn.xu.subject.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * (ProInform)表实体类
 *
 * @author ljxu
 * @since 2023-07-21 15:49:00
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ProInform extends Model<ProInform> {

    @TableId(type = IdType.AUTO)
    private Long id;
    //接收通知人
    private String informUser;
    //主题id
    private Long subjectId;
    //通知内容
    private String informContent;
    //消息类型
    private String infoType;
    //状态：0未读；1已读
    private String informStatus;
    //消息创建人
    private String createUser;
    //创建时间
    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;
    //已读时间
    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date readTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

