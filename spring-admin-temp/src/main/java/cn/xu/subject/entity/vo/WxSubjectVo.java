package cn.xu.subject.entity.vo;

import cn.xu.tools.file.entity.SysFile;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 这个是小程序 论坛类型首页及详情
 */
@Data
public class WxSubjectVo implements Serializable {

    private Long id;
    //首页主图
    private String mainImage;
    //首页描述文字（主题）
    private String desc;
    //类型
    private String label;
    //正文
    private String mainText;
    //收藏数量
    private Long collectionCount;
    //评论数量
    private Long commentCount;
    //点赞数量
    private Long likeCount;
    //发起人
    private String username;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //详情页轮播图
    private List<SysFile> swiperList;
    //店铺
    private String store;
    //单价
    private BigDecimal unitPrice;
}
