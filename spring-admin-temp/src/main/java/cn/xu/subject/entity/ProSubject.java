package cn.xu.subject.entity;

import cn.xu.subject.entity.vo.WxSubjectVo;
import cn.xu.tools.file.entity.vo.FileVo;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.security.auth.Subject;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * 首页业务对象 pro_subject
 *
 * @author ljxu
 * @date 2023-07-06
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("pro_subject")
public class ProSubject extends FileVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * 标题
     */
    private String title;
    /**
     * index
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long indexId;
    /**
     * 正文
     */
    private String mainText;
    /**
     * 类型
     */
    private String type;
    /**
     * 收藏数量
     */
    private Long collectNum;
    /**
     * 点赞数量
     */
    private Long likeNum;
    /**
     * 评论数量
     */
    private Long commentNum;
    /**
     * 用户
     */
    private String username;
    /**
     * 是否删除: 0未删除；1已删除
     */
    @TableLogic
    @TableField(select = false)
    private String isDelete;

    //店铺
    private String store;
    //单价
    private BigDecimal unitPrice;

    //--------创建修改时间-----------
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;


    /**
     * 类型转换
     *
     * @param subject
     * @return
     */
    public WxSubjectVo subjectToVo(ProSubject subject) {
        WxSubjectVo wxSubjectVo = new WxSubjectVo();
        wxSubjectVo.setId(subject.getId());
        wxSubjectVo.setDesc(subject.getTitle());
        wxSubjectVo.setMainText(subject.getMainText());
        wxSubjectVo.setLabel(subject.getType());
        wxSubjectVo.setCollectionCount(subject.getCollectNum());
        wxSubjectVo.setCommentCount(subject.getCommentNum());
        wxSubjectVo.setLikeCount(subject.getLikeNum());
        wxSubjectVo.setUsername(subject.getUsername());
        wxSubjectVo.setCreateTime(subject.getCreateTime());
        wxSubjectVo.setSwiperList(subject.getFileList());//轮播图设置
        wxSubjectVo.setStore(subject.getStore());
        wxSubjectVo.setUnitPrice(subject.getUnitPrice());
        return wxSubjectVo;
    }

}
