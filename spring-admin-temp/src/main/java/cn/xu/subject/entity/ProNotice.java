package cn.xu.subject.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统公告信息表(ProNotice)表实体类
 *
 * @author ljxu
 * @since 2023-07-26 10:56:43
 */
@SuppressWarnings("serial")
@Data
public class ProNotice extends Model<ProNotice> {
    @TableId(type = IdType.AUTO)
    private Long id;
    //通知标题
    private String title;
    //通知内容
    private String content;
    //创建人
    private String createUser;
    //创建时间
    @JsonFormat(pattern = "yyyy/MM/dd hh:mm:ss")
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

