package cn.xu.subject.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 订单记录表(UniOrder)表实体类
 *
 * @author ljxu
 * @since 2023-08-01 15:47:57
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UniOrder extends Model<UniOrder> implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    //subject_id
    private Long subjectId;
    //订单编号
    private String orderNo;
    //标题
    private String title;
    //购买人username
    private String username;
    //收货人
    private String buyUser;
    //收货地址
    private String address;
    //手机号
    private String telephone;
    //单价
    private Double unitPrice;
    //购买数量
    private Integer buyNum;
    //总价
    private Double totalPrice;
    //订单状态:0待付款；1待发货；2待评价；3完成；4取消
    private String orderStatus;
    //快递单号
    private String expressNo;
    //评价星级
    private int evaluateStar;
    //创建时间
    private Date createTime;
    //付款时间
    private Date payTime;
    //发货时间
    private Date sendTime;
    //收货时间
    private Date shippedTime;
    //评价时间
    private Date evaluatedTime;
    //订单结束时间
    private Date endTime;

    //查询方式
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String modeType;
    //当前查询用户
    @TableField(exist = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String selectUser;
    //主图
    @TableField(exist = false)
    private String mainImage;

}

