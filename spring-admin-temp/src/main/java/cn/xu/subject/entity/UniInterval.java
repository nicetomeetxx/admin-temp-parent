package cn.xu.subject.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serial;
import java.util.Date;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 预约时间区间对象 uni_interval
 *
 * @author ljxu
 * @date 2023-07-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("uni_interval")
public class UniInterval extends Model<UniInterval> {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 预约时间区间
     */
    private String timeInterval;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
