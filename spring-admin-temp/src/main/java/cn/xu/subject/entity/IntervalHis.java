package cn.xu.subject.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * 预约历史表(UniIntervalHis)表实体类
 *
 * @author ljxu
 * @since 2023-07-28 09:39:09
 */
@Data
@Accessors(chain = true)
@TableName("uni_interval_his")
public class IntervalHis extends Model<IntervalHis> {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    //主业务id
    private Long subjectId;
    //预约业务标题
    private String title;
    //预约的天
    private String appDay;
    //区间
    private String timeInterval;
    //预约人
    private String createUser;
    //预约人code
    private String userCode;
    //预约状态：0已预约；1已完成；2已取消
    private String appointStatus;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    //首页主图
    @TableField(exist = false)
    private String mainImage;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

