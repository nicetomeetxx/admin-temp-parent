package cn.xu.subject.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 收藏点赞评论(消息页展示)
 * 这个是subject和record两个为主表
 */
@Data
public class WxHistoryVo implements Serializable {

    private Long id;
    //主题id
    private Long subjectId;
    //收藏0；点赞1；评论2
    private String likeType;
    //评论内容
    private String comment;
    //创建人
    private String createUser;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date createTime;
    //归属人
    private String subjectUser;
    //首页主图
    private String mainImage;
    //首页描述文字（主题）
    private String title;
    //类型
    private String type;

}
