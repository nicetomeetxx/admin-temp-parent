package cn.xu.subject.entity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xu
 * @create 2023-07-23 14:12
 * @desc 收藏点赞记录（首页使用）
 * 这个是record为主表
 **/
@Data
public class WxRecordVo implements Serializable {

    private Long id;
    //主题id
    private Long subjectId;
    //收藏0；点赞1；评论2
    private String likeType;
    //评论内容
    private String comment;
    //创建人
    private String createAvatar;
    private String createUser;
    //创建时间
    @JsonFormat(pattern = "yyyy年MM月dd日 hh:mm:ss")
    private Date createTime;
    //归属人
    private String subjectAvatar;
    private String subjectUser;
    //评论回复
    private String commentReply;
    //回复时间
    @JsonFormat(pattern = "yyyy年MM月dd日 hh:mm:ss")
    private Date replyTime;
}
