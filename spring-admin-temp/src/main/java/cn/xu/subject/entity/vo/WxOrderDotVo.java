package cn.xu.subject.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class WxOrderDotVo implements Serializable {

    private String name;

    private String count;
}
