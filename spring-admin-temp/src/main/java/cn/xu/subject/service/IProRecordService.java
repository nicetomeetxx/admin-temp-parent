package cn.xu.subject.service;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxHistoryVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

public interface IProRecordService extends IService<ProRecord> {
    /**
     * 收藏点赞评论
     *
     * @param proRecord
     */
    void collectOrLikeOrCommentSubject(ProRecord proRecord);

    /**
     * zpaging分页查询收藏点赞评论记录
     *
     * @param params
     * @return
     */
    Page<WxHistoryVo> selectLikeOrCollectOrCommentList(PageParam<ProRecord> params);

    /**
     * 后台查询评论列表
     *
     * @param pageParam pageParam
     * @return r
     */
    Page<ProRecord> selectCommentList(PageParam<ProRecord> pageParam);

    /**
     * 回复评论
     *
     * @param record record
     */
    void replyComment(ProRecord record);
}
