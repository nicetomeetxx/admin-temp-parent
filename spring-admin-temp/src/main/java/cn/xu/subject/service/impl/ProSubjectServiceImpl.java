package cn.xu.subject.service.impl;

import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.PageParam;
import cn.xu.config.utils.CollFilterUtils;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.config.utils.StringUtils;
import cn.xu.config.utils.UserUtils;
import cn.xu.subject.dao.ProRecordDao;
import cn.xu.subject.dao.ProSubjectDao;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.ProSubject;
import cn.xu.subject.entity.vo.WxRecordVo;
import cn.xu.subject.entity.vo.WxSubjectVo;
import cn.xu.subject.service.IProSubjectService;
import cn.xu.system.entity.SysUser;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 首页业务Service业务层处理
 *
 * @author ljxu
 * @date 2023-07-06
 */
@Service
@RequiredArgsConstructor
public class ProSubjectServiceImpl extends ServiceImpl<ProSubjectDao, ProSubject> implements IProSubjectService {

    private final ProSubjectDao proSubjectDao;
    private final ProRecordDao proRecordDao;

    /**
     * 小程序首页查询前八条 按排名（基于协同过滤算法）
     *
     * @return
     */
    @Override
    public List<WxSubjectVo> getWxMainSubject() {
        //1.先判断用户是否登录，用户登录了就用协同过滤算法，用户没登录那就直接查询前n条数据
        String username = LoginUserUtils.getInstance().getUsername();
        List<WxSubjectVo> wxMainSubject = null;
        if (!StringUtils.isEmpty(username)) {
            wxMainSubject = collaborativeFilter(username, wxMainSubject);
        } else {
            wxMainSubject = baseMapper.getWxMainSubject();
        }
//        List<Long> collect = wxMainSubject.stream().map(WxSubjectVo::getId).toList();
        return wxMainSubject;
    }

    /**
     * 第二个接口 下滑分页
     *
     * @param pageParam p
     * @return
     */
    @Override
    public Page<WxSubjectVo> getWxPageSubject(PageParam<WxSubjectVo> pageParam) {
        //这里需要把vo转换成bo 查询要来回转换
        Page<WxSubjectVo> page = new Page<>(pageParam.getCurrent(), pageParam.getSize());
        return proSubjectDao.selectByCondition(page, pageParam.getParam());
    }

    /**
     * 下面是协同过滤获取数据
     *
     * @param username      username
     * @param wxMainSubject wxMainSubject
     * @return r
     */
    private List<WxSubjectVo> collaborativeFilter(String username, List<WxSubjectVo> wxMainSubject) {
        //1.查询所有用户（随机获取五个用户） SELECT * FROM `sys_user` ORDER BY RAND() LIMIT 0,5;
        //2.遍历用户，查询评论表中subject的id,like_num order by desc
        //3.放到上面map中，获取推荐的前几位
        //4.根据推荐的前几位去subject中in('id','id')得到推荐数据，实现协同过滤算法推荐

        SysUser userId = UserUtils.selectUserIdByUserName(username);
        Map<String, Map<String, Double>> userRatings = new HashMap<>();
        CollFilterUtils filter = new CollFilterUtils(userRatings);
        // 为指定用户推荐物品
        String targetUser = "User4";
        int numRecommendations = 4;
        List<String> recommendations = filter.recommendItems(targetUser, numRecommendations);
        return wxMainSubject;
    }

    /**
     * 查询详情页评论列表
     *
     * @param id id
     * @return
     */
    @Override
    public List<WxRecordVo> getWxSubjectRecord(Long id) {
        return proRecordDao.selectWxSubjectRecord(id);
    }

    /**
     * 回复评论内容
     *
     * @param proRecord
     */
    @Override
    public void replayComment(ProRecord proRecord) {
        proRecord.updateById();
    }

    /**
     * 查询我的点赞收藏状态
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, String> getMineLike(Long id) {
        String username = LoginUserUtils.getInstance().getUsername();
        List<ProRecord> list = proRecordDao.selectList(new LambdaQueryWrapper<ProRecord>().eq(ProRecord::getCreateUser, username).eq(ProRecord::getSubjectId, id));
        Map<String, String> map = new HashMap<>();
        boolean likeFlag = false;
        boolean collectFlag = false;
        for (ProRecord proRecord : list) {
            switch (proRecord.getLikeType()) {
                case WxConstants.COLLECT -> collectFlag = true;
                case WxConstants.LIKE -> likeFlag = true;
            }
        }
        if (likeFlag) {
            map.put("like", "1");
        }
        if (collectFlag) {
            map.put("collect", "1");
        }
        return map;
    }
}
