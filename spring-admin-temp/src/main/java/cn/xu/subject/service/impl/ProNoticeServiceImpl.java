package cn.xu.subject.service.impl;

import cn.xu.config.utils.LoginUserUtils;
import cn.xu.subject.dao.ProInformDao;
import cn.xu.subject.dao.ProNoticeDao;
import cn.xu.subject.entity.ProNotice;
import cn.xu.subject.service.IProInformService;
import cn.xu.subject.service.IProNoticeService;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * (ProNotice)公告信息表
 *
 * @author ljxu
 * @since 2023-07-25 09:39:56
 */
@Service("proNoticeService")
@RequiredArgsConstructor
public class ProNoticeServiceImpl extends ServiceImpl<ProNoticeDao, ProNotice> implements IProNoticeService {
    private final ProInformDao informDao;

    /**
     * 查询首页
     * @return
     */
    @Override
    public Map<String, Long> getMessageNum() {
        String username = LoginUserUtils.getInstance().getUsername();
        Long informNum = informDao.selectInformNum(username);
        Long noticeNum = baseMapper.selectNoticeNum();
        Map<String, Long> map = new HashMap<>();
        map.put("inform", informNum);
        map.put("notice", noticeNum);
        return map;
    }
}

