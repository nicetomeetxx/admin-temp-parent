package cn.xu.subject.service.impl;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxInformVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.subject.dao.ProInformDao;
import cn.xu.subject.entity.ProInform;
import cn.xu.subject.service.IProInformService;
import org.springframework.stereotype.Service;

/**
 * (ProInform)消息通知表
 *
 * @author ljxu
 * @since 2023-07-25 09:39:56
 */
@Service("proInformService")
public class ProInformServiceImpl extends ServiceImpl<ProInformDao, ProInform> implements IProInformService {
    /**
     * 消息页面查询通知内容和公告管理消息
     *
     * @param params
     * @return
     */
    @Override
    public Page<WxInformVo> selectInformAndNotice(PageParam<ProRecord> params) {
        return baseMapper.selectInformAndNotice(params, params.getParam());
    }
}

