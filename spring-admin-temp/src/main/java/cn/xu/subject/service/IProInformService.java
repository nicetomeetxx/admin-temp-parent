package cn.xu.subject.service;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxInformVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xu.subject.entity.ProInform;

/**
 * (ProInform)消息通知表
 *
 * @author ljxu
 * @since 2023-07-25 09:39:56
 */
public interface IProInformService extends IService<ProInform> {
    /**
     * 消息页查看通知和公告列表数据
     *
     * @param params
     * @return
     */
    Page<WxInformVo> selectInformAndNotice(PageParam<ProRecord> params);
}

