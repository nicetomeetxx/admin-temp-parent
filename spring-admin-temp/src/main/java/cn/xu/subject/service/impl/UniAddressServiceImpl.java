package cn.xu.subject.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.subject.dao.UniAddressDao;
import cn.xu.subject.entity.UniAddress;
import cn.xu.subject.service.IUniAddressService;
import org.springframework.stereotype.Service;

/**
 * 收货地址Service业务层处理
 *
 * @author ljxu
 * @date 2023-08-01
 */
@Service
public class UniAddressServiceImpl   extends ServiceImpl<UniAddressDao, UniAddress> implements IUniAddressService {


}
