package cn.xu.subject.service.impl;

import cn.xu.subject.dao.UniIntervalDao;
import cn.xu.subject.entity.UniInterval;
import cn.xu.subject.service.IUniIntervalService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 预约时间区间Service业务层处理
 *
 * @author ljxu
 * @date 2023-07-28
 */
@Service
public class UniIntervalServiceImpl extends ServiceImpl<UniIntervalDao, UniInterval> implements IUniIntervalService {


}
