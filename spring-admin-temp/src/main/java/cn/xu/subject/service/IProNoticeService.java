package cn.xu.subject.service;

import cn.xu.subject.entity.ProNotice;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * (ProNotice)公告信息表
 *
 * @author ljxu
 * @since 2023-07-25 09:39:56
 */
public interface IProNoticeService extends IService<ProNotice> {
    /**
     * 获取消息数量
     *
     * @return
     */
    Map<String, Long> getMessageNum();
}

