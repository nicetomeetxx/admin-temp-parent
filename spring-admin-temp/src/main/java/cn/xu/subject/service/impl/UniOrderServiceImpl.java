package cn.xu.subject.service.impl;

import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.vo.WxOrderDotVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.subject.dao.UniOrderDao;
import cn.xu.subject.entity.UniOrder;
import cn.xu.subject.service.IUniOrderService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 订单记录Service业务层处理
 *
 * @author ljxu
 * @date 2023-08-01
 */
@Service
public class UniOrderServiceImpl extends ServiceImpl<UniOrderDao, UniOrder> implements IUniOrderService {

    /**
     * 分页查询
     *
     * @param params params
     * @return
     */
    @Override
    public PageParam<UniOrder> listPage(PageParam<UniOrder> params) {
        return baseMapper.listPage(params, params.getParam());
    }

    /**
     * 查询红点
     *
     * @return
     */
    @Override
    public List<WxOrderDotVo> listDto() {
        List<WxOrderDotVo> listDto = baseMapper.listDto();
        List<WxOrderDotVo> list = new ArrayList<>();
        WxOrderDotVo wxOrderDotVo0 = new WxOrderDotVo().setName("全部");
        WxOrderDotVo wxOrderDotVo1 = new WxOrderDotVo().setName("待支付");
        WxOrderDotVo wxOrderDotVo2 = new WxOrderDotVo().setName("待发货");
        WxOrderDotVo wxOrderDotVo3 = new WxOrderDotVo().setName("待收货");
        WxOrderDotVo wxOrderDotVo4 = new WxOrderDotVo().setName("待评价");
        for (WxOrderDotVo orderDot : listDto) {
            switch (orderDot.getName()) {
                case WxConstants.TO_BE_PAID -> {
                    wxOrderDotVo1.setCount(orderDot.getCount());
                }
                case WxConstants.TO_BE_SHIPPED -> {
                    wxOrderDotVo2.setCount(orderDot.getCount());
                }
                case WxConstants.TO_GET_SHIPPED -> {
                    wxOrderDotVo3.setCount(orderDot.getCount());
                }
                case WxConstants.TO_BE_EVALUATED -> {
                    wxOrderDotVo4.setCount(orderDot.getCount());
                }
            }
        }
        list.add(wxOrderDotVo0);
        list.add(wxOrderDotVo1);
        list.add(wxOrderDotVo2);
        list.add(wxOrderDotVo3);
        list.add(wxOrderDotVo4);
        return list;
    }
}
