package cn.xu.subject.service;

import cn.xu.subject.entity.IntervalHis;
import cn.xu.system.entity.SysUser;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 预约历史Service接口
 *
 * @author ljxu
 * @date 2023-07-28
 */
public interface IIntervalHisService extends IService<IntervalHis> {
    /**
     * 预约前校验
     *
     * @param history history
     */
    void checkAppoint(IntervalHis history, SysUser sysUser);

    /**
     * 小程序分页查询我的预约列表
     *
     * @param build build
     * @param param param
     * @return r
     */
    Page<IntervalHis> pageList(Page<IntervalHis> build, IntervalHis param);
}
