package cn.xu.subject.service.impl;

import cn.xu.config.constant.WxConstants;
import cn.xu.config.exception.ServiceException;
import cn.xu.subject.dao.IntervalHisDao;
import cn.xu.subject.entity.IntervalHis;
import cn.xu.subject.service.IIntervalHisService;
import cn.xu.system.entity.SysUser;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 预约历史Service业务层处理
 *
 * @author ljxu
 * @date 2023-07-28
 */
@Service
public class IntervalHisServiceImpl extends ServiceImpl<IntervalHisDao, IntervalHis> implements IIntervalHisService {

    /**
     * 预约前校验
     *
     * @param history history
     */
    @Override
    public void checkAppoint(IntervalHis history, SysUser sysUser) {
        //查询该用户当天是否有预约
        IntervalHis one = baseMapper.selectOne(new LambdaQueryWrapper<IntervalHis>()
                .eq(IntervalHis::getAppDay, history.getAppDay())
                .eq(IntervalHis::getUserCode, sysUser.getUsername())
                .eq(IntervalHis::getAppointStatus, WxConstants.APPOINT_HAS));
        if (ObjectUtils.isNotEmpty(one)) {
            throw new ServiceException("今天有约哦");
        }
        //查询该用户当天是否有预约
        IntervalHis his = baseMapper.selectOne(new LambdaQueryWrapper<IntervalHis>()
                .eq(IntervalHis::getAppDay, history.getAppDay())
                .eq(IntervalHis::getTimeInterval, history.getTimeInterval())
                .eq(IntervalHis::getAppointStatus, WxConstants.APPOINT_HAS));
        if (ObjectUtils.isNotEmpty(his)) {
            throw new ServiceException("被人抢先一步");
        }
    }

    /**
     * 小程序分页查询我的预约列表
     *
     * @param build build
     * @param param param
     * @return
     */
    @Override
    public Page<IntervalHis> pageList(Page<IntervalHis> build, IntervalHis param) {
        return baseMapper.pageList(build, param);
    }
}
