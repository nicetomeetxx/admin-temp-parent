package cn.xu.subject.service;

import cn.xu.subject.entity.UniAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 收货地址Service接口
 *
 * @author ljxu
 * @date 2023-08-01
 */
public interface IUniAddressService  extends IService<UniAddress>{

}
