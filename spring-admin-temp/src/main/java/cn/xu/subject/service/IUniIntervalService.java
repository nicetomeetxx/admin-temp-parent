package cn.xu.subject.service;

import cn.xu.subject.entity.UniInterval;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 预约时间区间Service接口
 *
 * @author ljxu
 * @date 2023-07-28
 */
public interface IUniIntervalService  extends IService<UniInterval>{

}
