package cn.xu.subject.service;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.UniOrder;
import cn.xu.subject.entity.vo.WxOrderDotVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 订单记录Service接口
 *
 * @author ljxu
 * @date 2023-08-01
 */
public interface IUniOrderService extends IService<UniOrder> {
    /**
     * 分页查询
     *
     * @param params params
     * @return r
     */
    PageParam<UniOrder> listPage(PageParam<UniOrder> params);

    /**
     * 查询红点
     * @return
     */
    List<WxOrderDotVo> listDto();
}
