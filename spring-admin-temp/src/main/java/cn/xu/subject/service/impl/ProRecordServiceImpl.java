package cn.xu.subject.service.impl;

import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.PageParam;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.config.utils.UserUtils;
import cn.xu.subject.dao.ProRecordDao;
import cn.xu.subject.entity.ProInform;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.ProSubject;
import cn.xu.subject.entity.vo.WxHistoryVo;
import cn.xu.subject.service.IProRecordService;
import cn.xu.subject.service.IProSubjectService;
import cn.xu.system.entity.SysUser;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProRecordServiceImpl extends ServiceImpl<ProRecordDao, ProRecord> implements IProRecordService {
    private final IProSubjectService proSubjectService;

    /**
     * 收藏点赞评论
     *
     * @param proRecord
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void collectOrLikeOrCommentSubject(ProRecord proRecord) {
        //1.根据subjectId查询该记录创建人
        ProSubject subject = proSubjectService.getById(proRecord.getSubjectId());
        //获取当前登录人
        SysUser user = UserUtils.selectUserIdByUserName(LoginUserUtils.getInstance().getUsername());
        proRecord.setCreateUser(user.getUsername());
        //设置subject_user
        proRecord.setSubjectUser(subject.getUsername());
        //先查询是否已经点赞或者收藏了(不包含评论)
        if (!WxConstants.COMMENT.equals(proRecord.getLikeType())) {
            List<ProRecord> recordHistory = baseMapper.selectList(new LambdaQueryWrapper<ProRecord>()
                    .eq(ProRecord::getSubjectId, proRecord.getSubjectId()).eq(ProRecord::getCreateUser, user.getUsername())
                    .eq(ProRecord::getLikeType, proRecord.getLikeType()));
            if (CollectionUtils.isNotEmpty(recordHistory)) {
                return;
            }
        }
        //上面为空说明没有点赞过
        proRecord.insert();
        //2.生成通知，告诉subject_user文章动态
        ProInform proInform = new ProInform();
        proInform.setInformUser(subject.getUsername()).setSubjectId(subject.getId()).setCreateUser(user.getUsername());
        switch (proRecord.getLikeType()) {
            case WxConstants.COLLECT -> {
                subject.setCollectNum(subject.getCollectNum() + 1);
                proInform.setInformContent("收藏了您的:" + subject.getTitle());
                proInform.setInfoType(WxConstants.COLLECT);
            }
            case WxConstants.LIKE -> {
                subject.setLikeNum(subject.getLikeNum() + 1);
                proInform.setInformContent("点赞了您的:" + subject.getTitle());
                proInform.setInfoType(WxConstants.LIKE);
            }
            case WxConstants.COMMENT -> {
                subject.setCommentNum(subject.getCommentNum() + 1);
                proInform.setInformContent("评论了您的:" + subject.getTitle());
                proInform.setInfoType(WxConstants.COMMENT);
            }
        }
        proInform.insert();
        //3.修改subject点赞评论回复数量
        proSubjectService.updateById(subject);
    }

    /**
     * 查询我的收藏点赞评论记录
     *
     * @param params
     * @return
     */
    @Override
    public Page<WxHistoryVo> selectLikeOrCollectOrCommentList(PageParam<ProRecord> params) {
        //.分页查询收藏记录
        return baseMapper.selectLikeOrCollectOrCommentList(params, params.getParam());
    }

    /**
     * 后台查询评论列表
     *
     * @param params pageParam
     * @return
     */
    @Override
    public Page<ProRecord> selectCommentList(PageParam<ProRecord> params) {
        return baseMapper.selectCommentList(params, params.getParam());
    }

    /**
     * 回复评论
     *
     * @param record record
     */
    @Override
    public void replyComment(ProRecord record) {
        record.setReplyTime(new Date());
        baseMapper.updateById(record);
    }
}
