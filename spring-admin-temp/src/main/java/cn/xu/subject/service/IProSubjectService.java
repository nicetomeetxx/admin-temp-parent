package cn.xu.subject.service;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.ProSubject;
import cn.xu.subject.entity.vo.WxRecordVo;
import cn.xu.subject.entity.vo.WxSubjectVo;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 首页业务Service接口
 *
 * @author ljxu
 * @date 2023-07-06
 */
public interface IProSubjectService extends IService<ProSubject> {
    /**
     * 查询小程序首页前十条 按排名
     *
     * @return
     */
    List<WxSubjectVo> getWxMainSubject();

    /**
     * 第二个接口 分页查询 下滑分页
     *
     * @param pageParam p
     * @return
     */
    Page<WxSubjectVo> getWxPageSubject(PageParam<WxSubjectVo> pageParam);

    /**
     * 查询详情页评论列表
     *
     * @param id id
     * @return r
     */
    List<WxRecordVo> getWxSubjectRecord(Long id);

    /**
     * 管理台回复小程序端评论内容
     *
     * @param proRecord
     */
    void replayComment(ProRecord proRecord);

    /**
     * 查询我的点赞收藏状态
     *
     * @param id
     * @return
     */
    Map<String, String> getMineLike(Long id);
}
