package cn.xu.subject.dao;

import cn.xu.subject.entity.ProSubject;
import cn.xu.subject.entity.vo.WxSubjectVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 首页业务Mapper接口
 *
 * @author ljxu
 * @date 2023-07-06
 */
public interface ProSubjectDao extends BaseMapper<ProSubject> {
    /**
     * 查询小程序首页十条
     *
     * @return
     */
    List<WxSubjectVo> getWxMainSubject();

    /**
     * 分页查询
     *
     * @param page  page
     * @param param param
     * @return r
     */
    Page<WxSubjectVo> selectByCondition(Page<WxSubjectVo> page, @Param("param") WxSubjectVo param);
}
