package cn.xu.subject.dao;

import cn.xu.subject.entity.UniInterval;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 预约时间区间Mapper接口
 *
 * @author ljxu
 * @date 2023-07-28
 */
public interface UniIntervalDao extends BaseMapper<UniInterval>{

}
