package cn.xu.subject.dao;

import cn.xu.subject.entity.ProNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

public interface ProNoticeDao extends BaseMapper<ProNotice> {
    /**
     * 系统公告
     * @return
     */
    @Select("select count(1) from pro_notice")
    Long selectNoticeNum();
}
