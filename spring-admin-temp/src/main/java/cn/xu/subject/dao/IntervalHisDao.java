package cn.xu.subject.dao;

import cn.xu.subject.entity.IntervalHis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

/**
 * 预约历史Mapper接口
 *
 * @author ljxu
 * @date 2023-07-28
 */
public interface IntervalHisDao extends BaseMapper<IntervalHis> {
    /**
     * 小程序分页查询我的预约列表
     *
     * @param build build
     * @param param param
     * @return r
     */
    Page<IntervalHis> pageList(@Param("page") Page<IntervalHis> build, @Param("param") IntervalHis param);
}
