package cn.xu.subject.dao;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.ProInform;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxInformVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface ProInformDao extends BaseMapper<ProInform> {
    /**
     * 分页查询通知和公告
     *
     * @param page
     * @param param
     * @return
     */
    Page<WxInformVo> selectInformAndNotice(@Param("page") PageParam<ProRecord> page, @Param("param") ProRecord param);

    /**
     * 查询通知数量
     * @return
     */
    @Select("select count(1) from pro_inform where inform_user = #{username} and inform_status = '0'")
    Long selectInformNum(@Param("username") String username);
}
