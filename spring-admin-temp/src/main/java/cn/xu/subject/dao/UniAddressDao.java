package cn.xu.subject.dao;

import cn.xu.subject.entity.UniAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 收货地址Mapper接口
 *
 * @author ljxu
 * @date 2023-08-01
 */
public interface UniAddressDao extends BaseMapper<UniAddress> {

}
