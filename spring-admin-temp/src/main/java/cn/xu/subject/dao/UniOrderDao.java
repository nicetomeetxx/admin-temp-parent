package cn.xu.subject.dao;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.UniOrder;
import cn.xu.subject.entity.vo.WxOrderDotVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 订单记录Mapper接口
 *
 * @author ljxu
 * @date 2023-08-01
 */
public interface UniOrderDao extends BaseMapper<UniOrder> {
    /**
     * 分页查询
     *
     * @param build build
     * @param param param
     * @return r
     */
    PageParam<UniOrder> listPage(@Param("page") Page<UniOrder> build, @Param("param") UniOrder param);

    /**
     * 查询红点
     *
     * @return
     */
    @Select("select uo.order_status name, count(1) count from uni_order uo group by uo.order_status")
    List<WxOrderDotVo> listDto();
}
