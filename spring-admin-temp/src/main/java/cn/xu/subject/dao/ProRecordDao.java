package cn.xu.subject.dao;

import cn.xu.config.result.PageParam;
import cn.xu.subject.entity.ProRecord;
import cn.xu.subject.entity.vo.WxHistoryVo;
import cn.xu.subject.entity.vo.WxRecordVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProRecordDao extends BaseMapper<ProRecord> {
    /**
     * 查询评论列表
     *
     * @param id id
     * @return r
     */
    List<WxRecordVo> selectWxSubjectRecord(@Param("id") Long id);

    /**
     * 分页查询我的收藏点赞评论历史记录
     *
     * @param page
     * @param param
     * @return
     */
    Page<WxHistoryVo> selectLikeOrCollectOrCommentList(@Param("page") Page<ProRecord> page, @Param("param") ProRecord param);

    /**
     * (后台)查询评论列表
     * @param params params
     * @param param param
     * @return r
     */
    Page<ProRecord> selectCommentList(@Param("page") PageParam<ProRecord> params,@Param("param")  ProRecord param);
}
