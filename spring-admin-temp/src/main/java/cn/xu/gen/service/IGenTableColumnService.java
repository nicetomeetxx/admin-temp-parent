package cn.xu.gen.service;


import cn.xu.config.result.PageParam;
import cn.xu.gen.entity.GenTable;
import cn.xu.gen.entity.GenTableColumn;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 业务 服务层
 *
 * @author Lion Li
 */
public interface IGenTableColumnService extends IService<GenTableColumn> {

}
