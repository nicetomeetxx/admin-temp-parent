package cn.xu.gen.service.impl;

import cn.xu.gen.dao.GenTableColumnDao;
import cn.xu.gen.entity.GenTableColumn;
import cn.xu.gen.service.IGenTableColumnService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author xu
 * @create 2023-06-03 13:09
 * @desc
 **/
@Service
public class IGenTableColumnServiceImpl extends ServiceImpl<GenTableColumnDao, GenTableColumn> implements IGenTableColumnService {
}
