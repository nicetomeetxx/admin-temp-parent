package cn.xu.system.dao;

import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.bo.SysRoleBo;
import cn.xu.system.entity.vo.GrantVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.system.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色表(SysRole)表数据库访问层
 *
 * @author ljxu
 * @since 2023-05-01 15:36:18
 */
public interface SysRoleDao extends BaseMapper<SysRole> {

    /**
     * 根据用户id查询角色
     *
     * @param user id
     * @return list
     */
    List<SysRoleBo> selectRoleByUser(@Param("user") SysUser user);

    /**
     * 根据用户id删除
     *
     * @param userId
     */
    void deleteByUserId(@Param("id") Long userId);

    /**
     * 根据用户id添加角色列表
     *
     * @param grant
     */
    void insertUserRole(@Param("grant") GrantVo grant);
}

