package cn.xu.system.dao;

import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.bo.SysMenuBo;
import cn.xu.system.entity.vo.GrantVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.system.entity.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单表(SysMenu)表数据库访问层
 *
 * @author ljxu
 * @since 2023-05-01 15:51:22
 */
public interface SysMenuDao extends BaseMapper<SysMenu> {

    /**
     * 查询所有菜单列表
     *
     * @param user
     * @return
     */
    List<SysMenu> selectMenuByUser(@Param("user") SysUser user);

    /**
     * 根据角色查询菜单列表
     *
     * @param roleId
     * @return
     */
    List<SysMenuBo> selectMenuByRole(@Param("roleId") Long roleId);

    /**
     * 根据角色id删除中间表
     *
     * @param roleId
     */
    void deleteByRoleId(@Param("roleId") Long roleId);

    /**
     * 给角色添加权限
     *
     * @param grant
     */
    void insertRoleMenu(@Param("grant") GrantVo grant);
}

