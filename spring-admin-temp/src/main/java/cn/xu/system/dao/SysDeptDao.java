package cn.xu.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.system.entity.SysDept;

/**
 * 部门表(SysDept)表数据库访问层
 *
 * @author ljxu
 * @since 2023-05-03 16:08:05
 */
public interface SysDeptDao extends BaseMapper<SysDept> {


}

