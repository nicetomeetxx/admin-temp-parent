package cn.xu.system.dao;

import cn.xu.system.entity.eo.SysUserEo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.system.entity.SysUser;

import java.util.List;

/**
 * 用户表(SysUser)表数据库访问层
 *
 * @author ljxu
 * @since 2023-04-25 22:13:14
 */
public interface SysUserDao extends BaseMapper<SysUser> {

    /**
     * 批量新增
     *
     * @param trueList
     */
    void insertExcel(List<SysUserEo> trueList);
}

