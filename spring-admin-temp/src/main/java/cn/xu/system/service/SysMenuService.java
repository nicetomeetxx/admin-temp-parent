package cn.xu.system.service;

import cn.xu.system.entity.bo.SysMenuBo;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xu.system.entity.SysMenu;

import java.util.List;

/**
 * 菜单表(SysMenu)表服务接口
 *
 * @author ljxu
 * @since 2023-05-01 15:51:22
 */
public interface SysMenuService extends IService<SysMenu> {
    /**
     * 构建菜单树
     *
     * @param records
     * @return
     */
    List<SysMenu> makeMenuTree(List<SysMenu> records);

    /**
     * 根据角色查询菜单权限列表
     *
     * @param roleId
     * @return
     */
    List<SysMenuBo> selectByRoleId(Long roleId);
}

