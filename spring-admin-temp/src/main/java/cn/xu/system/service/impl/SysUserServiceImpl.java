package cn.xu.system.service.impl;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.xu.config.constant.CommonConstants;
import cn.xu.config.exception.BaseException;
import cn.xu.config.exception.ErrorType;
import cn.xu.config.result.SysCheck;
import cn.xu.config.utils.ExcelUtils;
import cn.xu.config.utils.FileUtils;
import cn.xu.config.utils.IPUtils;
import cn.xu.config.utils.MenuUtils;
import cn.xu.system.dao.SysMenuDao;
import cn.xu.system.dao.SysRoleDao;
import cn.xu.system.dao.SysUserDao;
import cn.xu.system.entity.SysMenu;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.bo.SysRoleBo;
import cn.xu.system.entity.eo.SysUserEo;
import cn.xu.system.entity.vo.GrantVo;
import cn.xu.system.entity.vo.RouterVo;
import cn.xu.system.entity.vo.SysUserVo;
import cn.xu.system.entity.vo.WxUserVo;
import cn.xu.system.service.SysUserService;
import cn.xu.tools.logs.dao.SysLogsLoginDao;
import cn.xu.tools.logs.entity.SysLogsLogin;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 用户表(SysUser)表服务实现类
 *
 * @author ljxu
 * @since 2023-04-25 22:26:50
 */
@Slf4j
@Service("sysUserService")
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserDao, SysUser> implements SysUserService {

    private final SysRoleDao sysRoleDao;
    private final SysMenuDao sysMenuDao;
    private final SysLogsLoginDao logsLoginDao;

    @Value("${sys.init.password}")
    private String defaultPassword;

    private final List<SysUserEo> errList = new ArrayList<>();

    /**
     * 登录
     *
     * @param username 账号
     * @param password 密码
     */
    @Override
    public void login(String username, String password) {
        //1.根据加密规则判断用户名密码是否正确
        SysUser user = baseMapper.selectOne(new QueryWrapper<SysUser>().eq("username", username));
        //获取数据库保存的密码
        String passwordData = user.getPassword();
        //前端输入的密码加密后跟数据库比较判断是否正确
        if (BCrypt.checkpw(password, passwordData)) {
            //2.saToken登录
            StpUtil.login(username);
            //4.记录到日志
            recordLoginLog(user);
        } else {
            throw new BaseException(ErrorType.USER_PASSWORD_ERROR);
        }
    }

    /**
     * 记录登录日志
     *
     * @param user user
     */
    private void recordLoginLog(SysUser user) {
        HttpServletRequest request = ((ServletRequestAttributes) (Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))).getRequest();
        //3.记录到登录日志
        Date date = new Date();
        SysUser loginLog = new SysUser().setId(user.getId()).setLoginTime(date);
        baseMapper.updateById(loginLog);
        SysLogsLogin logsLogin = new SysLogsLogin();
        String ipAddr = IPUtils.getIpAddr(request);
        logsLogin.setUserName(user.getUsername()).setIpAddr(ipAddr).setLoginTime(date).setLoginMac(IPUtils.getBrowserName(request));
        logsLoginDao.insert(logsLogin);
        log.info("[Login]username:{},ip:{}", user.getUsername(), ipAddr);
    }

    /**
     * 根据token获取用户信息
     *
     * @param token token
     * @return
     */
    @Override
    public SysUserVo getInfo(String token) {
        String username = (String) StpUtil.getLoginIdByToken(token);
        if (ObjectUtils.isEmpty(username)) throw new BaseException(ErrorType.USER_TOKEN_ERROR);
        //根据username查询登录的用户信息
        SysUser sysUser = baseMapper.selectOne(new QueryWrapper<SysUser>().eq("username", username));
        //根据用户查询角色列表及权限信息
        List<SysRoleBo> roleBoList = sysRoleDao.selectRoleByUser(sysUser);
        //查询菜单按钮列表，结构成权限和菜单树
        List<SysMenu> menuList = sysMenuDao.selectMenuByUser(sysUser);
        List<String> authList = menuList.stream().filter(Objects::nonNull).map(SysMenu::getMenuAuth).collect(Collectors.toList());
        List<SysMenu> menus = menuList.stream().distinct().filter(Objects::nonNull).filter(item -> !CommonConstants.MENU_TYPE_2.equals(item.getMenuType())).collect(Collectors.toList());
        List<RouterVo> routerList = MenuUtils.makeRouter(menus, 0L);

        SysUserVo userVo = new SysUserVo();
        userVo.setId(sysUser.getId());
        userVo.setName(sysUser.getName());
        userVo.setAvatar(sysUser.getAvatar());
        userVo.setRoles(roleBoList);
        userVo.setAuth(authList);
        userVo.setRoutes(routerList);
        return userVo;
    }

    /**
     * uniapp获取用户信息
     *
     * @param token token
     * @return
     */
    @Override
    public WxUserVo getWxInfo(String token) {
        String username = (String) StpUtil.getLoginIdByToken(token);
        SysUser sysUser = baseMapper.selectOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, username));
        WxUserVo wxUserVo = new WxUserVo();
        BeanUtils.copyProperties(sysUser, wxUserVo);
        return wxUserVo;
    }

    /**
     * 新增修改数据校验
     *
     * @param user user
     * @return
     */
    @Override
    public SysCheck beforeInsertOrUpdate(SysUser user, String method) {
        SysCheck check = new SysCheck();
        boolean usernameFlag = baseMapper.exists(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUsername, user.getUsername())
                .ne(ObjectUtil.isNotNull(user.getId()), SysUser::getId, user.getId()));
        if (usernameFlag) {
            check.setFlag(false);
            if ("insert".equals(method))
                check.setData("新增用户失败，" + user.getUsername() + "用户名已存在");
            else
                check.setData("修改用户失败，" + user.getUsername() + "用户名已存在");
            return check;
        }
        boolean telephoneFlag = baseMapper.exists(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getTelephone, user.getTelephone())
                .ne(ObjectUtil.isNotNull(user.getId()), SysUser::getId, user.getId()));
        if (telephoneFlag) {
            check.setFlag(false);
            if ("insert".equals(method))
                check.setData("新增用户失败，" + user.getTelephone() + "手机号已存在");
            else
                check.setData("修改用户失败，" + user.getTelephone() + "手机号已存在");
            return check;
        }
        check.setFlag(true);
        return check;
    }

    /**
     * 根据用户id赋予角色
     *
     * @param grantVo
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean grantRole(GrantVo grantVo) {
        //先删除原角色
        sysRoleDao.deleteByUserId(grantVo.getId());
        //再新增新角色
        if (CollUtil.isNotEmpty(grantVo.getRoleIds())) {
            sysRoleDao.insertUserRole(grantVo);
        }
        return true;
    }

    /**
     * 用户导入导出模板
     *
     * @param request
     * @param response
     */
    @SneakyThrows
    @Override
    public void exportTemplate(HttpServletRequest request, HttpServletResponse response) {
        ClassPathResource resource = new ClassPathResource("excel/userTemplate.xlsx");
        File file = resource.getFile();
        Assert.state(file.exists(), "文件不存在");
        FileUtils.downloadFile(file, "test", response);
    }

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    @SneakyThrows
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean importExcel(MultipartFile file, HttpServletResponse response) {
        // 保存正确数据
        List<SysUserEo> trueList = new ArrayList<>();

        //单sheet导入，读取excel
        List<SysUserEo> userPoi = ExcelUtils.importFileExcel(file, SysUserEo.class);

        //校验excel是否为空
        if (CollectionUtils.isEmpty(userPoi)) {
            return true;
        }
        checkExcelField(userPoi, trueList, errList);
        trueList.forEach(item -> item.setPassword(BCrypt.hashpw(defaultPassword))); //初始化默认密码
        //4.批量新增
        if (!CollectionUtils.isEmpty(trueList)) {
            try {
                baseMapper.insertExcel(trueList);
            } catch (Exception e) {
                SysUserEo user = new SysUserEo();
                user.setErrorMsg("批量导入错误,请检查数据是否重复");
                errList.add(user);
                throw new BaseException(ErrorType.SYSTEM_ERROR);
            }
            return CollUtil.isEmpty(errList);
        }
        return false;
    }

    /**
     * 校验excel文件
     *
     * @param userList
     * @param trueList
     * @param errList
     */
    private void checkExcelField(List<SysUserEo> userList, List<SysUserEo> trueList, List<SysUserEo> errList) {
        for (SysUserEo sysUserEo : userList) {
            if (ObjectUtils.isEmpty(sysUserEo.getUsername())) {
                sysUserEo.setErrorMsg("username is null; ");
                errList.add(sysUserEo);
            } else if (ObjectUtils.isEmpty(sysUserEo.getTelephone())) {
                sysUserEo.setErrorMsg(sysUserEo.getErrorMsg() + "telephone is null;");
                errList.add(sysUserEo);
            } else {
                trueList.add(sysUserEo);
            }
        }

    }

    /**
     * 导出错误数据
     *
     * @param response
     */
    @SneakyThrows
    @Override
    public void exportErrorMessage(HttpServletResponse response) {
        // 判断errList是否有数据，有数据写到excel中，返回excel
        if (!CollectionUtils.isEmpty(errList)) {
            ExportParams empExportParams = new ExportParams();
            empExportParams.setSheetName("error");
            empExportParams.setType(ExcelType.XSSF);  //这里HSSF格式报错
            Workbook workbook = ExcelExportUtil.exportExcel(empExportParams, SysUserEo.class, errList);
            ExcelUtils.downLoadExcel("errData", response, workbook);
        }
    }

    /**
     * 导出
     *
     * @param response
     */
    @SneakyThrows
    @Override
    public void exportExcel(HttpServletResponse response) {
        List<SysUser> sysUsers = baseMapper.selectList(new LambdaQueryWrapper<SysUser>().orderByDesc(SysUser::getCreateTime));
        int i = 1;
        List<SysUserEo> result = new ArrayList<>();
        for (SysUser loginUser : sysUsers) {
            SysUserEo userPoi = new SysUserEo();
            BeanUtils.copyProperties(loginUser, userPoi);
            userPoi.setTid(i++);//手动改变导出序号
            result.add(userPoi);
        }
        ExcelUtils.defaultExport(result, SysUserEo.class, "userList", response, new ExportParams(null, "用户数据"));
    }
}

