package cn.xu.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.xu.system.dao.SysMenuDao;
import cn.xu.system.dao.SysRoleDao;
import cn.xu.system.entity.SysRole;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.bo.SysRoleBo;
import cn.xu.system.entity.vo.GrantVo;
import cn.xu.system.service.SysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;


/**
 * 角色表(SysRole)表服务实现类
 *
 * @author ljxu
 * @since 2023-05-01 15:36:18
 */
@Service("sysRoleService")
@RequiredArgsConstructor
public class SysRoleServiceImpl extends ServiceImpl<SysRoleDao, SysRole> implements SysRoleService {
    private final SysMenuDao sysMenuDao;

    /**
     * 根据用户id查询所属角色
     *
     * @param uid
     * @return
     */
    @Override
    public List<SysRoleBo> selectByUserId(Long uid) {
        return this.baseMapper.selectRoleByUser(new SysUser().setId(uid));
    }

    /**
     * 根据角色id添加权限
     *
     * @param grantVo
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean grantMenu(GrantVo grantVo) {
        //先删除原角色
        sysMenuDao.deleteByRoleId(grantVo.getRoleId());
        //再新增新角色
        if (CollUtil.isNotEmpty(grantVo.getMenuIds())) {
            //防止数据重复去重
            List<Long> menuIds = grantVo.getMenuIds();
            menuIds = menuIds.stream().distinct().collect(Collectors.toList());
            grantVo.setMenuIds(menuIds);
            sysMenuDao.insertRoleMenu(grantVo);
        }
        return true;
    }
}

