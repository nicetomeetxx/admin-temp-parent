package cn.xu.system.service;

import cn.xu.system.entity.bo.SysRoleBo;
import cn.xu.system.entity.vo.GrantVo;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xu.system.entity.SysRole;

import java.util.List;

/**
 * 角色表(SysRole)表服务接口
 *
 * @author ljxu
 * @since 2023-05-01 15:36:18
 */
public interface SysRoleService extends IService<SysRole> {
    /**
     * 根据用户id查询所属角色
     *
     * @param uid
     * @return
     */
    List<SysRoleBo> selectByUserId(Long uid);

    /**
     * 根据角色id添加权限
     *
     * @param grantVo
     * @return
     */
    Boolean grantMenu(GrantVo grantVo);
}

