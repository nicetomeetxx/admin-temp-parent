package cn.xu.system.service;

import cn.hutool.http.server.HttpServerRequest;
import cn.hutool.http.server.HttpServerResponse;
import cn.xu.config.result.SysCheck;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.vo.GrantVo;
import cn.xu.system.entity.vo.SysUserVo;
import cn.xu.system.entity.vo.WxUserVo;
import com.baomidou.mybatisplus.extension.service.IService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

/**
 * 用户表(SysUser)表服务接口
 *
 * @author ljxu
 * @since 2023-04-25 22:26:50
 */
public interface SysUserService extends IService<SysUser> {
    /**
     * 登录
     *
     * @param username 账号
     * @param password 密码
     */
    void login(String username, String password);

    /**
     * 根据token获取用户信息
     *
     * @param token token
     * @return
     */
    SysUserVo getInfo(String token);

    /**
     * 新增修改数据校验
     *
     * @param sysUser user
     * @return
     */
    SysCheck beforeInsertOrUpdate(SysUser sysUser, String method);

    /**
     * 根据用户id赋予角色
     *
     * @param grantVo
     * @return
     */
    Boolean grantRole(GrantVo grantVo);

    /**
     * 用户导入导出模板
     *
     * @param request
     * @param response
     */
    void exportTemplate(HttpServletRequest request, HttpServletResponse response);

    /**
     * 导入
     *
     * @param file
     * @param response
     * @return
     */
    boolean importExcel(MultipartFile file, HttpServletResponse response);

    /**
     * 导出错误数据
     *
     * @param response
     */
    void exportErrorMessage(HttpServletResponse response);

    /**
     * 导出
     *
     * @param response
     */
    void exportExcel(HttpServletResponse response);

    /**
     * uniapp登录获取用户信息
     *
     * @param token token
     * @return r
     */
    WxUserVo getWxInfo(String token);
}

