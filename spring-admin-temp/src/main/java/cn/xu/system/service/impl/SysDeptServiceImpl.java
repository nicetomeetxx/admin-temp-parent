package cn.xu.system.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.tree.Tree;
import cn.xu.config.utils.TreeUtils;
import cn.xu.system.dao.SysDeptDao;
import cn.xu.system.entity.SysDept;
import cn.xu.system.service.SysDeptService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * 部门表(SysDept)表服务实现类
 *
 * @author ljxu
 * @since 2023-05-03 16:08:05
 */
@Service("sysDeptService")
public class SysDeptServiceImpl extends ServiceImpl<SysDeptDao, SysDept> implements SysDeptService {
    /**
     * 查询部门树
     *
     * @return
     */
    @Override
    public List<Tree<Long>> selectTree() {
        LambdaQueryWrapper<SysDept> lqw = new LambdaQueryWrapper<>();
        List<SysDept> deptList = baseMapper.selectList(lqw);
        if (CollUtil.isEmpty(deptList)) {
            return CollUtil.newArrayList();
        }
        return TreeUtils.build(deptList, (dep, tree) ->
                tree.setId(dep.getDeptId())
                        .setParentId(dep.getPDeptId())
                        .setName(dep.getDeptName())
                        .setWeight(dep.getIsSort()), "pDeptId");
    }

    /**
     * 构建部门树
     *
     * @param records
     * @return
     */
    @Override
    public List<SysDept> makeDeptTree(List<SysDept> records) {
        List<SysDept> deptList = new ArrayList<>();
        // 最后的结果 先找到所有的一级菜单
        for (int i = 0; i < records.size(); i++) {
            // 一级菜单没有parentId
            if (records.get(i).getPDeptId() == null || records.get(i).getPDeptId() == 0) {
                deptList.add(records.get(i));
            }
            // 为一级菜单设置子菜单，是递归调用的
            for (SysDept dept : deptList) {
                dept.setChildren(getDeptChild(dept.getDeptId(), records));
            }
        }
        return deptList;
    }

    /**
     * 递归查询所有子部门
     *
     * @param id
     * @param records
     * @return
     */
    private List<SysDept> getDeptChild(Long id, List<SysDept> records) {
        // 子菜单
        List<SysDept> childList = new ArrayList<>();
        for (SysDept dept : records) {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (dept.getPDeptId() != null && dept.getPDeptId().equals(id)) {
                childList.add(dept);
            }
        }
        // 把子菜单的子菜单再循环一遍
        childList.forEach(dept -> dept.setChildren(getDeptChild(dept.getDeptId(), records)));
        // 递归退出条件
        if (childList.isEmpty()) {
            return Collections.emptyList();
        }
        return childList;
    }
}

