package cn.xu.system.service.impl;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.convert.Convert;
import cn.xu.config.utils.UserUtils;
import cn.xu.system.dao.SysMenuDao;
import cn.xu.system.dao.SysRoleDao;
import cn.xu.system.entity.SysMenu;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.bo.SysRoleBo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class SysAuthServiceImpl implements StpInterface {

    private final SysRoleDao sysRoleDao;
    private final SysMenuDao sysMenuDao;

    /**
     * 权限列表
     *
     * @param userId
     * @param userType
     * @return
     */
    @Override
    public List<String> getPermissionList(Object userId, String userType) {
        //查询菜单按钮列表，结构成权限和菜单树
        SysUser sysUser = new SysUser();
        sysUser.setUsername(Convert.toStr(userId));
        List<SysMenu> menuList = sysMenuDao.selectMenuByUser(sysUser);
        return menuList.stream().filter(Objects::nonNull).map(SysMenu::getMenuAuth).collect(Collectors.toList());
    }

    /**
     * 角色列表
     *
     * @param userId
     * @param userType
     * @return
     */
    @Override
    public List<String> getRoleList(Object userId, String userType) {
        //根据用户查询角色列表及权限信息
        SysUser sysUser = new SysUser();
        sysUser.setUsername(Convert.toStr(userId));
        List<SysRoleBo> roleBoList = sysRoleDao.selectRoleByUser(sysUser);
        return roleBoList.stream().map(SysRoleBo::getRoleAuth).collect(Collectors.toList());
    }

    /**
     * 校验是否包含某个角色
     *
     * @param username
     * @param roleAuth
     * @return
     */
    public boolean checkRole(String username, String roleAuth) {
        SysUser sysUser = UserUtils.selectUserIdByUserName(username);
        List<String> roleList = getRoleList(sysUser.getId(), "");
        return roleList.contains(roleAuth);
    }


    /**
     * 校验是否管理员
     *
     * @param username
     * @return
     */
    public boolean checkRoleAdmin(String username) {
        SysUser sysUser = UserUtils.selectUserIdByUserName(username);
        List<String> roleList = getRoleList(sysUser.getId(), "");
        return roleList.contains("system");
    }
}
