package cn.xu.system.service.impl;

import cn.xu.system.entity.bo.SysMenuBo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.system.dao.SysMenuDao;
import cn.xu.system.entity.SysMenu;
import cn.xu.system.service.SysMenuService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * 菜单表(SysMenu)表服务实现类
 *
 * @author ljxu
 * @since 2023-05-01 15:51:22
 */
@Service("sysMenuService")
public class SysMenuServiceImpl extends ServiceImpl<SysMenuDao, SysMenu> implements SysMenuService {


    /**
     * 获取权限树
     *
     * @param records
     * @return
     */
    public List<SysMenu> makeMenuTree(List<SysMenu> records) {
        List<SysMenu> menuList = new ArrayList<>();
        // 最后的结果 先找到所有的一级菜单
        for (int i = 0; i < records.size(); i++) {
            // 一级菜单没有parentId
            if (records.get(i).getPMenuId() == null || records.get(i).getPMenuId() == 0) {
                menuList.add(records.get(i));
            }
            // 为一级菜单设置子菜单，是递归调用的
            for (SysMenu menu : menuList) {
                menu.setChildren(getPermissionChild(menu.getMenuId(), records));
            }
        }
        return menuList;
    }

    /**
     * 递归查询所有子菜单:权限树子节点
     *
     * @param id
     * @param records
     * @return
     */
    private List<SysMenu> getPermissionChild(Long id, List<SysMenu> records) {
        // 子菜单
        List<SysMenu> childList = new ArrayList<>();
        for (SysMenu menu : records) {
            // 遍历所有节点，将父菜单id与传过来的id比较
            if (menu.getPMenuId() != null && menu.getPMenuId().equals(id)) {
                childList.add(menu);
            }
        }
        // 把子菜单的子菜单再循环一遍
        childList.forEach(menu -> menu.setChildren(getPermissionChild(menu.getMenuId(), records)));
        // 递归退出条件
        if (childList.isEmpty()) {
            return Collections.emptyList();
        }
        return childList;
    }

    /**
     * 根据角色查询菜单权限列表
     *
     * @param roleId
     * @return
     */
    @Override
    public List<SysMenuBo> selectByRoleId(Long roleId) {
        return this.baseMapper.selectMenuByRole(roleId);
    }
}

