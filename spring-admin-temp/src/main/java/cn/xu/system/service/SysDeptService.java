package cn.xu.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.xu.system.entity.SysDept;

import java.util.List;

/**
 * 部门表(SysDept)表服务接口
 *
 * @author ljxu
 * @since 2023-05-03 16:08:05
 */
public interface SysDeptService extends IService<SysDept> {
    /**
     * 查询部门树
     *
     * @return
     */
    List<Tree<Long>> selectTree();

    /**
     * 构建部门树
     *
     * @param records
     * @return
     */
    List<SysDept> makeDeptTree(List<SysDept> records);
}

