package cn.xu.system.controller;


import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.system.entity.vo.GrantVo;
import cn.xu.system.service.SysMenuService;
import cn.xu.system.service.SysRoleService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import cn.xu.system.entity.SysRole;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 角色表(SysRole)表控制层
 *
 * @author ljxu
 * @since 2023-05-01 15:36:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("sysRole")
public class SysRoleController {
    /**
     * 服务对象
     */
    private final SysRoleService sysRoleService;
    private final SysMenuService sysMenuService;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysRole>> selectAll(@RequestBody PageParam<SysRole> params) {
        return R.ok(sysRoleService.page(params, new QueryWrapper<>(params.getParam()).orderByAsc("is_sort")));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R<SysRole> selectOne(@PathVariable Serializable id) {
        SysRole role = this.sysRoleService.getById(id);
        //根据用户id查询角色列表
        role.setMenuList(sysMenuService.selectByRoleId(role.getRoleId()));
        return R.ok(role);
    }

    /**
     * 新增数据
     *
     * @param sysRole 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R<Boolean> insert(@RequestBody SysRole sysRole) {
        return R.ok(this.sysRoleService.save(sysRole));
    }

    /**
     * 修改数据
     *
     * @param sysRole 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R<Boolean> update(@RequestBody SysRole sysRole) {
        return R.ok(this.sysRoleService.updateById(sysRole));
    }

    /**
     * 删除数据
     *
     * @param id 单个删除
     * @return 删除结果
     */
    @DeleteMapping("{id}")
    public R<Boolean> delete(@PathVariable("id") Long id) {
        return R.ok(this.sysRoleService.removeById(id));
    }

    /**
     * 批量删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R<Boolean> delete(@RequestBody List<Long> idList) {
        return R.ok(this.sysRoleService.removeByIds(idList));
    }

    /**
     * 根据角色id赋予权限
     *
     * @param grantVo grantVo
     * @return
     */
    @PostMapping("grant")
    public R<Boolean> grant(@RequestBody GrantVo grantVo) {
        return R.ok(sysRoleService.grantMenu(grantVo));
    }
}

