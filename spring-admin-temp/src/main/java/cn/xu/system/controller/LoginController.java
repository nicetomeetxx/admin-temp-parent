package cn.xu.system.controller;


import cn.dev33.satoken.stp.StpUtil;
import cn.xu.config.result.R;
import cn.xu.config.utils.AesUtils;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.vo.SysUserVo;
import cn.xu.system.entity.vo.WxUserVo;
import cn.xu.system.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 登录(SysUser)表控制层
 *
 * @author ljxu
 * @since 2023-04-25 22:26:49
 */
@RestController
@RequiredArgsConstructor
public class LoginController {

    private final SysUserService userService;

    /**
     * 登录
     *
     * @return R
     */
    @PostMapping("login")
    public R login(@RequestBody SysUser user) {
        //1.校验用户名密码是否正确
        userService.login(user.getUsername(), AesUtils.decrypt(user.getPassword()));
        //3.返回token给前端
        Map<String, Object> map = new HashMap<>();
        map.put("token", StpUtil.getTokenInfo().tokenValue);
        return R.ok().put("data", map);
    }

    /**
     * 获取用户基本信息：用户名 头像 权限 菜单
     *
     * @param token token
     * @return R
     */
    @GetMapping("info")
    public R<Map<String, Object>> info(String token) {
        SysUserVo user = userService.getInfo(token);
        Map<String, Object> data = new HashMap<>();
        data.put("data", user);
        return R.ok(data);
    }

    /**
     * 获取用户基本信息：用户名 头像 权限 菜单
     *
     * @param token token
     * @return R
     */
    @GetMapping("wxInfo")
    public R<Map<String, Object>> wxInfo(String token) {
        WxUserVo user = userService.getWxInfo(token);
        Map<String, Object> data = new HashMap<>();
        data.put("data", user);
        return R.ok(data);
    }

    /**
     * 退出登录
     *
     * @return R
     */
    @PostMapping("logout")
    public R<Map<String, Object>> logout() {
        return R.ok();
    }

}
