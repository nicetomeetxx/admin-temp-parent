package cn.xu.system.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.secure.BCrypt;
import cn.dev33.satoken.stp.StpUtil;
import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.result.SysCheck;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.vo.GrantVo;
import cn.xu.system.service.SysRoleService;
import cn.xu.system.service.SysUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户表(SysUser)表控制层
 *
 * @author ljxu
 * @since 2023-04-25 22:26:49
 */
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("sysUser")
public class SysUserController {
    /**
     * 服务对象
     */
    private final SysUserService sysUserService;
    private final SysRoleService sysRoleService;

    @Value("${sys.init.password}")
    private String defaultPassword;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysUser>> selectAll(@RequestBody PageParam<SysUser> params) {
        PageParam<SysUser> page = this.sysUserService.page(params, new QueryWrapper<>(params.getParam()).orderByAsc("create_time"));
        List<String> strings = StpUtil.searchSessionId("", 0, -1, false);
        List<SysUser> collect = page.getRecords().stream().peek(sysUser -> {
            strings.forEach(item -> {
                if (item.contains(sysUser.getUsername())) {
                    sysUser.setIsOnline("1");
                }
            });
        }).collect(Collectors.toList());
        return R.ok(page.setRecords(collect));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R<SysUser> selectOne(@PathVariable Serializable id) {
        SysUser user = this.sysUserService.getById(id);
        //根据用户id查询角色列表
        user.setRoleList(sysRoleService.selectByUserId(user.getId()));
        return R.ok(user);
    }

    /**
     * 新增数据
     *
     * @param sysUser 实体对象
     * @return 新增结果
     */
    @PostMapping
    @Log(title = "新增用户数据", businessType = BusinessType.INSERT)
    @SaCheckPermission("system:user:insert")
    public R<Boolean> insert(@RequestBody SysUser sysUser) {
        SysCheck check = sysUserService.beforeInsertOrUpdate(sysUser, "insert");
        if (Boolean.FALSE.equals(check.getFlag())) {
            return R.error(check.getData());
        }
        sysUser.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        sysUser.setPassword(BCrypt.hashpw(defaultPassword));
        return R.ok(this.sysUserService.save(sysUser));
    }

    /**
     * 修改数据
     *
     * @param sysUser 实体对象
     * @return 修改结果
     */
    @PutMapping
    @Log(title = "修改用户数据", businessType = BusinessType.UPDATE)
    public R<Boolean> update(@RequestBody SysUser sysUser) {
        SysCheck check = sysUserService.beforeInsertOrUpdate(sysUser, "update");
        if (Boolean.FALSE.equals(check.getFlag())) {
            return R.error(check.getData());
        }
        sysUser.setUpdateTime(new Date());//设置修改时间
        return R.ok(this.sysUserService.updateById(sysUser));
    }

    /**
     * 删除数据
     *
     * @param id 单个删除
     * @return 删除结果
     */
    @DeleteMapping("{id}")
    @Log(title = "删除用户数据", businessType = BusinessType.DELETE)
    public R<Boolean> delete(@PathVariable("id") Long id) {
        return R.ok(this.sysUserService.removeById(id));
    }

    /**
     * 批量删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    @Log(title = "删除用户数据", businessType = BusinessType.DELETE)
    public R<Boolean> delete(@RequestBody List<Long> idList) {
        List<Long> list = idList.stream().filter(e -> 1L != e).collect(Collectors.toList());
        return R.ok(this.sysUserService.removeByIds(list));
    }

    /**
     * 根据用户id赋予权限
     *
     * @param grantVo grantVo
     * @return
     */
    @PostMapping("grant")
    public R<Boolean> grant(@RequestBody GrantVo grantVo) {
        return R.ok(sysUserService.grantRole(grantVo));
    }

    /**
     * 用户导入导出模板
     *
     * @param request  request
     * @param response response
     * @return
     */
    @PostMapping("exportTemplate")
    public void exportTemplate(HttpServletRequest request, HttpServletResponse response) {
        sysUserService.exportTemplate(request, response);
    }

    /**
     * 用户信息导入
     * consumes = MediaType.MULTIPART_FORM_DATA_VALUE
     *
     * @param file
     * @throws Exception
     */
    @RequestMapping(value = "import")
    public R importExcel(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        if (sysUserService.importExcel(file, response)) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    /**
     * 错误信息导出
     *
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "exportError")
    public void exportErrorMessage(HttpServletResponse response) {
        sysUserService.exportErrorMessage(response);
    }

    /**
     * 导出
     *
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "export")
    public void exportExcel(HttpServletResponse response) {
        sysUserService.exportExcel(response);
    }

    /**
     * 获取系统用户数量
     */
    @RequestMapping(value = "number")
    public R<Long> userNumber() {
        return R.ok(sysUserService.count(new LambdaQueryWrapper<SysUser>()));
    }

}

