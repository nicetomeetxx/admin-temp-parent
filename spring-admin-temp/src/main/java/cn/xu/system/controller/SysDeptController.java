package cn.xu.system.controller;


import cn.hutool.core.lang.tree.Tree;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.system.entity.SysDept;
import cn.xu.system.service.SysDeptService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 部门表(SysDept)表控制层
 *
 * @author ljxu
 * @since 2023-05-03 16:08:05
 */
@RestController
@RequestMapping("sysDept")
@RequiredArgsConstructor
public class SysDeptController {
    /**
     * 服务对象
     */
    private final SysDeptService sysDeptService;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysDept>> selectAll(@RequestBody PageParam<SysDept> params) {
        SysDept requestParam = params.getParam();
        Page<SysDept> page = this.sysDeptService.page(params, new LambdaQueryWrapper<SysDept>()
                .like(ObjectUtils.isNotEmpty(requestParam.getDeptName()), SysDept::getDeptName, requestParam.getDeptName())
                .like(ObjectUtils.isNotEmpty(requestParam.getLeader()), SysDept::getLeader, requestParam.getLeader())
                .orderByAsc(SysDept::getIsSort));
        List<SysDept> deptList = sysDeptService.makeDeptTree(page.getRecords());
        page.setRecords(deptList);
        return R.ok(page);
    }

    /**
     * 查询部门树结构
     *
     * @return 所有数据
     */
    @PostMapping("tree")
    public R<List<Tree<Long>>> selectTree() {
        return R.ok(this.sysDeptService.selectTree());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R<SysDept> selectOne(@PathVariable Serializable id) {
        return R.ok(this.sysDeptService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param sysDept 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R<Boolean> insert(@RequestBody SysDept sysDept) {
        return R.ok(this.sysDeptService.save(sysDept));
    }

    /**
     * 修改数据
     *
     * @param sysDept 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R<Boolean> update(@RequestBody SysDept sysDept) {
        return R.ok(this.sysDeptService.updateById(sysDept));
    }

    /**
     * 删除数据
     *
     * @param id 单个删除
     * @return 删除结果
     */
    @DeleteMapping("{id}")
    public R<Boolean> delete(@PathVariable("id") Serializable id) {
        return R.ok(this.sysDeptService.removeById(id));
    }

    /**
     * 批量删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R<Boolean> delete(@RequestBody List<Long> idList) {
        return R.ok(this.sysDeptService.removeByIds(idList));
    }
}

