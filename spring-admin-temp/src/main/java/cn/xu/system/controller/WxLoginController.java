package cn.xu.system.controller;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.ShearCaptcha;
import cn.xu.config.result.R;
import cn.xu.config.utils.AesUtils;
import cn.xu.system.entity.SysUser;
import cn.xu.system.entity.vo.RegisterVo;
import cn.xu.system.service.SysUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/wx/user")
@RequiredArgsConstructor
public class WxLoginController {
    private final SysUserService userService;

    /**
     * 注册
     *
     * @return
     */
    @PostMapping("/register")
    public R<Void> register(@RequestBody RegisterVo register) {
        //1.先查询账号是否存在
        SysUser one = userService.getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, register.getUsername()));
        if (ObjectUtils.isNotEmpty(one)) {
            return R.error("用户名已存在");
        }
        SysUser user = new SysUser();
        user.setUsername(register.getUsername());
        user.setName(String.valueOf(System.currentTimeMillis()));
        user.setTelephone(register.getUsername());
        //2.密码解密后重写加密
        String decrypt = AesUtils.decrypt(register.getPassword());
        String hashpw = BCrypt.hashpw(decrypt);
        boolean checkpw = BCrypt.checkpw(decrypt, hashpw);
        log.info("check password :{}", checkpw);
        user.setPassword(hashpw);
        //3.添加默认值到user,默认角色
        user.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        //4.保存数据
        user.insert();
        return R.ok();
    }

    /**
     * 获取验证码
     *
     * @return r
     */
    @GetMapping("captcha")
    public R<Map<String, String>> captcha() {
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(102, 38);
        String imageBase64 = captcha.getImageBase64();
        String code = captcha.getCode();
        Map<String, String> data = new HashMap<>();
        data.put("base64", "data:image/png;base64," + imageBase64);
        data.put("text", code);
        return R.ok(data);
    }

}
