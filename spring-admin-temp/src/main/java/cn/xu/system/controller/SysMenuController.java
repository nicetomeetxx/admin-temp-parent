package cn.xu.system.controller;


import cn.xu.config.constant.CommonConstants;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.system.entity.SysMenu;
import cn.xu.system.service.SysMenuService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * 菜单表(SysMenu)表控制层
 *
 * @author ljxu
 * @since 2023-05-01 15:51:22
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("sysMenu")
public class SysMenuController {
    /**
     * 服务对象
     */
    private final SysMenuService sysMenuService;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysMenu>> selectAll(@RequestBody PageParam<SysMenu> params) {
        SysMenu requestParam = params.getParam();
        Page<SysMenu> page = this.sysMenuService.page(params, new LambdaQueryWrapper<>(requestParam)
                .eq(ObjectUtils.isNotEmpty(requestParam) && ObjectUtils.isNotEmpty(requestParam.getMenuTypeFlag()), SysMenu::getMenuType, requestParam.getMenuTypeFlag())
                .orderByAsc(SysMenu::getIsSort));
        if (ObjectUtils.isNotEmpty(requestParam) && CommonConstants.MENU_TYPE_0.equals(requestParam.getMenuTypeFlag())) {
            List<SysMenu> childList = sysMenuService.list(new LambdaQueryWrapper<SysMenu>().ne(SysMenu::getPMenuId, CommonConstants.MENU_TYPE_0));
            List<SysMenu> records = page.getRecords();
            records.addAll(childList);
            List<SysMenu> menuList = sysMenuService.makeMenuTree(records);
            page.setRecords(menuList);
        }
        return R.ok(page);
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("{id}")
    public R<SysMenu> selectOne(@PathVariable Serializable id) {
        return R.ok(this.sysMenuService.getById(id));
    }

    /**
     * 新增数据
     *
     * @param sysMenu 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R<Boolean> insert(@RequestBody SysMenu sysMenu) {
        return R.ok(this.sysMenuService.save(sysMenu));
    }

    /**
     * 修改数据
     *
     * @param sysMenu 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R<Boolean> update(@RequestBody SysMenu sysMenu) {
        return R.ok(this.sysMenuService.updateById(sysMenu));
    }

    /**
     * 删除数据
     *
     * @param id 单个删除
     * @return 删除结果
     */
    @DeleteMapping("{id}")
    public R<Boolean> delete(@PathVariable("id") Serializable id) {
        return R.ok(this.sysMenuService.removeById(id));
    }

    /**
     * 批量删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R<Boolean> delete(@RequestBody List<Long> idList) {
        return R.ok(this.sysMenuService.removeByIds(idList));
    }

    /**
     * 查询部门是否隐藏
     *
     * @return 单条数据
     */
    @GetMapping("dept")
    public R selectDeptHidden() {
        SysMenu one = this.sysMenuService.getOne(new LambdaQueryWrapper<SysMenu>().eq(SysMenu::getUrl, "/system/dept"));
        return R.put(one.getIsHidden());
    }
}

