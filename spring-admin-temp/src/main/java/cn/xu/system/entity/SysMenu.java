package cn.xu.system.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 菜单表(SysMenu)表实体类
 *
 * @author ljxu
 * @since 2023-05-01 15:51:23
 */
@Data
@SuppressWarnings("serial")
public class SysMenu extends Model<SysMenu> {
    //主键
    @TableId(type = IdType.AUTO)
    private Long menuId;
    //父权限id
    private Long pMenuId;
    //权限名称
    private String menuName;
    //权限路径
    private String url;
    //权限标识
    private String menuAuth;
    //是否隐藏:0否；1是
    private String isHidden;
    //菜单类型: 0目录 1菜单 2按钮
    private String menuType;
    //展示类型: 第一栏,第二栏,第三栏
    private String showType;
    //菜单图标
    private String menuIcon;
    //排序等级
    private Integer isSort;
    //权限描述
    private String remark;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date updateTime;
    //状态：0未启用；1已启用
    @TableLogic
    private String isDelete;

    //初始化查询菜单类型
    @TableField(exist = false)
    private String menuTypeFlag;
    @TableField(exist = false)
    private List<SysMenu> children;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.menuId;
    }
}

