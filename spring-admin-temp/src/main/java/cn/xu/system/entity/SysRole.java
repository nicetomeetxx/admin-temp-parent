package cn.xu.system.entity;

import java.util.Date;

import cn.xu.system.entity.bo.SysMenuBo;
import cn.xu.system.entity.bo.SysRoleBo;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 角色表(SysRole)表实体类
 *
 * @author ljxu
 * @since 2023-05-01 15:36:18
 */
@Data
@SuppressWarnings("serial")
public class SysRole extends Model<SysRole> {
    //主键
    @TableId(type = IdType.AUTO)
    private Long roleId;
    //角色名称
    @TableField(value = "role_name", condition = SqlCondition.LIKE)
    private String roleName;
    //角色权限标识
    private String roleAuth;
    //排序字段
    private String isSort;
    //角色描述
    private String remark;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date updateTime;
    //状态：0未启用；1已启用
    @TableLogic
    private String isDelete;

    //菜单列表
    @TableField(exist = false)
    private List<SysMenuBo> menuList;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.roleId;
    }
}

