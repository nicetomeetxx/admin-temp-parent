package cn.xu.system.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 部门表(SysDept)表实体类
 *
 * @author ljxu
 * @since 2023-05-03 16:08:05
 */
@Data
@SuppressWarnings("serial")
public class SysDept extends Model<SysDept> {
    //主键
    @TableId(type = IdType.AUTO)
    private Long deptId;
    //部门名称
    private String deptName;
    //上级部门id
    private Long pDeptId;
    //部门负责人
    private String leader;
    //负责人电话
    private String phone;
    //显示顺序
    private Integer isSort;
    //备注
    private String remark;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date createTime;
    //修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date updateTime;

    @TableField(exist = false)
    private List<SysDept> children;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.deptId;
    }
}

