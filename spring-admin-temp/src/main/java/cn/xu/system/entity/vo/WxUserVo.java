package cn.xu.system.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xu
 * @create 2023-07-23 18:26
 * @desc
 **/
@Data
public class WxUserVo implements Serializable {
    private Long id;
    private String name;
    private String avatar;
    private String username;
    private String telephone;
    private Integer age;
    private String sex;
    private String email;
    private String remark;
    private Date createTime;
}
