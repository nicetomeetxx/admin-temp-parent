package cn.xu.system.entity.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class RegisterVo implements Serializable {

    private String username;

    private String password;
}
