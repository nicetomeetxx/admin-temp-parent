package cn.xu.system.entity.bo;

import lombok.Data;

import java.io.Serializable;

@Data

public class SysMenuBo implements Serializable {

    private Long menuId;

    private String menuName;

}