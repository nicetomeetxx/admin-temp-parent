package cn.xu.system.entity.bo;

import lombok.Data;

import java.io.Serializable;

@Data

public class SysRoleBo implements Serializable {

    private Long roleId;

    private String roleName;

    private String roleAuth;
}
