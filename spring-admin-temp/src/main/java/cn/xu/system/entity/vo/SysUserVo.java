package cn.xu.system.entity.vo;

import cn.xu.system.entity.bo.SysRoleBo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SysUserVo implements Serializable {

    private Long id;
    private String name;
    private String avatar;
    private List<SysRoleBo> roles;
    private List<String> auth;
    private List<RouterVo> routes;

}
