package cn.xu.system.entity.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class GrantVo implements Serializable {

    private Long id;
    private Long roleId;
    private List<Long> roleIds;
    private List<Long> menuIds;
}
