package cn.xu.system.entity.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 路由需要的数据格式
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RouterVo implements Serializable {

    private String path;

    private Boolean hidden;

    private String name;

    private String component;

    private boolean alwaysShow;

    private Meta meta;

    @Data
    @AllArgsConstructor
    public class Meta implements Serializable {
        private String title;
        private String icon;
        private Object[] roles;
    }

    private List<RouterVo> children = new ArrayList<>();

}