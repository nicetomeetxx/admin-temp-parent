package cn.xu.system.entity;

import cn.xu.system.entity.bo.SysRoleBo;
import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 用户表(SysUser)表实体类
 *
 * @author ljxu
 * @since 2023-04-25 22:13:15
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SysUser extends Model<SysUser> {
    //主键
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    //用户名
    @NotBlank(message = "用户名不能为空")
    @Size(max = 30, message = "用户名长度不能超过{max}个字符")
    private String username;
    //密码
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;
    //姓名(condition模糊查询)
    @TableField(value = "name", condition = SqlCondition.LIKE)
    private String name;
    //头像
    private String avatar;
    //手机号
    @NotBlank(message = "手机号不能为空")
    @Size(max = 11, message = "手机号长度不能超过{max}个字符")
    private String telephone;
    //年龄
    @TableField(value = "age", fill = FieldFill.UPDATE)
    private Integer age;
    //性别：0:男；1:女
    private String sex;
    //邮箱
    @TableField(value = "email", fill = FieldFill.UPDATE)
    private String email;
    //所属部门id(@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)前端返回忽略)
    private Integer deptId;
    //是否启用：0未启用；1已启用
    private String isUse;
    //是否删除: 0未删除；1已删除
    @TableLogic
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String isDelete;
    //备注
    private String remark;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date loginTime;
    //修改时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date updateTime;

    //在线状态
    @TableField(exist = false)
    private String isOnline;
    //角色列表
    @TableField(exist = false)
    private List<SysRoleBo> roleList;

    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

