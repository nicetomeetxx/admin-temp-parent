package cn.xu.system.entity.eo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.xu.config.result.ErrorMsg;
import lombok.Data;

import java.io.Serializable;


@Data
public class SysUserEo extends ErrorMsg implements Serializable {

    @Excel(name = "序号", width = 5)
    private Integer tid;

    @Excel(name = "用户名", width = 30)
    private String username;

    @Excel(name = "姓名", width = 15)
    private String name;

    @Excel(name = "手机号", width = 20)
    private String telephone;

    @Excel(name = "年龄", width = 5)
    private Integer age;

    @Excel(name = "性别", width = 5)
    private String sex;

    @Excel(name = "邮箱", width = 20)
    private String email;

    @Excel(name = "状态", width = 10)
    private String isUse;

    //密码
    private String password;
}
