package cn.xu.tools.dict.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.tools.dict.entity.SysDictData;
import cn.xu.tools.dict.service.ISysDictDataService;
import cn.xu.tools.dict.service.ISysDictTypeService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 字典数据表(SysDictData)表控制层
 *
 * @author ljxu
 * @since 2023-06-06 21:15:43
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("sys/dictData")
public class SysDictDataController {
    /**
     * 服务对象
     */
    private final ISysDictDataService sysDictDataService;
    private final ISysDictTypeService sysDictTypeService;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysDictData>> selectAll(@RequestBody PageParam<SysDictData> params) {
        return R.ok(this.sysDictDataService.selectPageDictDataList(params));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param dictCode 主键
     * @return 单条数据
     */
    @GetMapping(value = "{dictCode}")
    public R<SysDictData> getInfo(@PathVariable Long dictCode) {
        return R.ok(sysDictDataService.selectDictDataById(dictCode));
    }


    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType 字典类型
     */
    @GetMapping(value = "/type/{dictType}")
    public R<List<SysDictData>> dictType(@PathVariable String dictType) {
        List<SysDictData> data = sysDictTypeService.selectDictDataByType(dictType);
        if (ObjectUtil.isNull(data)) {
            data = new ArrayList<>();
        }
        return R.ok(data);
    }

    /**
     * 新增字典类型
     */
    @PostMapping
    public R<Void> add(@Validated @RequestBody SysDictData dict) {
        sysDictDataService.insertDictData(dict);
        return R.ok();
    }

    /**
     * 修改保存字典类型
     */
    @PutMapping
    public R<Void> edit(@Validated @RequestBody SysDictData dict) {
        sysDictDataService.updateDictData(dict);
        return R.ok();
    }

    /**
     * 删除字典类型
     *
     * @param dictCodes 字典code串
     */
    @DeleteMapping("/{dictCodes}")
    public R<Void> remove(@PathVariable Long[] dictCodes) {
        sysDictDataService.deleteDictDataByIds(dictCodes);
        return R.ok();
    }
}

