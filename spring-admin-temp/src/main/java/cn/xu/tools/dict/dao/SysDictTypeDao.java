package cn.xu.tools.dict.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.tools.dict.entity.SysDictType;

/**
 * 字典类型表(SysDictType)表数据库访问层
 *
 * @author ljxu
 * @since 2023-06-06 21:15:45
 */
public interface SysDictTypeDao extends BaseMapper<SysDictType> {


}

