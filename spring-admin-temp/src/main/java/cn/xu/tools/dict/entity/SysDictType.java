package cn.xu.tools.dict.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 字典类型表(SysDictType)表实体类
 *
 * @author ljxu
 * @since 2023-06-06 21:15:45
 */
@EqualsAndHashCode(callSuper = true)
@Data
@SuppressWarnings("serial")
public class SysDictType extends Model<SysDictType> {
    //字典主键
    @TableId(type = IdType.AUTO)
    private Long dictId;
    //字典名称
    private String dictName;
    //字典类型
    private String dictType;
    //状态（0正常 1停用）
    private String status;
    //创建者
    private String createBy;
    //创建时间
    private Date createTime;
    //更新者
    private String updateBy;
    //更新时间
    private Date updateTime;
    //备注
    private String remark;

    //--------创建修改时间-----------
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.dictId;
    }
}

