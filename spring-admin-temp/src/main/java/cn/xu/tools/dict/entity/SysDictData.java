package cn.xu.tools.dict.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 字典数据表(SysDictData)表实体类
 *
 * @author ljxu
 * @since 2023-06-06 21:15:44
 */
@Data
@SuppressWarnings("serial")
@Accessors(chain = true)
public class SysDictData extends Model<SysDictData> {
    //字典编码
    @TableId(type = IdType.AUTO)
    private Long dictCode;
    //字典排序
    private Integer dictSort;
    //字典标签
    private String dictLabel;
    //字典键值
    private String dictValue;
    //字典类型
    private String dictType;
    //样式属性（其他样式扩展）
    private String cssClass;
    //表格回显样式
    private String listClass;
    //是否默认（Y是 N否）
    private String isDefault;
    //状态（0正常 1停用）
    private String status;
    //创建者
    private String createBy;
    //创建时间
    private Date createTime;
    //更新者
    private String updateBy;
    //更新时间
    private Date updateTime;
    //备注
    private String remark;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.dictCode;
    }

    public SysDictData build() {
        SysDictData dictData = new SysDictData();
        dictData.setDictSort(1);
        dictData.setIsDefault("N");
        dictData.setListClass("default");
        dictData.setStatus("1");
        return dictData;
    }
}

