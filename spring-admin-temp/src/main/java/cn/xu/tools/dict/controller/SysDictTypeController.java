package cn.xu.tools.dict.controller;


import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.tools.dict.entity.SysDictType;
import cn.xu.tools.dict.service.ISysDictTypeService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 字典类型表(SysDictType)表控制层
 *
 * @author ljxu
 * @since 2023-06-06 21:15:45
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("sys/dictType")
public class SysDictTypeController {
    /**
     * 服务对象
     */
    private final ISysDictTypeService sysDictTypeService;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysDictType>> list(@RequestBody PageParam<SysDictType> params) {
        return R.ok(sysDictTypeService.selectPageDictTypeList(params));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param dictId 主键
     * @return 单条数据
     */
    @GetMapping(value = "/{dictId}")
    public R<SysDictType> getInfo(@PathVariable Long dictId) {
        return R.ok(sysDictTypeService.selectDictTypeById(dictId));
    }

    /**
     * 新增数据
     *
     * @param dict 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R<Void> add(@RequestBody SysDictType dict) {
        if (!sysDictTypeService.checkDictTypeUnique(dict)) {
            return R.error("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        sysDictTypeService.insertDictType(dict);
        return R.ok();
    }

    /**
     * 修改数据
     *
     * @param dict 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R<Void> edit(@Validated @RequestBody SysDictType dict) {
        if (!sysDictTypeService.checkDictTypeUnique(dict)) {
            return R.error("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
        sysDictTypeService.updateDictType(dict);
        return R.ok();
    }

    /**
     * 删除数据
     *
     * @param dictIds 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/{dictIds}")
    public R<Void> remove(@PathVariable Long[] dictIds) {
        sysDictTypeService.deleteDictTypeByIds(dictIds);
        return R.ok();
    }

    /**
     * 获取字典选择框列表
     */
    @GetMapping("/optionSelect")
    public R<List<SysDictType>> optionSelect() {
        return R.ok(sysDictTypeService.selectDictTypeAll());
    }
}

