package cn.xu.tools.file.controller;


import cn.hutool.core.util.IdUtil;
import cn.xu.config.result.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 附件索引表(SysIndex)表控制层
 *
 * @author ljxu
 * @since 2023-07-05 21:29:35
 */
@RestController
@RequestMapping("sys/index")
public class IndexController {


    /**
     * 新增
     * 1.点击新增按钮时先生成业务id回显
     * 2.点击文件上传时先上传文件，然后保存文件id及业务id
     * 3.保存业务id到业务表
     * <p>
     * 删除
     * 1.是否能获取到fileId，根据fileId删除
     * 2.只能根据文件名删除，那就要保证文件名唯一
     */

    /**
     * 获取index
     *
     * @return r
     */
    @GetMapping
    public R<String> createIndex() {
        return R.ok(IdUtil.getSnowflakeNextIdStr());
    }
}

