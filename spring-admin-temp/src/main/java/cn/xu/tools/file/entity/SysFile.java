package cn.xu.tools.file.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 附件(图片)表(SysFile)表实体类
 *
 * @author ljxu
 * @since 2023-07-05 18:54:49
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class SysFile extends Model<SysFile> {

    @TableId(type = IdType.AUTO)
    private Long id;
    //索引id
    private String indexId;
    //文件名
    private String fileName;
    //文件地址(url)
    private String filePath;
    //文件类型(0预览；1图片；2附件)
    private String fileType;
    //文件后缀名
    private String fileSuffix;
    //文件大小
    private Long fileSize;
    //创建人
    private String createBy;
    //创建时间
    private Date createTime;
    //是否删除: 0未删除；1已删除
    @TableLogic
    private String isDelete;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

