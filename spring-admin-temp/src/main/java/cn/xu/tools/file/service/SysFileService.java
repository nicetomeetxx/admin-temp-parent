package cn.xu.tools.file.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xu.tools.file.entity.SysFile;

import java.util.List;

/**
 * 附件(图片)表(SysFile)表服务接口
 *
 * @author ljxu
 * @since 2023-07-05 19:05:37
 */
public interface SysFileService extends IService<SysFile> {
    /**
     * 根据indexId查询文件列表
     * @param indexId
     * @return
     */
    List<SysFile> getFileListByIndexId(Long indexId);
}

