package cn.xu.tools.file.service.impl;

import cn.xu.config.constant.WxConstants;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.tools.file.dao.SysFileDao;
import cn.xu.tools.file.entity.SysFile;
import cn.xu.tools.file.service.SysFileService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * 附件(图片)表(SysFile)表服务实现类
 *
 * @author ljxu
 * @since 2023-07-05 19:05:37
 */
@Service("sysFileService")
public class SysFileServiceImpl extends ServiceImpl<SysFileDao, SysFile> implements SysFileService {

    /**
     * 根据indexId查询文件列表
     *
     * @param indexId
     * @return
     */
    @Override
    public List<SysFile> getFileListByIndexId(Long indexId) {
        return baseMapper.selectList(new LambdaQueryWrapper<SysFile>().eq(SysFile::getIndexId, indexId).ne(SysFile::getFileType, WxConstants.THREE));
    }
}

