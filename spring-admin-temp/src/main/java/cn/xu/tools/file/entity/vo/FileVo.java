package cn.xu.tools.file.entity.vo;

import cn.xu.tools.file.entity.SysFile;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FileVo implements Serializable {

    //文件id列表
    @TableField(exist = false)
    private List<Long> fileIdList;

    //文件列表
    @TableField(exist = false)
    private List<SysFile> fileList;
}
