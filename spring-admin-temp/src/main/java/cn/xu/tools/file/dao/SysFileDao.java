package cn.xu.tools.file.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.tools.file.entity.SysFile;

/**
 * 附件(图片)表(SysFile)表数据库访问层
 *
 * @author ljxu
 * @since 2023-07-05 19:06:02
 */
public interface SysFileDao extends BaseMapper<SysFile> {


}

