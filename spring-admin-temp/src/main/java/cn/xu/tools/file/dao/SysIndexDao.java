package cn.xu.tools.file.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.tools.file.entity.SysIndex;

/**
 * 附件索引表(SysIndex)表数据库访问层
 *
 * @author ljxu
 * @since 2023-07-05 19:43:46
 */
public interface SysIndexDao extends BaseMapper<SysIndex> {


}

