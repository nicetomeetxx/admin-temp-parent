package cn.xu.tools.file.controller;

import cn.xu.config.constant.CommonConstants;
import cn.xu.config.constant.WxConstants;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.tools.file.entity.SysFile;
import cn.xu.tools.file.service.SysFileService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.shaded.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@RestController
@RequestMapping("file")
@Slf4j
@RequiredArgsConstructor
public class FileController {
    private SysFileService fileService;
    //文件上传路径
    private static final String file_path_pre = "/static/";

    @Value("${sys.file.upload}")
    private String filePath;


    @SneakyThrows
    @PostMapping("upload/{indexId}/{fileTip}")
    public R<SysFile> uploadFile(@RequestBody MultipartFile file, @PathVariable String indexId, @PathVariable String fileTip) {
        String filename = file.getOriginalFilename();
        log.info("System.getProperty(user.dir) :{}", System.getProperty("user.dir"));
        File fileUpload = new File(System.getProperty("user.dir") + this.filePath + File.separator + filename);
        log.info("上传文件：{}", fileUpload.getAbsolutePath());
        file.transferTo(fileUpload);
        List<SysFile> list = fileService.list(new LambdaQueryWrapper<SysFile>().eq(SysFile::getIndexId, indexId).eq(SysFile::getFileType, CommonConstants.FILE_TYPE_0));
        SysFile sysFile = new SysFile()
                .setIndexId(indexId).setFileName(filename).setFilePath(file_path_pre + filename)
                .setFileType(fileTip).setFileSize(file.getSize())
                .setCreateBy(LoginUserUtils.getInstance().getUsername())
                .setFileSuffix(FilenameUtils.getExtension(filename));
        if (CollectionUtils.isEmpty(list)) {
            sysFile.setFileType("1"); //这里强改了类型
        }
        sysFile.insert();
        return R.ok(sysFile);
    }

}
