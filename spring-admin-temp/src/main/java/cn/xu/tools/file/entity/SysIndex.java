package cn.xu.tools.file.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 附件索引表(SysIndex)表实体类
 *
 * @author ljxu
 * @since 2023-07-05 19:09:32
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@TableName("sys_index")
public class SysIndex extends Model<SysIndex> {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;
    //创建人
    private String createBy;
    //创建时间
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

