package cn.xu.tools.logs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.tools.logs.dao.SysLogsDao;
import cn.xu.tools.logs.entity.SysLogs;
import cn.xu.tools.logs.service.ISysLogsService;
import org.springframework.stereotype.Service;

/**
 * 操作日志Service业务层处理
 *
 * @author ljxu
 * @date 2023-06-12
 */
@Service
public class SysLogsServiceImpl   extends ServiceImpl<SysLogsDao, SysLogs> implements ISysLogsService {


}
