package cn.xu.tools.logs.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;


/**
 * 操作日志对象 sys_logs
 *
 * @author ljxu
 * @date 2023-06-12
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_logs")
public class SysLogs extends Model<SysLogs> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "log_id")
    private Long logId;
    /**
     * 操作描述
     */
    private String logDesc;
    /**
     * 操作类型
     */
    private String operation;
    /**
     * 请求方法
     */
    private String method;
    /**
     * 请求参数
     */
    private String params;
    /**
     * 操作人
     */
    private String username;
    /**
     * ip地址
     */
    private String ip;
    /**
     * 执行时长
     */
    private Long time;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    //--------创建修改时间-----------
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

}
