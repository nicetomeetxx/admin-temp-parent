package cn.xu.tools.logs.entity;

import java.util.Date;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * (SysLogsLogin)表实体类
 *
 * @author ljxu
 * @since 2023-06-13 19:04:20
 */
@Data
@SuppressWarnings("serial")
@Accessors(chain = true)
public class SysLogsLogin extends Model<SysLogsLogin> {

    private Long id;
    //登录账号
    private String userName;
    //ip地址
    private String ipAddr;
    //登录时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date loginTime;
    //登录设备
    private String loginMac;
    //创建时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "Asia/Shanghai")
    private Date createTime;


    /**
     * 获取主键值
     *
     * @return 主键值
     */
    @Override
    public Serializable pkVal() {
        return this.id;
    }
}

