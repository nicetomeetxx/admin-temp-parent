package cn.xu.tools.logs.service;

import cn.xu.tools.logs.entity.SysLogs;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.Collection;
import java.util.List;

/**
 * 操作日志Service接口
 *
 * @author ljxu
 * @date 2023-06-12
 */
public interface ISysLogsService  extends IService<SysLogs>{

}
