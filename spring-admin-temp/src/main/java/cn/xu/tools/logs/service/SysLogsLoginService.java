package cn.xu.tools.logs.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.xu.tools.logs.entity.SysLogsLogin;

/**
 * (SysLogsLogin)表服务接口
 *
 * @author ljxu
 * @since 2023-06-13 19:04:20
 */
public interface SysLogsLoginService extends IService<SysLogsLogin> {

}

