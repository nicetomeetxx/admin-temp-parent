package cn.xu.tools.logs.dao;

import cn.xu.tools.logs.entity.SysLogs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 操作日志Mapper接口
 *
 * @author ljxu
 * @date 2023-06-12
 */
public interface SysLogsDao extends BaseMapper<SysLogs>{

}
