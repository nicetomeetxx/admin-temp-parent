package cn.xu.tools.logs.controller;

import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.anno.Log;
import cn.xu.config.utils.ExcelUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import cn.xu.tools.logs.entity.SysLogs;
import cn.xu.tools.logs.service.ISysLogsService;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 操作日志
 *
 * @author ljxu
 * @date 2023-06-12
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/logs/logs")
public class SysLogsController {

    private final ISysLogsService iSysLogsService;

    /**
     * 查询操作日志列表
     */
    @PostMapping("/page")
    public R<Page<SysLogs>> list(@RequestBody PageParam<SysLogs> params) {
    SysLogs param = params.getParam();
    LambdaQueryWrapper<SysLogs> lqw = Wrappers.lambdaQuery();
                lqw.eq(ObjectUtils.isNotEmpty(param.getOperation()), SysLogs::getOperation, param.getOperation());
                lqw.eq(ObjectUtils.isNotEmpty(param.getMethod()), SysLogs::getMethod, param.getMethod());
                lqw.eq(ObjectUtils.isNotEmpty(param.getParams()), SysLogs::getParams, param.getParams());
                lqw.like(ObjectUtils.isNotEmpty(param.getUsername()), SysLogs::getUsername, param.getUsername());
                lqw.eq(ObjectUtils.isNotEmpty(param.getIp()), SysLogs::getIp, param.getIp());
                lqw.eq(ObjectUtils.isNotEmpty(param.getTime()), SysLogs::getTime, param.getTime());
    return R.ok(this.iSysLogsService.page(params.build(), lqw));
    }


    /**
     * 获取操作日志详细信息
     *
     * @param logId 主键
     */
    @GetMapping("/{logId}")
    public R<SysLogs> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long logId) {
        return R.ok(iSysLogsService.getById(logId));
    }

    /**
     * 新增操作日志
     */
    @PostMapping()
    public R<Boolean> add(@RequestBody SysLogs entity) {
        return R.ok(iSysLogsService.save(entity));
    }

    /**
     * 修改操作日志
     */
    @PutMapping()
    public R<Boolean> edit(@RequestBody SysLogs entity) {
        return  R.ok(iSysLogsService.updateById(entity));
    }

    /**
     * 删除操作日志
     *
     * @param idList 主键串
     */
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestParam("idList") List<Long> idList) {
        return R.ok(iSysLogsService.removeByIds(idList));
    }

    /**
     * 导出操作日志列表
     */
    @SneakyThrows
    @PostMapping("/export")
    public void export(SysLogs entity, HttpServletResponse response) {
        LambdaQueryWrapper<SysLogs> lqw = Wrappers.lambdaQuery();
                    lqw.eq(ObjectUtils.isNotEmpty(entity.getOperation()), SysLogs::getOperation, entity.getOperation());
                    lqw.eq(ObjectUtils.isNotEmpty(entity.getMethod()), SysLogs::getMethod, entity.getMethod());
                    lqw.eq(ObjectUtils.isNotEmpty(entity.getParams()), SysLogs::getParams, entity.getParams());
                    lqw.like(ObjectUtils.isNotEmpty(entity.getUsername()), SysLogs::getUsername, entity.getUsername());
                    lqw.eq(ObjectUtils.isNotEmpty(entity.getIp()), SysLogs::getIp, entity.getIp());
                    lqw.eq(ObjectUtils.isNotEmpty(entity.getTime()), SysLogs::getTime, entity.getTime());
        List<SysLogs> list = iSysLogsService.list(lqw);
        ExcelUtils.exportExcel(list,"操作日志数据表", "操作日志", SysLogs.class,"操作日志", response);
    }
}
