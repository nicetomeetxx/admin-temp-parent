package cn.xu.tools.logs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.tools.logs.dao.SysLogsLoginDao;
import cn.xu.tools.logs.entity.SysLogsLogin;
import cn.xu.tools.logs.service.SysLogsLoginService;
import org.springframework.stereotype.Service;


/**
 * (SysLogsLogin)表服务实现类
 *
 * @author ljxu
 * @since 2023-06-13 19:04:20
 */
@Service("sysLogsLoginService")
public class SysLogsLoginServiceImpl extends ServiceImpl<SysLogsLoginDao, SysLogsLogin> implements SysLogsLoginService {

}

