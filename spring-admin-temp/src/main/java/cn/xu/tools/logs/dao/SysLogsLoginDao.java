package cn.xu.tools.logs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.xu.tools.logs.entity.SysLogsLogin;

/**
 * (SysLogsLogin)表数据库访问层
 *
 * @author ljxu
 * @since 2023-06-13 19:04:20
 */
public interface SysLogsLoginDao extends BaseMapper<SysLogsLogin> {


}

