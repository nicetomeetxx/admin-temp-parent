package cn.xu.tools.logs.controller;


import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.tools.logs.entity.SysLogsLogin;
import cn.xu.tools.logs.service.SysLogsLoginService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * (SysLogsLogin)表控制层
 *
 * @author ljxu
 * @since 2023-06-13 19:04:20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/logs/login")
public class SysLogsLoginController {
    /**
     * 服务对象
     */
    private final SysLogsLoginService sysLogsLoginService;

    /**
     * 分页查询所有数据
     *
     * @param params 分页对象
     * @return 所有数据
     */
    @PostMapping("page")
    public R<Page<SysLogsLogin>> selectAll(@RequestBody PageParam<SysLogsLogin> params) {
        SysLogsLogin param = params.getParam();
        LambdaQueryWrapper<SysLogsLogin> lqw = Wrappers.lambdaQuery();
        lqw.eq(ObjectUtils.isNotEmpty(param.getUserName()), SysLogsLogin::getUserName, param.getUserName()).orderByDesc(SysLogsLogin::getLoginTime);
        return R.ok(this.sysLogsLoginService.page(params.build(), lqw));
    }

}

