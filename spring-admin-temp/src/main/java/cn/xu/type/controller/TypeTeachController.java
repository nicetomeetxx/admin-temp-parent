package cn.xu.type.controller;

import cn.xu.config.anno.Log;
import cn.xu.config.constant.BusinessType;
import cn.xu.config.result.PageParam;
import cn.xu.config.result.R;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.tools.dict.entity.SysDictData;
import cn.xu.tools.dict.service.ISysDictDataService;
import cn.xu.tools.file.entity.SysFile;
import cn.xu.tools.file.entity.SysIndex;
import cn.xu.tools.file.service.impl.SysFileServiceImpl;
import cn.xu.type.entity.TypeTeach;
import cn.xu.type.service.ITypeTeachService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 类型
 *
 * @author ljxu
 * @date 2023-08-07
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/work/teach")
public class TypeTeachController {

    private final ITypeTeachService iTypeTeachService;
    private final SysFileServiceImpl fileService;
    private final ISysDictDataService dictDataService;

    /**
     * 查询类型列表
     */
    @PostMapping("/page")
    public R<Page<TypeTeach>> list(@RequestBody PageParam<TypeTeach> params) {
        TypeTeach param = params.getParam();
        LambdaQueryWrapper<TypeTeach> lqw = Wrappers.lambdaQuery();
        lqw.like(ObjectUtils.isNotEmpty(param.getName()), TypeTeach::getName, param.getName());
        return R.ok(this.iTypeTeachService.page(params.build(), lqw));
    }


    /**
     * 获取类型详细信息
     *
     * @param id 主键
     */
    @GetMapping("/{id}")
    public R<TypeTeach> getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        TypeTeach teach = iTypeTeachService.getById(id);
        Long indexId = teach.getIndexId();
        //根据index查询file,3为富文本图片，不能展示在图片列表中
        List<SysFile> fileList = fileService.lambdaQuery().eq(SysFile::getIndexId, indexId).ne(SysFile::getFileType, "3").list(); //①注意
        teach.setFileList(fileList);
        return R.ok(teach);
    }

    /**
     * 新增类型
     */
    @Log(title = "类型", businessType = BusinessType.INSERT)
    @PostMapping()
    public R<Boolean> add(@RequestBody TypeTeach entity) {
        //这里新增index
        new SysIndex().setId(entity.getIndexId()).setCreateBy(LoginUserUtils.getInstance().getUsername()).insert(); //①注意
        //这里需要添加到dict_data表中
        SysDictData dictData = new SysDictData().build().setDictType("sys_type_demo").setDictLabel(entity.getName()).setDictValue(entity.getName());//②注意
        dictDataService.insertDictData(dictData);
        entity.setDictCode(dictData.getDictCode());
        return R.ok(iTypeTeachService.save(entity));
    }

    /**
     * 修改类型
     */
    @Log(title = "类型", businessType = BusinessType.UPDATE)
    @PutMapping()
    public R<Boolean> edit(@RequestBody TypeTeach entity) {
        dictDataService.updateDictData(new SysDictData().setDictCode(entity.getDictCode()).setDictLabel(entity.getName()).setDictValue(entity.getName()));//②注意
        return R.ok(iTypeTeachService.updateById(entity));
    }

    /**
     * 删除类型
     *
     * @param idList 主键串
     */
    @Log(title = "类型", businessType = BusinessType.DELETE)
    @DeleteMapping
    public R<Boolean> remove(@NotEmpty(message = "主键不能为空") @RequestBody List<Long> idList) {
        for (Long aLong : idList) {
            TypeTeach teach = iTypeTeachService.getById(aLong);
            dictDataService.deleteDictDataById(teach.getDictCode());//②注意
        }
        return R.ok(iTypeTeachService.removeByIds(idList));
    }

}
