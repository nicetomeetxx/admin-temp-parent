package cn.xu.type.entity;

import cn.xu.tools.file.entity.SysFile;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;


/**
 * 类型对象 type_teach
 *
 * @author ljxu
 * @date 2023-08-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("type_teach")
public class TypeTeach extends Model<TypeTeach> {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    /**
     *
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING) //①注意
    private Long indexId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 年龄
     */
    private Long age;
    /**
     * 地址
     */
    private String address;
    /**
     * 科目
     */
    private String subejct;
    /**
     *
     */
    private Long dictCode;

    //--------创建修改时间-----------
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginTime;
    @TableField(exist = false)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    //文件id列表
    @TableField(exist = false)
    private List<Long> fileIdList; //①注意

    //文件列表
    @TableField(exist = false)
    private List<SysFile> fileList; //①注意

}
