package cn.xu.type.dao;

import cn.xu.type.entity.TypeTeach;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 类型Mapper接口
 *
 * @author ljxu
 * @date 2023-08-07
 */
public interface TypeTeachDao extends BaseMapper<TypeTeach>{

}
