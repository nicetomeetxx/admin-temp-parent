package cn.xu.type.service;

import cn.xu.type.entity.TypeTeach;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 类型Service接口
 *
 * @author ljxu
 * @date 2023-08-07
 */
public interface ITypeTeachService  extends IService<TypeTeach>{

}
