package cn.xu.type.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.xu.type.dao.TypeTeachDao;
import cn.xu.type.entity.TypeTeach;
import cn.xu.type.service.ITypeTeachService;
import org.springframework.stereotype.Service;

/**
 * 类型Service业务层处理
 *
 * @author ljxu
 * @date 2023-08-07
 */
@Service
public class TypeTeachServiceImpl   extends ServiceImpl<TypeTeachDao, TypeTeach> implements ITypeTeachService {


}
