package cn.xu.config.exception;


import cn.dev33.satoken.exception.NotLoginException;
import cn.xu.config.result.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Objects;


/**
 * 异常处理器
 *
 * @author ljxu
 */
@RestControllerAdvice
@Slf4j
public class BaseExceptionHandler {

    @ExceptionHandler(BaseException.class)
    public R handleBaseException(BaseException e) {
        e.printStackTrace();
        return Objects.requireNonNull(R.error(e.getErrorType().getInfo()));
    }

    @ExceptionHandler(ServiceException.class)
    public R handleServiceException(ServiceException e) {
        e.printStackTrace();
        return Objects.requireNonNull(R.error(e.getMessage()));
    }


    @ExceptionHandler(NotLoginException.class)
    public R handleBaseException(NotLoginException nle) {
        // 不同异常返回不同状态码
        String message = "";
        if (nle.getType().equals(NotLoginException.NOT_TOKEN)) {
            message = "未提供Token";
        } else if (nle.getType().equals(NotLoginException.INVALID_TOKEN)) {
            message = "登录已过期";
        } else if (nle.getType().equals(NotLoginException.TOKEN_TIMEOUT)) {
            message = "登录信息已过期，请重新登录";
        } else if (nle.getType().equals(NotLoginException.BE_REPLACED)) {
            message = "您的账户已在另一台设备上登录，如非本人操作，请立即修改密码";
        } else if (nle.getType().equals(NotLoginException.KICK_OUT)) {
            message = "已被系统强制下线";
        } else {
            message = "会话未登录";
        }
        nle.printStackTrace();
        // 返回给前端
        return R.error(50001, message);
    }

    /**
     * 系统异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(RuntimeException.class)
    public R handleRuntimeException(RuntimeException e) {
        e.printStackTrace();
        if (!StringUtils.isEmpty(e.getCause().getMessage())) return R.error(e.getCause().getMessage());
        return R.error();
    }

    /**
     * 系统异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)
    public R handleException(Exception e) {
        e.printStackTrace();
        return R.error();
    }
}
