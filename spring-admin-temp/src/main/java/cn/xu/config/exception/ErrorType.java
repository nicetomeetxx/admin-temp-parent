package cn.xu.config.exception;

import lombok.Getter;

@Getter
public enum ErrorType {

    SYSTEM_ERROR("500", "服务器异常"),
    USER_TOKEN_ERROR("501", "会话超时，重新登录"),
    USER_PASSWORD_ERROR("502", "用户名密码错误"),

    EXCEL_IMPORT_ERROR("EXCEL00001", "EXCEL导入错误"),


    ;

    /**
     * 错误类型码
     */
    private String code;
    /**
     * 错误类型描述信息
     */
    private String info;

    ErrorType(String code, String info) {
        this.code = code;
        this.info = info;
    }
}
