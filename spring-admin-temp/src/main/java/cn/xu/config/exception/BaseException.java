package cn.xu.config.exception;

import lombok.Getter;

@Getter
public class BaseException extends RuntimeException {
    /**
     * 异常对应的错误类型
     */
    private final ErrorType errorType;


    @Override
    public Throwable fillInStackTrace() {
        return this;
    }

    @Override
    public String getMessage() {
        return errorType.getInfo();
    }

    /**
     * 默认是系统异常
     */
    public BaseException() {
        this.errorType = ErrorType.SYSTEM_ERROR;
    }

    public BaseException(ErrorType errorType) {
        super(errorType.getInfo());
        this.errorType = errorType;
    }


    @Override
    public String toString() {
        return "BaseException{" +
                "errorType=" + errorType +
                '}';
    }
}
