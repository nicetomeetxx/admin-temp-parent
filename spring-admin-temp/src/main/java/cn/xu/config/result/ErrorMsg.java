package cn.xu.config.result;

import lombok.Data;

import java.io.Serializable;

@Data
public class ErrorMsg implements Serializable {
    /**
     * 错误信息
     */
//    @Excel(name = "错误信息", width = 50)
    private String errorMsg;
}
