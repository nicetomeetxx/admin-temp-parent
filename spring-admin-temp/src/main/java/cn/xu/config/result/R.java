package cn.xu.config.result;

import cn.xu.config.exception.ErrorType;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 返回数据
 *
 * @author ljxu
 */
@Accessors(chain = true)
public class R<T> extends HashMap<String, Object> implements Serializable {
    private static final long serialVersionUID = 1L;

    public R() {
        put("code", 200);
        put("info", "操作成功");
    }

    public static <T> R<T> error() {
        return error(500, "未知异常，请联系管理员");
    }

    public static <T> R<T> error(String info) {
        return error(500, info);
    }

    public static <T> R<T> error(int code, String info) {
        R<T> r = new R<>();
        r.put("code", code);
        r.put("info", info);
        return r;
    }

    public static <T> R<T> error(ErrorType errorType) {
        R<T> r = new R<>();
        r.put("code", errorType.getCode());
        r.put("info", errorType.getInfo());
        return r;
    }

    public static <T> R<T> ok(String info) {
        R<T> r = new R<>();
        r.put("info", info);
        return r;
    }

    public static <T> R<T> ok(String info, Object data) {
        R<T> r = new R<>();
        r.put("info", info);
        r.put("data", data);
        return r;
    }

    public static <T> R<T> ok(T data) {
        R<T> r = new R<>();
        r.put("info", "操作成功");
        r.put("data", data);
        return r;
    }

    public static <T> R<T> ok(Map<String, Object> map) {
        R<T> r = new R<>();
        r.putAll(map);
        return r;
    }

    public static <T> R<T> ok() {
        return new R<>();
    }

    public static <T> R<T> put(Object value) {
        R<T> r = new R<>();
        r.put("data", value);
        return r;
    }

    @Override
    public R<T> put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
