package cn.xu.config.result;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysCheck implements Serializable {

    private Boolean flag;

    private String data;
}
