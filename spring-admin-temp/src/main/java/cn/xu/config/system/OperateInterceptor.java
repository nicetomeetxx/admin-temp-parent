package cn.xu.config.system;

import cn.hutool.extra.spring.SpringUtil;
import cn.xu.config.anno.Log;
import cn.xu.config.utils.IPUtils;
import cn.xu.config.utils.LoginUserUtils;
import cn.xu.tools.logs.dao.SysLogsDao;
import cn.xu.tools.logs.entity.SysLogs;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.lang.reflect.Method;
import java.util.Objects;

@Aspect
@Slf4j
@Order(1)
@Component
public class OperateInterceptor {

    @Pointcut("@annotation(cn.xu.config.anno.Log)")
    public void pointCut() {
    }


    @Around("pointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();
        Object[] args = point.getArgs();
        String params = null;
        if (!ObjectUtils.isEmpty(args)) {
            params = JSON.toJSONString(args);
        }
        Object proceed = point.proceed();
        //记录日志
        recordLog(point, params, beginTime);
        return proceed;
    }

    private void recordLog(ProceedingJoinPoint point, String params, long beginTime) {
        HttpServletRequest request = ((ServletRequestAttributes) (Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))).getRequest();
        String ipAddr = IPUtils.getIpAddr(request);
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        Log annotation = method.getAnnotation(Log.class);

        String className = point.getTarget().getClass().getName().split("controller.")[1];
        String methodName = signature.getName();
        String url = className + "." + methodName + "()";
        long time = System.currentTimeMillis() - beginTime;

        if (ObjectUtils.isNotEmpty(annotation)) {
            SysLogs sysLogs = new SysLogs();
            sysLogs.setLogDesc(annotation.title());
            sysLogs.setOperation(annotation.businessType());
            sysLogs.setIp(ipAddr);
            sysLogs.setMethod(url);
            sysLogs.setParams(params);
            sysLogs.setTime(time);
            sysLogs.setUsername(LoginUserUtils.getInstance().getUsername());
            SpringUtil.getBean(SysLogsDao.class).insert(sysLogs);
            //记录操作日志到数据库
            log.info("[Operate]IP:{},param:{},time:{}ms", url, params, time);
        }
    }


}
