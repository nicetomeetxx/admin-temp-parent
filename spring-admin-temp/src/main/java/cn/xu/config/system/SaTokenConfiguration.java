package cn.xu.config.system;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.util.UrlPathHelper;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Slf4j
@EnableWebMvc
@Configuration
public class SaTokenConfiguration implements WebMvcConfigurer {

    @Value("${sys.file.upload}")
    private String filePath;

    private final String[] noAuth = {
            "/login",
            "/static/**",
            "/admin/static/**",
            "/subject/subject/noAuth/wx/subject",
            "/subject/subject/noAuth/wx/subject/page",
            "/subject/subject/wx/subject/detail",
            "/subject/subject/wx/subject/record",
            "/wx/like/comment",
            "/wx/user/register",
            "/wx/user/captcha"
    };

    /**
     * satoken权限拦截器**
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CorsConfiguration()).addPathPatterns("/**");
        registry.addInterceptor(new LoginHandlerInterceptor())
                .addPathPatterns("/**").excludePathPatterns("/login", "/static/**", "/admin/static/**");
        registry.addInterceptor(new SaInterceptor(item -> StpUtil.checkLogin()))
                .addPathPatterns("/**").excludePathPatterns(Arrays.asList(noAuth));
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        UrlPathHelper pathHelper = new UrlPathHelper();
        pathHelper.setUrlDecode(false);
        pathHelper.setDefaultEncoding(StandardCharsets.UTF_8.name());
        configurer.setUrlPathHelper(pathHelper);
    }

    /**
     * springboot 2.0配置WebMvcConfigurationSupport之后，会导致默认配置被覆盖，要访问静态资源需要重写addResourceHandlers方法
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("file:" + System.getProperty("user.dir") + "/" + filePath + "/");
    }
}
