package cn.xu.config.system;


import cn.dev33.satoken.stp.StpUtil;
import cn.xu.config.utils.LoginUserUtils;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 请求拦截:添加用户数据
 */
@Configuration
@Slf4j
public class LoginHandlerInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.校验token
        String token = request.getHeader("X-Token");
        String username = (String) StpUtil.getLoginIdByToken(token);
        if (!ObjectUtils.isEmpty(username)) {
            LoginUserUtils.getInstance().setUsername(username);
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        LoginUserUtils.getInstance().clear();
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
