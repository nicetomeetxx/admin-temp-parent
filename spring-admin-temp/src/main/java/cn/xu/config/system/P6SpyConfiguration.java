package cn.xu.config.system;

import com.p6spy.engine.spy.appender.MessageFormattingStrategy;
import com.p6spy.engine.spy.appender.StdoutLogger;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * mybatis日志打印
 */
@Slf4j
public class P6SpyConfiguration extends StdoutLogger implements MessageFormattingStrategy {


    public P6SpyConfiguration() {
    }


    public String formatMessage(int connectionId, String now, long elapsed, String category, String prepared, String sql, String url) {
        return StringUtils.isNotBlank(sql) ? " Consume Time: " + elapsed + " ms ;Start: " + now +
                "\n SQL: " + sql.replaceAll("[\\s]+", " ") : "";
    }

    @Override
    public void logText(String text) {
        //打印sql颜色
        //[1;Xm==>X: 30黑色；31红色；32绿色；33黄色；34蓝色；35青蓝色；37白色
        System.out.println("\033[1;34m" + text + "\033[0m\n");
    }
}
