package cn.xu.config.utils;

import cn.xu.config.exception.BaseException;
import cn.xu.config.exception.ErrorType;
import cn.xu.system.entity.SysUser;
import cn.xu.system.service.SysUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.util.ObjectUtils;

/**
 * @author xu
 * @create 2023-07-20 19:20
 * @desc
 **/
public class UserUtils {
    /**
     * 根据username查询用户
     *
     * @param username
     * @return
     */
    public static SysUser selectUserIdByUserName(String username) {
        SysUser sysUser = SpringUtils.getBean(SysUserService.class).getOne(new LambdaQueryWrapper<SysUser>().eq(SysUser::getUsername, username));
        if (ObjectUtils.isEmpty(sysUser)) {
            throw new BaseException(ErrorType.SYSTEM_ERROR);
        }
        return sysUser;
    }
}
