package cn.xu.config.utils;


import cn.xu.system.entity.SysMenu;
import cn.xu.system.entity.vo.RouterVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

//构造树形工具
public class MenuUtils {

    /**
     * 生成路由数据格式
     */
    public static List<RouterVo> makeRouter(List<SysMenu> menuList, Long pid) {
        //接受生产的路由数据
        List<RouterVo> list = new ArrayList<>();
        //组装数据
        Optional.ofNullable(menuList).orElse(new ArrayList<>())
                .stream()
                .filter(item -> item != null && item.getPMenuId().equals(pid))
                .forEach(item -> {
                    RouterVo router = new RouterVo();
                    router.setName(item.getMenuName());  //name前端标识
                    router.setPath(item.getUrl()); //url去除后面的/index标志
                    router.setHidden("1".equals(item.getIsHidden())); // hidden:0否；1是
                    //判断是否是一级菜单，设置component---> component是渲染前端路由，url是页面访问路径
                    if (item.getPMenuId() == 0L) {
                        router.setComponent("Layout");
                        router.setAlwaysShow(true);
                    } else {
                        router.setComponent(item.getUrl());
                        router.setAlwaysShow(false);
                    }
                    //设置meta
                    router.setMeta(router.new Meta(
                            item.getMenuName(),
                            item.getMenuIcon(),
                            item.getMenuAuth().split(",")
                    ));
                    //设置children
                    List<RouterVo> children = makeRouter(menuList, item.getMenuId());
                    router.setChildren(children);
                    list.add(router);
                });
        return list;
    }
}
