package cn.xu.config.utils;

import java.util.*;

public class CollFilterUtils {
    private Map<String, Map<String, Double>> userRatings;
    private Map<String, List<String>> itemUsers;

    public CollFilterUtils(Map<String, Map<String, Double>> userRatings) {
        this.userRatings = userRatings;
        this.itemUsers = new HashMap<>();

        // 构建物品-用户倒排表
        for (String user : userRatings.keySet()) {
            Map<String, Double> ratings = userRatings.get(user);
            for (String item : ratings.keySet()) {
                if (!itemUsers.containsKey(item)) {
                    itemUsers.put(item, new ArrayList<>());
                }
                itemUsers.get(item).add(user);
            }
        }
    }

    /**
     * 计算皮尔逊系数
     *
     * @param user1Ratings
     * @param user2Ratings
     * @return
     */
    public double calculateSimilarity(Map<String, Double> user1Ratings, Map<String, Double> user2Ratings) {
        // 获取Map中所有value的集合
        Collection<Double> values = user1Ratings.values();
        // 将集合转换为数组
        Double[] x = values.toArray(new Double[values.size()]);
        // 输出数组
        System.out.println(Arrays.toString(x));

        // 获取Map中所有value的集合
        Collection<Double> valuesY = user2Ratings.values();
        // 将集合转换为数组
        Double[] y = valuesY.toArray(new Double[valuesY.size()]);
        // 输出数组
        System.out.println(Arrays.toString(y));

        // 计算用户之间的相似度，具体实现略
        double sumXY = 0;
        double sumX = 0;
        double sumY = 0;
        double sumX2 = 0;
        double sumY2 = 0;
        int n = x.length;
//      计算参数值
        for (int i = 0; i < n; i++) {
            sumXY += x[i] * y[i];
            sumX += x[i];
            sumY += y[i];
            sumX2 += x[i] * x[i];
            sumY2 += y[i] * y[i];
        }
//      分子和分母
        double numerator = sumXY - (sumX * sumY / n);
        double denominator = Math.sqrt((sumX2 - sumX * sumX / n) * (sumY2 - sumY * sumY / n));
//      分母为0（一个用户的情况）,单独返回
        if (denominator == 0) {
            return 0;
        }
//      返回皮尔逊相关系数
        return numerator / denominator;
    }

    public List<String> recommendItems(String targetUser, int numRecommendations) {
        // 计算目标用户与其他用户的相似度
        Map<String, Double> userSimilarities = new HashMap<>();
        for (String user : userRatings.keySet()) {
            if (!user.equals(targetUser)) {
                double similarity = calculateSimilarity(userRatings.get(targetUser), userRatings.get(user));
                userSimilarities.put(user, similarity);
            }
        }

        // 根据相似度进行排序
        List<Map.Entry<String, Double>> sortedSimilarities = new ArrayList<>(userSimilarities.entrySet());
        sortedSimilarities.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));

        // 选择相似度最高的K个用户
        List<String> similarUsers = new ArrayList<>();
        for (int i = 0; i < numRecommendations; i++) {
            if (i < sortedSimilarities.size()) {
                similarUsers.add(sortedSimilarities.get(i).getKey());
            } else {
                break;
            }
        }

        // 获取相似用户喜欢的物品，并进行推荐
        Set<String> recommendations = new HashSet<>();
        for (String user : similarUsers) {
            Map<String, Double> ratings = userRatings.get(user);
            for (String item : ratings.keySet()) {
                if (!userRatings.get(targetUser).containsKey(item)) {
                    recommendations.add(item);
                }
            }
        }

        // 排序推荐物品
        List<String> sortedRecommendations = new ArrayList<>(recommendations);
        sortedRecommendations.sort((item1, item2) -> {
            double rating1 = userRatings.get(targetUser).getOrDefault(item1, 0.0);
            double rating2 = userRatings.get(targetUser).getOrDefault(item2, 0.0);
            return Double.compare(rating2, rating1);
        });

        // 取前N个推荐物品
        int numItems = Math.min(numRecommendations, sortedRecommendations.size());
        return sortedRecommendations.subList(0, numItems);
    }

    public static void main(String[] args) {
        // 示例数据
        Map<String, Map<String, Double>> ratings = new HashMap<>();
        Map<String, Double> user1Ratings = new HashMap<>();
        user1Ratings.put("item1", 4.5);
        user1Ratings.put("item2", 3.0);
        user1Ratings.put("item3", 5.0);
        user1Ratings.put("item4", 2.4);
        user1Ratings.put("item5", 3.5);
        user1Ratings.put("item6", 3.1);
        user1Ratings.put("item7", 2.0);
        user1Ratings.put("item8", 1.0);
        ratings.put("User1", user1Ratings);

        Map<String, Double> user2Ratings = new HashMap<>();
        user2Ratings.put("item1", 4.0);
        user2Ratings.put("item2", 2.5);
        user2Ratings.put("item3", 3.5);
        user2Ratings.put("item4", 5.0);
        user2Ratings.put("item5", 4.3);
        user2Ratings.put("item6", 1.0);
        user2Ratings.put("item7", 3.0);
        user2Ratings.put("item8", 2.0);
        ratings.put("User2", user2Ratings);

        Map<String, Double> user3Ratings = new HashMap<>();
        user3Ratings.put("item1", 2.5);
        user3Ratings.put("item4", 4.5);
        ratings.put("User3", user3Ratings);

        Map<String, Double> user4Ratings = new HashMap<>();
        user4Ratings.put("item1", 3.5);
        user4Ratings.put("item2", 4.5);
        ratings.put("User4", user4Ratings);

        //1.查询所有用户（随机获取五个用户） SELECT * FROM `sys_user` ORDER BY RAND() LIMIT 0,5;
        //2.遍历用户，查询评论表中subject的id,like_num order by desc
        //3.放到上面map中，获取推荐的前几位
        //4.根据推荐的前几位去subject中in('id','id')得到推荐数据，实现协同过滤算法推荐

        // 创建协同过滤对象
        CollFilterUtils filter = new CollFilterUtils(ratings);

        // 为指定用户推荐物品
        String targetUser = "User4";
        int numRecommendations = 4;
        List<String> recommendations = filter.recommendItems(targetUser, numRecommendations);

        // 输出推荐结果
        System.out.println("Recommendations for " + targetUser + ":");
        for (String item : recommendations) {
            System.out.println(item);
        }
    }
}

