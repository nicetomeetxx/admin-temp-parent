package cn.xu.config.utils;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户上下文
 */
public class LoginUserUtils {

    private final ThreadLocal<Map<String, Object>> threadLocal;

    public LoginUserUtils() {
        this.threadLocal = new ThreadLocal<>();
    }

    /**
     * 创建实例
     *
     * @return e
     */
    public static LoginUserUtils getInstance() {
        return SingletonHolder.sInstance;
    }

    /**
     * 静态内部类单例模式
     * 单例初使化
     */
    private static class SingletonHolder {
        private static final LoginUserUtils sInstance = new LoginUserUtils();
    }

    /**
     * 用户上下文中放入信息
     *
     * @param map m
     */
    public void setContext(Map<String, Object> map) {
        threadLocal.set(map);
    }

    /**
     * 获取上下文中的用户名
     */
    public void setUsername(String username) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put("username", username);
        threadLocal.set(map);
    }

    /**
     * 获取上下文中的信息
     *
     * @return e
     */
    public Map<String, Object> getContext() {
        return threadLocal.get();
    }

    /**
     * 获取上下文中的用户名
     *
     * @return
     */
    public String getUsername() {
        return (String) Optional.ofNullable(threadLocal.get()).orElse(Maps.newHashMap()).get("username");
    }


    /**
     * 清空上下文
     */
    public void clear() {
        threadLocal.remove();
    }

}
