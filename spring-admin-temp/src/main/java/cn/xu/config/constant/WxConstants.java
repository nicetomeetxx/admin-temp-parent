package cn.xu.config.constant;

public abstract class WxConstants {

    //消息类型
    /**
     * 收藏
     */
    public final static String COLLECT = "0";
    /**
     * 点赞
     */
    public final static String LIKE = "1";
    /**
     * 评论
     */
    public final static String COMMENT = "2";
    /**
     * 消息
     */
    public final static String MESSAGE = "9";

    //消息状态
    /**
     * 未读
     */
    public static final String UN_READ = "0";
    /**
     * 已读
     */
    public static final String READ = "1";

    //图片类型
    /**
     * 主图
     */
    public static final String ZERO = "0";
    /**
     * 正文
     */
    public static final String ONE = "1";
    /**
     * 附件
     */
    public static final String TWO = "2";
    /**
     * 富文本
     */
    public static final String THREE = "3";

    //预约状态
    /**
     * 已预约
     */
    public static final String APPOINT_HAS = "0";
    /**
     * 完成预约
     */
    public static final String APPOINT_END = "1";
    /**
     * 取消预约
     */
    public static final String APPOINT_CAN = "2";

    //订单支付状态:0待付款；1待发货；2待评价；3完成；4取消
    /**
     * 待付款
     */
    public static final String TO_BE_PAID = "1";
    /**
     * 待发货
     */
    public static final String TO_BE_SHIPPED = "2";
    /**
     * 待收货
     */
    public static final String TO_GET_SHIPPED = "3";
    /**
     * 待评价
     */
    public static final String TO_BE_EVALUATED = "4";
    /**
     * 订单完成
     */
    public static final String ORDER_COMPLETED = "5";
    /**
     * 订单取消
     */
    public static final String ORDER_CANCEL = "6";
    //前后台标识modeType
    /**
     * VUE管理台
     */
    public static final String MODE_VUE = "0";
    /**
     * uniapp小程序
     */
    public static final String MODE_APP = "1";


}
