package cn.xu.config.constant;

/**
 * 常量类
 *
 * @author ljxu
 * @Date 2023-3-18
 */
public abstract class CommonConstants {

    public static final String UTF8 = "UTF-8";

    //菜单类型
    public static final String MENU_TYPE_0 = "0";
    public static final String MENU_TYPE_1 = "1";
    public static final String MENU_TYPE_2 = "2";

    //文件类型
    public static final String FILE_TYPE_0 = "0";
    public static final String FILE_TYPE_1 = "1";
    public static final String FILE_TYPE_2 = "2";
    public static final String FILE_TYPE_3 = "3";

}
