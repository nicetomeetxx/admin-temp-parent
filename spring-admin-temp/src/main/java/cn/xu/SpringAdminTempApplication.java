package cn.xu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 项目启动类
 */
@SpringBootApplication
@MapperScan("cn.xu.**.dao")
public class SpringAdminTempApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringAdminTempApplication.class, args);
    }

}
