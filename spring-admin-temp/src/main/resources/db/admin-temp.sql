/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : admin-temp

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 06/07/2023 22:11:29
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`
(
    `table_id`          bigint                                                   NOT NULL COMMENT '编号',
    `table_name`        varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '表名称',
    `table_comment`     varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '表描述',
    `sub_table_name`    varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT NULL COMMENT '关联子表的表名',
    `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT NULL COMMENT '子表关联的外键名',
    `class_name`        varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '实体类名称',
    `tpl_category`      varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
    `package_name`      varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '生成包路径',
    `module_name`       varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT NULL COMMENT '生成模块名',
    `business_name`     varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT NULL COMMENT '生成业务名',
    `function_name`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT NULL COMMENT '生成功能名',
    `function_author`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT NULL COMMENT '生成功能作者',
    `gen_type`          char(1) CHARACTER SET utf8 COLLATE utf8_general_ci       NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
    `gen_path`          varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
    `options`           varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
    `create_by`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT '' COMMENT '创建者',
    `create_time`       datetime                                                 NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`         varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci   NULL DEFAULT '' COMMENT '更新者',
    `update_time`       datetime                                                 NULL DEFAULT NULL COMMENT '更新时间',
    `remark`            varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '代码生成业务表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table`
VALUES (1665256079496237057, 'sys_menu', '菜单表', NULL, NULL, 'SysMenu', 'crud', 'cn.xu.test', 'system', 'menu', '菜单', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":\"2\"}', NULL,
        '2023-05-04 22:05:33', NULL, NULL, NULL);
INSERT INTO `gen_table`
VALUES (1665256079567540238, 'sys_user', '用户表', NULL, NULL, 'SysUser', 'crud', 'com.ruoyi.system', 'system', 'user', '用户', 'ljxu', '0', '/', '{\"parentMenuId\":2}', NULL, '2023-05-04 22:05:51', NULL, NULL, NULL);
INSERT INTO `gen_table`
VALUES (1668250038367584257, 'sys_logs', '日志表', NULL, NULL, 'SysLogs', 'crud', 'cn.xu.tools.logs', 'logs', 'logs', '操作日志', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":21}', NULL,
        '2023-04-23 20:41:19', NULL, NULL, NULL);
INSERT INTO `gen_table`
VALUES (1676950524190740482, 'pro_subject', '首页业务表', NULL, NULL, 'ProSubject', 'crud', 'cn.xu.subject', 'subject', 'subject', '首页业务', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":\"30\"}',
        'admin', '2023-07-06 21:36:49', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`
(
    `column_id`      bigint                                                  NOT NULL COMMENT '编号',
    `table_id`       bigint                                                  NULL DEFAULT NULL COMMENT '归属表编号',
    `column_name`    varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
    `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
    `column_type`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
    `java_type`      varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
    `java_field`     varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
    `is_pk`          char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否主键（1是）',
    `is_increment`   char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否自增（1是）',
    `is_required`    char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否必填（1是）',
    `is_insert`      char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
    `is_edit`        char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
    `is_list`        char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
    `is_query`       char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
    `query_type`     varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
    `html_type`      varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
    `dict_type`      varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
    `sort`           int                                                     NULL DEFAULT NULL COMMENT '排序',
    `create_by`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time`    datetime                                                NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`      varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time`    datetime                                                NULL DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column`
VALUES (1665256079496237058, 1665256079496237057, 'menu_id', '主键', 'bigint', 'Long', 'menuId', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540225, 1665256079496237057, 'p_menu_id', '父权限id', 'bigint', 'Long', 'pMenuId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540226, 1665256079496237057, 'menu_name', '权限名称', 'varchar(20)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540227, 1665256079496237057, 'url', '权限路径', 'varchar(40)', 'String', 'url', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540228, 1665256079496237057, 'menu_auth', '权限标识', 'varchar(255)', 'String', 'menuAuth', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'fileUpload', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540229, 1665256079496237057, 'is_hidden', '是否隐藏:0否；1是', 'char(1)', 'String', 'isHidden', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540230, 1665256079496237057, 'menu_type', '菜单类型: 0目录 1菜单 2按钮', 'varchar(1)', 'String', 'menuType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540231, 1665256079496237057, 'show_type', '展示类型: 第一栏,第二栏,第三栏', 'char(1)', 'String', 'showType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540232, 1665256079496237057, 'menu_icon', '菜单图标', 'varchar(255)', 'String', 'menuIcon', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540233, 1665256079496237057, 'is_sort', '排序等级', 'int', 'Long', 'isSort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540234, 1665256079496237057, 'remark', '权限描述', 'varchar(50)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 11, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540235, 1665256079496237057, 'create_time', '', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 12, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540236, 1665256079496237057, 'update_time', '', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 13, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079567540237, 1665256079496237057, 'is_delete', '是否删除: 0未删除；1已删除', 'varchar(1)', 'String', 'isDelete', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 14, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649090, 1665256079567540238, 'id', '主键', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649091, 1665256079567540238, 'username', '用户名', 'varchar(30)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649092, 1665256079567540238, 'password', '密码', 'varchar(300)', 'String', 'password', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649093, 1665256079567540238, 'name', '姓名', 'varchar(20)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649094, 1665256079567540238, 'avatar', '头像', 'varchar(255)', 'String', 'avatar', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649095, 1665256079567540238, 'telephone', '手机号', 'varchar(11)', 'String', 'telephone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649096, 1665256079567540238, 'age', '年龄', 'int', 'Long', 'age', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649097, 1665256079567540238, 'sex', '性别：0:男；1:女', 'varchar(1)', 'String', 'sex', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649098, 1665256079567540238, 'email', '邮箱', 'varchar(50)', 'String', 'email', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649099, 1665256079567540238, 'dept_id', '所属部门id', 'int', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649100, 1665256079567540238, 'is_use', '是否启用：0未启用；1已启用', 'varchar(1)', 'String', 'isUse', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 11, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649101, 1665256079567540238, 'is_delete', '是否删除: 0未删除；1已删除', 'varchar(1)', 'String', 'isDelete', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 12, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649102, 1665256079567540238, 'remark', '备注', 'varchar(30)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 13, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649103, 1665256079567540238, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 14, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649104, 1665256079567540238, 'login_time', '登录时间', 'datetime', 'Date', 'loginTime', '0', '0', '1', '1', '1', '0', '0', 'EQ', 'datetime', '', 15, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1665256079634649105, 1665256079567540238, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 16, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038392750081, 1668250038367584257, 'log_id', '主键', 'bigint', 'Long', 'logId', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038392750082, 1668250038367584257, 'operation', '操作类型', 'varchar(20)', 'String', 'operation', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038392750083, 1668250038367584257, 'method', '请求方法', 'varchar(255)', 'String', 'method', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038392750084, 1668250038367584257, 'params', '请求参数', 'varchar(500)', 'String', 'params', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038455664642, 1668250038367584257, 'username', '操作人', 'varchar(50)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038455664643, 1668250038367584257, 'ip', 'ip地址', 'varchar(255)', 'String', 'ip', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038455664644, 1668250038367584257, 'time', '执行时长', 'bigint', 'Long', 'time', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1668250038455664645, 1668250038367584257, 'create_time', '操作时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849345, 1676950524190740482, 'id', '', 'bigint', 'Long', 'id', '1', '1', '0', NULL, '0', '0', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849346, 1676950524190740482, 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849347, 1676950524190740482, 'index_id', 'index', 'bigint', 'Long', 'indexId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849348, 1676950524190740482, 'main_text', '正文', 'text', 'String', 'mainText', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849349, 1676950524190740482, 'type', '类型', 'varchar(128)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849350, 1676950524190740482, 'collect_num', '收藏数量', 'bigint', 'Long', 'collectNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849351, 1676950524190740482, 'like_num', '点赞数量', 'bigint', 'Long', 'likeNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849352, 1676950524190740482, 'comment_num', '评论数量', 'bigint', 'Long', 'commentNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 8, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849353, 1676950524190740482, 'create_by', '创建人', 'varchar(128)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849354, 1676950524190740482, 'create_time', ' 创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column`
VALUES (1676950524257849355, 1676950524190740482, 'is_delete', '是否删除: 0未删除；1已删除', 'char(1)', 'String', 'isDelete', '0', '0', '1', '0', '0', '0', '0', 'EQ', 'input', '', 11, 'admin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pro_subject
-- ----------------------------
DROP TABLE IF EXISTS `pro_subject`;
CREATE TABLE `pro_subject`
(
    `id`          bigint                                                  NOT NULL AUTO_INCREMENT,
    `title`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
    `index_id`    bigint                                                  NULL DEFAULT NULL COMMENT 'index',
    `main_text`   text CHARACTER SET utf8 COLLATE utf8_general_ci         NULL COMMENT '正文',
    `type`        varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
    `collect_num` bigint                                                  NULL DEFAULT NULL COMMENT '收藏数量',
    `like_num`    bigint                                                  NULL DEFAULT NULL COMMENT '点赞数量',
    `comment_num` bigint                                                  NULL DEFAULT NULL COMMENT '评论数量',
    `username`    varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户',
    `create_time` datetime                                                NULL DEFAULT NULL COMMENT ' 创建时间',
    `is_delete`   char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT NULL COMMENT '是否删除: 0未删除；1已删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '首页业务表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_subject
-- ----------------------------

-- ----------------------------
-- Table structure for pro_type
-- ----------------------------
DROP TABLE IF EXISTS `pro_type`;
CREATE TABLE `pro_type`
(
    `id`          bigint                                                        NOT NULL,
    `type`        varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT ' 类型',
    `create_time` datetime                                                      NULL DEFAULT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '业务类型表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_type
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`
(
    `sys_id`       bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '主键',
    `parent_id`    bigint                                                        NULL DEFAULT NULL COMMENT '父id',
    `config_key`   varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'key',
    `config_value` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'value',
    `create_time`  datetime                                                      NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `remark`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '注释',
    `update_time`  datetime                                                      NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `update_by`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '修改人',
    PRIMARY KEY (`sys_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '系统配置表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`
(
    `dept_id`     bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `dept_name`   varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '部门名称',
    `p_dept_id`   bigint                                                       NULL DEFAULT NULL COMMENT '上级部门id',
    `leader`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '部门负责人',
    `phone`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人电话',
    `is_sort`     int                                                          NULL DEFAULT NULL COMMENT '显示顺序',
    `remark`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
    `create_time` datetime                                                     NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime                                                     NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `is_delete`   char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci     NULL DEFAULT NULL COMMENT '是否删除: 0未删除；1已删除',
    PRIMARY KEY (`dept_id`) USING BTREE,
    UNIQUE INDEX `dept_name` (`dept_name`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '部门表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept`
VALUES (1, '案例公司', 0, '张三', '17388888888', NULL, NULL, '2023-05-03 16:40:34', '2023-05-03 16:48:02', '1');
INSERT INTO `sys_dept`
VALUES (2, '研发中心', 1, '李四', '13333333333', NULL, NULL, '2023-05-03 16:40:48', '2023-05-03 16:48:03', '1');
INSERT INTO `sys_dept`
VALUES (3, '研发部', 2, NULL, NULL, NULL, NULL, '2023-05-03 16:40:56', '2023-05-03 16:48:04', '1');
INSERT INTO `sys_dept`
VALUES (4, '测试部', 2, NULL, NULL, NULL, NULL, '2023-05-03 16:41:27', '2023-05-03 16:48:04', '1');
INSERT INTO `sys_dept`
VALUES (5, '需求基地', 1, '王五', '18999999999', NULL, NULL, '2023-05-03 16:41:44', '2023-05-03 16:48:05', '1');
INSERT INTO `sys_dept`
VALUES (6, '需求部', 5, '赵六', '15855552525', NULL, NULL, '2023-05-03 16:41:53', '2023-05-03 16:48:06', '1');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`
(
    `dict_code`   bigint                                                  NOT NULL COMMENT '字典编码',
    `dict_sort`   int                                                     NULL DEFAULT 0 COMMENT '字典排序',
    `dict_label`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
    `dict_value`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
    `dict_type`   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
    `css_class`   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
    `list_class`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
    `is_default`  char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
    `status`      char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
    `create_by`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime                                                NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime                                                NULL DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '字典数据表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data`
VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别男');
INSERT INTO `sys_dict_data`
VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别女');
INSERT INTO `sys_dict_data`
VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别未知');
INSERT INTO `sys_dict_data`
VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data`
VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data`
VALUES (6, 1, '正常', '1', 'sys_normal_disable', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data`
VALUES (7, 2, '停用', '0', 'sys_normal_disable', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data`
VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data`
VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data`
VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知');
INSERT INTO `sys_dict_data`
VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '公告');
INSERT INTO `sys_dict_data`
VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data`
VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data`
VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '新增操作');
INSERT INTO `sys_dict_data`
VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '修改操作');
INSERT INTO `sys_dict_data`
VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '删除操作');
INSERT INTO `sys_dict_data`
VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '授权操作');
INSERT INTO `sys_dict_data`
VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '导出操作');
INSERT INTO `sys_dict_data`
VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '导入操作');
INSERT INTO `sys_dict_data`
VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '强退操作');
INSERT INTO `sys_dict_data`
VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '生成操作');
INSERT INTO `sys_dict_data`
VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '清空操作');
INSERT INTO `sys_dict_data`
VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data`
VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data`
VALUES (29, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '其他操作');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`
(
    `dict_id`     bigint                                                  NOT NULL COMMENT '字典主键',
    `dict_name`   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
    `dict_type`   varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
    `status`      char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
    `create_by`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime                                                NULL DEFAULT NULL COMMENT '创建时间',
    `update_by`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime                                                NULL DEFAULT NULL COMMENT '更新时间',
    `remark`      varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
    PRIMARY KEY (`dict_id`) USING BTREE,
    UNIQUE INDEX `dict_type` (`dict_type`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '字典类型表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type`
VALUES (1, '用户性别', 'sys_user_sex', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type`
VALUES (2, '菜单状态', 'sys_show_hide', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type`
VALUES (3, '系统开关', 'sys_normal_disable', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type`
VALUES (6, '系统是否', 'sys_yes_no', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type`
VALUES (7, '通知类型', 'sys_notice_type', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type`
VALUES (8, '通知状态', 'sys_notice_status', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type`
VALUES (9, '操作类型', 'sys_oper_type', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type`
VALUES (10, '系统状态', 'sys_common_status', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '登录状态列表');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`
(
    `id`          bigint                                                        NOT NULL AUTO_INCREMENT,
    `index_id`    bigint                                                        NULL DEFAULT NULL COMMENT '索引id',
    `file_name`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
    `file_path`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件地址(url)',
    `file_type`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件类型(0预览；1图片；2附件)',
    `file_suffix` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '文件后缀名',
    `file_size`   double                                                        NULL DEFAULT NULL COMMENT '文件大小',
    `create_by`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci  NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime                                                      NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `is_delete`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci   NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 134
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '附件(图片)表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file`
VALUES (1, 1676558272473808896, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:46:55', '0');
INSERT INTO `sys_file`
VALUES (2, 1676558782371151872, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:48:57', '0');
INSERT INTO `sys_file`
VALUES (3, 1676559247313944576, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:50:48', '0');
INSERT INTO `sys_file`
VALUES (4, 1676559292574679040, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:50:58', '0');
INSERT INTO `sys_file`
VALUES (5, 1676559982453800960, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:53:43', '0');
INSERT INTO `sys_file`
VALUES (6, 1676560531333005312, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:55:54', '0');
INSERT INTO `sys_file`
VALUES (7, 1676560726556884992, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:56:40', '0');
INSERT INTO `sys_file`
VALUES (8, 1676561249305575424, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:58:45', '0');
INSERT INTO `sys_file`
VALUES (9, 1676561460836909056, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:59:35', '0');
INSERT INTO `sys_file`
VALUES (10, 1676561487365881856, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:59:42', '0');
INSERT INTO `sys_file`
VALUES (11, 1676561519313895424, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:59:49', '0');
INSERT INTO `sys_file`
VALUES (12, 1676561535289999360, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 19:59:53', '0');
INSERT INTO `sys_file`
VALUES (13, 1676561778807095296, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 20:00:51', '0');
INSERT INTO `sys_file`
VALUES (14, 1676561946243710976, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 20:01:31', '0');
INSERT INTO `sys_file`
VALUES (15, 1676562250443997184, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 20:02:44', '0');
INSERT INTO `sys_file`
VALUES (16, 1676562345142992896, '营业执照.jpg', '/static/营业执照.jpg', '1', 'jpg', 2238638, NULL, '2023-07-05 20:03:06', '0');
INSERT INTO `sys_file`
VALUES (17, 1676563581430874112, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:08:01', '0');
INSERT INTO `sys_file`
VALUES (18, 1676563656752185344, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:08:19', '0');
INSERT INTO `sys_file`
VALUES (19, 1676563822649491456, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:08:59', '0');
INSERT INTO `sys_file`
VALUES (20, 1676563986890047488, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:09:38', '0');
INSERT INTO `sys_file`
VALUES (21, 1676564818960269312, '2051058.jpg', '/static/2051058.jpg', '1', 'jpg', 730468, NULL, '2023-07-05 20:12:56', '0');
INSERT INTO `sys_file`
VALUES (22, 1676564909074890752, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:13:18', '0');
INSERT INTO `sys_file`
VALUES (23, 1676565591379099648, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:16:00', '0');
INSERT INTO `sys_file`
VALUES (24, 1676565806949548032, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:16:52', '0');
INSERT INTO `sys_file`
VALUES (25, 1676565997966540800, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:17:37', '0');
INSERT INTO `sys_file`
VALUES (26, 1676566334651711488, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:18:57', '0');
INSERT INTO `sys_file`
VALUES (27, 1676566804552171520, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:20:49', '0');
INSERT INTO `sys_file`
VALUES (28, 1676566821572657152, '2051058.jpg', '/static/2051058.jpg', '1', 'jpg', 730468, NULL, '2023-07-05 20:20:54', '0');
INSERT INTO `sys_file`
VALUES (29, 1676566840195366912, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:20:58', '0');
INSERT INTO `sys_file`
VALUES (30, 1676566981157535744, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:21:32', '0');
INSERT INTO `sys_file`
VALUES (31, 1676566992431824896, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 20:21:34', '0');
INSERT INTO `sys_file`
VALUES (32, 1676567064750014464, '2049047.jpg', '/static/2049047.jpg', '1', 'jpg', 1009239, NULL, '2023-07-05 20:21:52', '0');
INSERT INTO `sys_file`
VALUES (33, 1676567079325220864, '2051058.jpg', '/static/2051058.jpg', '1', 'jpg', 730468, NULL, '2023-07-05 20:21:55', '0');
INSERT INTO `sys_file`
VALUES (34, 1676567094974169088, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:21:59', '0');
INSERT INTO `sys_file`
VALUES (35, 1676568881437294592, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:29:05', '0');
INSERT INTO `sys_file`
VALUES (36, 1676568894984896512, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:29:08', '0');
INSERT INTO `sys_file`
VALUES (37, 1676568937439641600, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:29:18', '0');
INSERT INTO `sys_file`
VALUES (38, 1676568964593565696, '2051058.jpg', '/static/2051058.jpg', '1', 'jpg', 730468, NULL, '2023-07-05 20:29:24', '0');
INSERT INTO `sys_file`
VALUES (39, 1676569052669755392, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:29:45', '0');
INSERT INTO `sys_file`
VALUES (40, 1676569065084895232, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 20:29:48', '0');
INSERT INTO `sys_file`
VALUES (41, 1676569075436437504, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:29:51', '0');
INSERT INTO `sys_file`
VALUES (42, 1676569087738331136, '2049047.jpg', '/static/2049047.jpg', '1', 'jpg', 1009239, NULL, '2023-07-05 20:29:54', '0');
INSERT INTO `sys_file`
VALUES (43, 1676569232030777344, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:30:28', '0');
INSERT INTO `sys_file`
VALUES (44, 1676569243753857024, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:30:31', '0');
INSERT INTO `sys_file`
VALUES (45, 1676569259658657792, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:30:35', '0');
INSERT INTO `sys_file`
VALUES (46, 1676569271855693824, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 20:30:38', '0');
INSERT INTO `sys_file`
VALUES (47, 1676569794591801344, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:32:42', '0');
INSERT INTO `sys_file`
VALUES (48, 1676569809502552064, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:32:46', '0');
INSERT INTO `sys_file`
VALUES (49, 1676569829471633408, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:32:51', '0');
INSERT INTO `sys_file`
VALUES (50, 1676569852062154752, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 20:32:56', '0');
INSERT INTO `sys_file`
VALUES (51, 1676570060447760384, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:33:46', '0');
INSERT INTO `sys_file`
VALUES (52, 1676570279566589952, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:34:38', '0');
INSERT INTO `sys_file`
VALUES (53, 1676570307966222336, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:34:45', '0');
INSERT INTO `sys_file`
VALUES (54, 1676570320469442560, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:34:48', '0');
INSERT INTO `sys_file`
VALUES (55, 1676570332343517184, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:34:51', '0');
INSERT INTO `sys_file`
VALUES (56, 1676570463646203904, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:35:22', '0');
INSERT INTO `sys_file`
VALUES (57, 1676570474421370880, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:35:24', '0');
INSERT INTO `sys_file`
VALUES (58, 1676570486081536000, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:35:27', '0');
INSERT INTO `sys_file`
VALUES (59, 1676571031399776256, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:37:37', '0');
INSERT INTO `sys_file`
VALUES (60, 1676571414046130176, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:39:08', '0');
INSERT INTO `sys_file`
VALUES (61, 1676571471835250688, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:39:22', '0');
INSERT INTO `sys_file`
VALUES (62, 1676571545965379584, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:39:40', '0');
INSERT INTO `sys_file`
VALUES (63, 1676571602521374720, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:39:53', '0');
INSERT INTO `sys_file`
VALUES (64, 1676571844960534528, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:40:51', '0');
INSERT INTO `sys_file`
VALUES (65, 1676572017497423872, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:41:32', '0');
INSERT INTO `sys_file`
VALUES (66, 1676572084870529024, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:41:48', '0');
INSERT INTO `sys_file`
VALUES (67, 1676572648798896128, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:44:03', '0');
INSERT INTO `sys_file`
VALUES (68, 1676572686828650496, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:44:12', '0');
INSERT INTO `sys_file`
VALUES (69, 1676572716385910784, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:44:19', '0');
INSERT INTO `sys_file`
VALUES (70, 1676572815946104832, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:44:43', '0');
INSERT INTO `sys_file`
VALUES (71, 1676572933902516224, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:45:11', '0');
INSERT INTO `sys_file`
VALUES (72, 1676572952177098752, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:45:15', '0');
INSERT INTO `sys_file`
VALUES (73, 1676572971781275648, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:45:20', '0');
INSERT INTO `sys_file`
VALUES (74, 1676573717817933824, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:48:18', '0');
INSERT INTO `sys_file`
VALUES (75, 1676573939772112896, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:49:11', '0');
INSERT INTO `sys_file`
VALUES (76, 1676574003911409664, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:49:26', '0');
INSERT INTO `sys_file`
VALUES (77, 1676574048337477632, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 20:49:37', '0');
INSERT INTO `sys_file`
VALUES (78, 1676574532808949760, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:51:32', '0');
INSERT INTO `sys_file`
VALUES (79, 1676576128355745792, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:57:52', '0');
INSERT INTO `sys_file`
VALUES (80, 1676576206269136896, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 20:58:11', '0');
INSERT INTO `sys_file`
VALUES (81, 1676576245431353344, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 20:58:20', '0');
INSERT INTO `sys_file`
VALUES (82, 1676576312456331264, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-05 20:58:36', '0');
INSERT INTO `sys_file`
VALUES (83, 1676576345830408192, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 20:58:44', '0');
INSERT INTO `sys_file`
VALUES (84, 1676588051101073408, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 21:45:15', '0');
INSERT INTO `sys_file`
VALUES (85, 1676588109120880640, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 21:45:29', '0');
INSERT INTO `sys_file`
VALUES (86, 1676588758063595520, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 21:48:04', '0');
INSERT INTO `sys_file`
VALUES (87, 1676588972958760960, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 21:48:55', '0');
INSERT INTO `sys_file`
VALUES (88, 1676589157512331264, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:49:39', '0');
INSERT INTO `sys_file`
VALUES (89, 1676589493908094976, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:50:59', '0');
INSERT INTO `sys_file`
VALUES (90, 1676589531078017024, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:51:08', '0');
INSERT INTO `sys_file`
VALUES (91, 1676589654688350208, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 21:51:37', '0');
INSERT INTO `sys_file`
VALUES (92, 1676589817762889728, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:52:16', '0');
INSERT INTO `sys_file`
VALUES (93, 1676589830907838464, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 21:52:19', '0');
INSERT INTO `sys_file`
VALUES (94, 1676589855389990912, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:52:25', '0');
INSERT INTO `sys_file`
VALUES (95, 1676589960209842176, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 21:52:50', '0');
INSERT INTO `sys_file`
VALUES (96, 1676589971463159808, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:52:53', '0');
INSERT INTO `sys_file`
VALUES (97, 1676589984406781952, '2049047.jpg', '/static/2049047.jpg', '1', 'jpg', 1009239, NULL, '2023-07-05 21:52:56', '0');
INSERT INTO `sys_file`
VALUES (98, 1676590226028052480, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:53:54', '0');
INSERT INTO `sys_file`
VALUES (99, 1676590264523374592, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:54:03', '0');
INSERT INTO `sys_file`
VALUES (100, 1676590277668323328, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:54:06', '0');
INSERT INTO `sys_file`
VALUES (101, 1676590291446611968, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 21:54:09', '0');
INSERT INTO `sys_file`
VALUES (102, 1676590351349661696, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:54:23', '0');
INSERT INTO `sys_file`
VALUES (103, 1676590362955300864, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 21:54:26', '0');
INSERT INTO `sys_file`
VALUES (104, 1676590377966714880, '2049047.jpg', '/static/2049047.jpg', '1', 'jpg', 1009239, NULL, '2023-07-05 21:54:30', '0');
INSERT INTO `sys_file`
VALUES (105, 1676590744246894592, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:55:57', '0');
INSERT INTO `sys_file`
VALUES (106, 1676590754204172288, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 21:56:00', '0');
INSERT INTO `sys_file`
VALUES (107, 1676590766321516544, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:56:02', '0');
INSERT INTO `sys_file`
VALUES (108, 1676590805047525376, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 21:56:12', '0');
INSERT INTO `sys_file`
VALUES (109, 1676590816233734144, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:56:14', '0');
INSERT INTO `sys_file`
VALUES (110, 1676590829336739840, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:56:17', '0');
INSERT INTO `sys_file`
VALUES (111, 1676590957346897920, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 21:56:48', '0');
INSERT INTO `sys_file`
VALUES (112, 1676591364106305536, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:58:25', '0');
INSERT INTO `sys_file`
VALUES (113, 1676591444053934080, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 21:58:44', '0');
INSERT INTO `sys_file`
VALUES (114, 1676591730675892224, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 21:59:52', '0');
INSERT INTO `sys_file`
VALUES (115, 1676592915948453888, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 22:04:35', '0');
INSERT INTO `sys_file`
VALUES (116, 1676592928069992448, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 22:04:38', '0');
INSERT INTO `sys_file`
VALUES (117, 1676592946462015488, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 22:04:42', '0');
INSERT INTO `sys_file`
VALUES (118, 1676593354953670656, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 22:06:20', '0');
INSERT INTO `sys_file`
VALUES (119, 1676593365275852800, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 22:06:22', '0');
INSERT INTO `sys_file`
VALUES (120, 1676593376776634368, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 22:06:25', '0');
INSERT INTO `sys_file`
VALUES (121, 1676593841971085312, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 22:08:16', '0');
INSERT INTO `sys_file`
VALUES (122, 1676593851936751616, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-05 22:08:18', '0');
INSERT INTO `sys_file`
VALUES (123, 1676593875580043264, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 22:08:24', '0');
INSERT INTO `sys_file`
VALUES (124, 1676600843845713920, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 22:37:03', '0');
INSERT INTO `sys_file`
VALUES (125, 1676600843845713920, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-05 22:41:51', '0');
INSERT INTO `sys_file`
VALUES (126, 1676600843845713920, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 22:41:56', '0');
INSERT INTO `sys_file`
VALUES (127, 1676603058798977024, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 22:46:15', '0');
INSERT INTO `sys_file`
VALUES (128, 1676604002618040320, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 22:49:46', '0');
INSERT INTO `sys_file`
VALUES (129, 1676612318136098816, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, NULL, '2023-07-05 23:21:44', '0');
INSERT INTO `sys_file`
VALUES (130, 1676612377506471936, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-05 23:21:58', '0');
INSERT INTO `sys_file`
VALUES (131, 1676940486663340032, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, NULL, '2023-07-06 21:05:46', '0');
INSERT INTO `sys_file`
VALUES (132, 1676940486663340032, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-06 21:06:06', '0');
INSERT INTO `sys_file`
VALUES (133, 1676940486663340032, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-06 21:06:49', '0');
INSERT INTO `sys_file`
VALUES (134, 1676941991747448832, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, NULL, '2023-07-06 21:11:49', '0');
INSERT INTO `sys_file`
VALUES (135, 1676941991747448832, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, NULL, '2023-07-06 21:13:12', '0');
INSERT INTO `sys_file`
VALUES (136, 1676941991747448832, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, NULL, '2023-07-06 21:13:35', '0');
INSERT INTO `sys_file`
VALUES (137, 1676943806647980032, '317952.jpg', '/static/317952.jpg', '1', 'jpg', 416170, 'admin', '2023-07-06 21:18:57', '0');
INSERT INTO `sys_file`
VALUES (138, 1676955583951544320, '315747.jpg', '/static/315747.jpg', '1', 'jpg', 40224, 'admin', '2023-07-06 22:05:45', '0');
INSERT INTO `sys_file`
VALUES (139, 1676955583951544320, '323014.jpg', '/static/323014.jpg', '1', 'jpg', 161704, 'admin', '2023-07-06 22:05:49', '0');

-- ----------------------------
-- Table structure for sys_index
-- ----------------------------
DROP TABLE IF EXISTS `sys_index`;
CREATE TABLE `sys_index`
(
    `id`          bigint                                                        NOT NULL,
    `create_by`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
    `create_time` datetime                                                      NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_general_ci COMMENT = '附件索引表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_index
-- ----------------------------
INSERT INTO `sys_index`
VALUES (1676558272473808896, NULL, '2023-07-05 19:46:55');
INSERT INTO `sys_index`
VALUES (1676558782371151872, NULL, '2023-07-05 19:48:57');
INSERT INTO `sys_index`
VALUES (1676559247313944576, NULL, '2023-07-05 19:50:48');
INSERT INTO `sys_index`
VALUES (1676559292574679040, NULL, '2023-07-05 19:50:58');
INSERT INTO `sys_index`
VALUES (1676559982453800960, NULL, '2023-07-05 19:53:43');
INSERT INTO `sys_index`
VALUES (1676560531333005312, NULL, '2023-07-05 19:55:54');
INSERT INTO `sys_index`
VALUES (1676560726556884992, NULL, '2023-07-05 19:56:40');
INSERT INTO `sys_index`
VALUES (1676561249305575424, NULL, '2023-07-05 19:58:45');
INSERT INTO `sys_index`
VALUES (1676561460836909056, NULL, '2023-07-05 19:59:35');
INSERT INTO `sys_index`
VALUES (1676561487365881856, NULL, '2023-07-05 19:59:42');
INSERT INTO `sys_index`
VALUES (1676561519313895424, NULL, '2023-07-05 19:59:49');
INSERT INTO `sys_index`
VALUES (1676561535289999360, NULL, '2023-07-05 19:59:53');
INSERT INTO `sys_index`
VALUES (1676561778807095296, NULL, '2023-07-05 20:00:51');
INSERT INTO `sys_index`
VALUES (1676561946243710976, NULL, '2023-07-05 20:01:31');
INSERT INTO `sys_index`
VALUES (1676562250443997184, NULL, '2023-07-05 20:02:44');
INSERT INTO `sys_index`
VALUES (1676562345142992896, NULL, '2023-07-05 20:03:06');
INSERT INTO `sys_index`
VALUES (1676563581430874112, NULL, '2023-07-05 20:08:01');
INSERT INTO `sys_index`
VALUES (1676563656752185344, NULL, '2023-07-05 20:08:19');
INSERT INTO `sys_index`
VALUES (1676563822649491456, NULL, '2023-07-05 20:08:59');
INSERT INTO `sys_index`
VALUES (1676563986890047488, NULL, '2023-07-05 20:09:38');
INSERT INTO `sys_index`
VALUES (1676564818960269312, NULL, '2023-07-05 20:12:56');
INSERT INTO `sys_index`
VALUES (1676564909074890752, NULL, '2023-07-05 20:13:18');
INSERT INTO `sys_index`
VALUES (1676565591379099648, NULL, '2023-07-05 20:16:00');
INSERT INTO `sys_index`
VALUES (1676565806949548032, NULL, '2023-07-05 20:16:52');
INSERT INTO `sys_index`
VALUES (1676565997966540800, NULL, '2023-07-05 20:17:37');
INSERT INTO `sys_index`
VALUES (1676566334651711488, NULL, '2023-07-05 20:18:57');
INSERT INTO `sys_index`
VALUES (1676566804552171520, NULL, '2023-07-05 20:20:49');
INSERT INTO `sys_index`
VALUES (1676566821572657152, NULL, '2023-07-05 20:20:54');
INSERT INTO `sys_index`
VALUES (1676566840195366912, NULL, '2023-07-05 20:20:58');
INSERT INTO `sys_index`
VALUES (1676566981157535744, NULL, '2023-07-05 20:21:32');
INSERT INTO `sys_index`
VALUES (1676566992431824896, NULL, '2023-07-05 20:21:34');
INSERT INTO `sys_index`
VALUES (1676567064750014464, NULL, '2023-07-05 20:21:52');
INSERT INTO `sys_index`
VALUES (1676567079325220864, NULL, '2023-07-05 20:21:55');
INSERT INTO `sys_index`
VALUES (1676567094974169088, NULL, '2023-07-05 20:21:59');
INSERT INTO `sys_index`
VALUES (1676568881437294592, NULL, '2023-07-05 20:29:05');
INSERT INTO `sys_index`
VALUES (1676568894984896512, NULL, '2023-07-05 20:29:08');
INSERT INTO `sys_index`
VALUES (1676568937439641600, NULL, '2023-07-05 20:29:18');
INSERT INTO `sys_index`
VALUES (1676568964593565696, NULL, '2023-07-05 20:29:24');
INSERT INTO `sys_index`
VALUES (1676569052669755392, NULL, '2023-07-05 20:29:45');
INSERT INTO `sys_index`
VALUES (1676569065084895232, NULL, '2023-07-05 20:29:48');
INSERT INTO `sys_index`
VALUES (1676569075436437504, NULL, '2023-07-05 20:29:51');
INSERT INTO `sys_index`
VALUES (1676569087738331136, NULL, '2023-07-05 20:29:54');
INSERT INTO `sys_index`
VALUES (1676569232030777344, NULL, '2023-07-05 20:30:28');
INSERT INTO `sys_index`
VALUES (1676569243753857024, NULL, '2023-07-05 20:30:31');
INSERT INTO `sys_index`
VALUES (1676569259658657792, NULL, '2023-07-05 20:30:35');
INSERT INTO `sys_index`
VALUES (1676569271855693824, NULL, '2023-07-05 20:30:38');
INSERT INTO `sys_index`
VALUES (1676569794591801344, NULL, '2023-07-05 20:32:42');
INSERT INTO `sys_index`
VALUES (1676569809502552064, NULL, '2023-07-05 20:32:46');
INSERT INTO `sys_index`
VALUES (1676569829471633408, NULL, '2023-07-05 20:32:51');
INSERT INTO `sys_index`
VALUES (1676569852062154752, NULL, '2023-07-05 20:32:56');
INSERT INTO `sys_index`
VALUES (1676570060447760384, NULL, '2023-07-05 20:33:46');
INSERT INTO `sys_index`
VALUES (1676570279566589952, NULL, '2023-07-05 20:34:38');
INSERT INTO `sys_index`
VALUES (1676570307966222336, NULL, '2023-07-05 20:34:45');
INSERT INTO `sys_index`
VALUES (1676570320469442560, NULL, '2023-07-05 20:34:48');
INSERT INTO `sys_index`
VALUES (1676570332343517184, NULL, '2023-07-05 20:34:51');
INSERT INTO `sys_index`
VALUES (1676570463646203904, NULL, '2023-07-05 20:35:22');
INSERT INTO `sys_index`
VALUES (1676570474421370880, NULL, '2023-07-05 20:35:24');
INSERT INTO `sys_index`
VALUES (1676570486081536000, NULL, '2023-07-05 20:35:27');
INSERT INTO `sys_index`
VALUES (1676571031399776256, NULL, '2023-07-05 20:37:37');
INSERT INTO `sys_index`
VALUES (1676571414046130176, NULL, '2023-07-05 20:39:08');
INSERT INTO `sys_index`
VALUES (1676571471835250688, NULL, '2023-07-05 20:39:22');
INSERT INTO `sys_index`
VALUES (1676571545965379584, NULL, '2023-07-05 20:39:40');
INSERT INTO `sys_index`
VALUES (1676571602521374720, NULL, '2023-07-05 20:39:53');
INSERT INTO `sys_index`
VALUES (1676571844960534528, NULL, '2023-07-05 20:40:51');
INSERT INTO `sys_index`
VALUES (1676572017497423872, NULL, '2023-07-05 20:41:32');
INSERT INTO `sys_index`
VALUES (1676572084870529024, NULL, '2023-07-05 20:41:48');
INSERT INTO `sys_index`
VALUES (1676572648798896128, NULL, '2023-07-05 20:44:03');
INSERT INTO `sys_index`
VALUES (1676572686828650496, NULL, '2023-07-05 20:44:12');
INSERT INTO `sys_index`
VALUES (1676572716385910784, NULL, '2023-07-05 20:44:19');
INSERT INTO `sys_index`
VALUES (1676572815946104832, NULL, '2023-07-05 20:44:43');
INSERT INTO `sys_index`
VALUES (1676572933902516224, NULL, '2023-07-05 20:45:11');
INSERT INTO `sys_index`
VALUES (1676572952177098752, NULL, '2023-07-05 20:45:15');
INSERT INTO `sys_index`
VALUES (1676572971781275648, NULL, '2023-07-05 20:45:20');
INSERT INTO `sys_index`
VALUES (1676573717817933824, NULL, '2023-07-05 20:48:18');
INSERT INTO `sys_index`
VALUES (1676573939772112896, NULL, '2023-07-05 20:49:11');
INSERT INTO `sys_index`
VALUES (1676574003911409664, NULL, '2023-07-05 20:49:26');
INSERT INTO `sys_index`
VALUES (1676574048337477632, NULL, '2023-07-05 20:49:37');
INSERT INTO `sys_index`
VALUES (1676574532808949760, NULL, '2023-07-05 20:51:32');
INSERT INTO `sys_index`
VALUES (1676576128355745792, NULL, '2023-07-05 20:57:52');
INSERT INTO `sys_index`
VALUES (1676576206269136896, NULL, '2023-07-05 20:58:11');
INSERT INTO `sys_index`
VALUES (1676576245431353344, NULL, '2023-07-05 20:58:20');
INSERT INTO `sys_index`
VALUES (1676576312456331264, NULL, '2023-07-05 20:58:36');
INSERT INTO `sys_index`
VALUES (1676576345830408192, NULL, '2023-07-05 20:58:44');
INSERT INTO `sys_index`
VALUES (1676588051101073408, NULL, '2023-07-05 21:45:15');
INSERT INTO `sys_index`
VALUES (1676588109120880640, NULL, '2023-07-05 21:45:29');
INSERT INTO `sys_index`
VALUES (1676588758063595520, NULL, '2023-07-05 21:48:04');
INSERT INTO `sys_index`
VALUES (1676588972958760960, NULL, '2023-07-05 21:48:55');
INSERT INTO `sys_index`
VALUES (1676589157512331264, NULL, '2023-07-05 21:49:39');
INSERT INTO `sys_index`
VALUES (1676589493908094976, NULL, '2023-07-05 21:50:59');
INSERT INTO `sys_index`
VALUES (1676589531078017024, NULL, '2023-07-05 21:51:08');
INSERT INTO `sys_index`
VALUES (1676589654688350208, NULL, '2023-07-05 21:51:37');
INSERT INTO `sys_index`
VALUES (1676589817762889728, NULL, '2023-07-05 21:52:16');
INSERT INTO `sys_index`
VALUES (1676589830907838464, NULL, '2023-07-05 21:52:19');
INSERT INTO `sys_index`
VALUES (1676589855389990912, NULL, '2023-07-05 21:52:25');
INSERT INTO `sys_index`
VALUES (1676589960209842176, NULL, '2023-07-05 21:52:50');
INSERT INTO `sys_index`
VALUES (1676589971463159808, NULL, '2023-07-05 21:52:53');
INSERT INTO `sys_index`
VALUES (1676589984406781952, NULL, '2023-07-05 21:52:56');
INSERT INTO `sys_index`
VALUES (1676590226028052480, NULL, '2023-07-05 21:53:54');
INSERT INTO `sys_index`
VALUES (1676590264523374592, NULL, '2023-07-05 21:54:03');
INSERT INTO `sys_index`
VALUES (1676590277668323328, NULL, '2023-07-05 21:54:06');
INSERT INTO `sys_index`
VALUES (1676590291446611968, NULL, '2023-07-05 21:54:09');
INSERT INTO `sys_index`
VALUES (1676590351349661696, NULL, '2023-07-05 21:54:23');
INSERT INTO `sys_index`
VALUES (1676590362955300864, NULL, '2023-07-05 21:54:26');
INSERT INTO `sys_index`
VALUES (1676590377966714880, NULL, '2023-07-05 21:54:30');
INSERT INTO `sys_index`
VALUES (1676590744246894592, NULL, '2023-07-05 21:55:57');
INSERT INTO `sys_index`
VALUES (1676590754204172288, NULL, '2023-07-05 21:56:00');
INSERT INTO `sys_index`
VALUES (1676590766321516544, NULL, '2023-07-05 21:56:02');
INSERT INTO `sys_index`
VALUES (1676590805047525376, NULL, '2023-07-05 21:56:12');
INSERT INTO `sys_index`
VALUES (1676590816233734144, NULL, '2023-07-05 21:56:14');
INSERT INTO `sys_index`
VALUES (1676590829336739840, NULL, '2023-07-05 21:56:17');
INSERT INTO `sys_index`
VALUES (1676590957346897920, NULL, '2023-07-05 21:56:48');
INSERT INTO `sys_index`
VALUES (1676591364106305536, NULL, '2023-07-05 21:58:25');
INSERT INTO `sys_index`
VALUES (1676591444053934080, NULL, '2023-07-05 21:58:44');
INSERT INTO `sys_index`
VALUES (1676591730675892224, NULL, '2023-07-05 21:59:52');
INSERT INTO `sys_index`
VALUES (1676592915948453888, NULL, '2023-07-05 22:04:35');
INSERT INTO `sys_index`
VALUES (1676592928069992448, NULL, '2023-07-05 22:04:38');
INSERT INTO `sys_index`
VALUES (1676592946462015488, NULL, '2023-07-05 22:04:42');
INSERT INTO `sys_index`
VALUES (1676593354953670656, NULL, '2023-07-05 22:06:20');
INSERT INTO `sys_index`
VALUES (1676593365275852800, NULL, '2023-07-05 22:06:22');
INSERT INTO `sys_index`
VALUES (1676593376776634368, NULL, '2023-07-05 22:06:25');
INSERT INTO `sys_index`
VALUES (1676593841971085312, NULL, '2023-07-05 22:08:16');
INSERT INTO `sys_index`
VALUES (1676593851936751616, NULL, '2023-07-05 22:08:18');
INSERT INTO `sys_index`
VALUES (1676593875580043264, NULL, '2023-07-05 22:08:24');

-- ----------------------------
-- Table structure for sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE `sys_logs`
(
    `log_id`      bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '主键',
    `log_desc`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '描述',
    `operation`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL COMMENT '操作类型',
    `method`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '请求方法',
    `params`      varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '请求参数',
    `username`    varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL COMMENT '操作人',
    `ip`          varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT 'ip地址',
    `time`        bigint                                                        NOT NULL COMMENT '执行时长',
    `create_time` datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
    PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1668598193651109890
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '日志表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logs
-- ----------------------------
INSERT INTO `sys_logs`
VALUES (1668257096047480833, '新增用户', '1', 'SysUserController.update()',
        '[{\"age\":22,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":1,\"email\":\"xx_2589@163.com\",\"id\":2,\"isDelete\":\"0\",\"isUse\":\"1\",\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"opera\",\"roleId\":2,\"roleName\":\"操作员\"}],\"sex\":\"0\",\"telephone\":\"17388888887\",\"updateTime\":1683108613000,\"username\":\"222\"}]',
        'admin', '127.0.0.1', 72, '2023-06-12 22:01:01');
INSERT INTO `sys_logs`
VALUES (1668598193651109890, '修改用户数据', '2', 'SysUserController.update()',
        '[{\"age\":22,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":1,\"email\":\"xx_2589@163.com\",\"id\":2,\"isDelete\":\"0\",\"isUse\":\"1\",\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"opera\",\"roleId\":2,\"roleName\":\"操作员\"}],\"sex\":\"0\",\"telephone\":\"17388888887\",\"updateTime\":1683108613000,\"username\":\"222\"}]',
        'admin', '127.0.0.1', 76, '2023-06-13 20:36:25');

-- ----------------------------
-- Table structure for sys_logs_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs_login`;
CREATE TABLE `sys_logs_login`
(
    `id`          bigint                                                  NOT NULL AUTO_INCREMENT,
    `user_name`   varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '登录账号',
    `ip_addr`     varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT 'ip地址',
    `login_time`  datetime                                                NULL DEFAULT NULL COMMENT '登录时间',
    `login_mac`   varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录设备',
    `create_time` datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1676940421148311555
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logs_login
-- ----------------------------
INSERT INTO `sys_logs_login`
VALUES (1668606173377449985, 'admin', '127.0.0.1', '2023-06-13 21:08:08', 'Chrome', '2023-06-13 21:08:07');
INSERT INTO `sys_logs_login`
VALUES (1668617709655293953, 'admin', '127.0.0.1', '2023-06-13 21:53:58', 'Chrome', '2023-06-13 21:53:58');
INSERT INTO `sys_logs_login`
VALUES (1668617980569595905, 'admin', '127.0.0.1', '2023-06-13 21:55:03', 'Chrome', '2023-06-13 21:55:02');
INSERT INTO `sys_logs_login`
VALUES (1675846024193323009, 'admin', '127.0.0.1', '2023-07-03 20:36:43', 'Chrome', '2023-07-03 20:36:42');
INSERT INTO `sys_logs_login`
VALUES (1676190058011140097, 'admin', '127.0.0.1', '2023-07-04 19:23:47', 'Chrome', '2023-07-04 19:23:46');
INSERT INTO `sys_logs_login`
VALUES (1676553936062611458, 'admin', '127.0.0.1', '2023-07-05 19:29:42', 'Chrome', '2023-07-05 19:29:41');
INSERT INTO `sys_logs_login`
VALUES (1676940421148311554, 'admin', '127.0.0.1', '2023-07-06 21:05:27', 'Chrome', '2023-07-06 21:05:27');
INSERT INTO `sys_logs_login`
VALUES (1676941975045730305, 'admin', '127.0.0.1', '2023-07-06 21:11:38', 'Chrome', '2023-07-06 21:11:37');
INSERT INTO `sys_logs_login`
VALUES (1676942914901192706, 'admin', '127.0.0.1', '2023-07-06 21:15:22', 'Chrome', '2023-07-06 21:15:21');
INSERT INTO `sys_logs_login`
VALUES (1676943154790215681, 'admin', '127.0.0.1', '2023-07-06 21:16:19', 'Chrome', '2023-07-06 21:16:18');
INSERT INTO `sys_logs_login`
VALUES (1676943789522636801, 'admin', '127.0.0.1', '2023-07-06 21:18:50', 'Chrome', '2023-07-06 21:18:50');
INSERT INTO `sys_logs_login`
VALUES (1676951151826391041, 'admin', '127.0.0.1', '2023-07-06 21:48:05', 'Chrome', '2023-07-06 21:48:05');
INSERT INTO `sys_logs_login`
VALUES (1676951157409009665, 'admin', '127.0.0.1', '2023-07-06 21:48:07', 'Chrome', '2023-07-06 21:48:06');
INSERT INTO `sys_logs_login`
VALUES (1676951198777430018, 'admin', '127.0.0.1', '2023-07-06 21:48:17', 'Chrome', '2023-07-06 21:48:16');
INSERT INTO `sys_logs_login`
VALUES (1676951623375134721, 'admin', '127.0.0.1', '2023-07-06 21:49:58', 'Chrome', '2023-07-06 21:49:57');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `menu_id`     bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '主键',
    `p_menu_id`   bigint                                                        NULL     DEFAULT NULL COMMENT '父权限id',
    `menu_name`   varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL COMMENT '权限名称',
    `url`         varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NULL     DEFAULT NULL COMMENT '权限路径',
    `menu_auth`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '权限标识',
    `is_hidden`   char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      NULL     DEFAULT NULL COMMENT '是否隐藏:0否；1是',
    `menu_type`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci   NULL     DEFAULT NULL COMMENT '菜单类型: 0目录 1菜单 2按钮',
    `show_type`   char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      NULL     DEFAULT NULL COMMENT '展示类型: 第一栏,第二栏,第三栏',
    `menu_icon`   varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '菜单图标',
    `is_sort`     int                                                           NULL     DEFAULT NULL COMMENT '排序等级',
    `remark`      varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NULL     DEFAULT NULL COMMENT '权限描述',
    `create_time` datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `update_time` datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `is_delete`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci   NULL     DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
    PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 30
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '菜单表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu`
VALUES (1, 0, '系统管理', '/system', 'sys', '0', '0', '0', 'el-icon-s-home', 1, NULL, '2023-05-01 14:47:21', '2023-05-05 23:15:26', '0');
INSERT INTO `sys_menu`
VALUES (2, 1, '用户管理', '/system/user', 'sys:user', '0', '1', '0', 'el-icon-user', 1, NULL, '2023-05-01 14:47:56', '2023-05-05 23:14:41', '0');
INSERT INTO `sys_menu`
VALUES (3, 2, '新增', NULL, 'sys:user:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:48:27', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (4, 2, '修改', NULL, 'sys:user:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:48:43', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (5, 2, '删除', NULL, 'sys:user:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:00', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (6, 2, '导入', NULL, 'sys:user:import', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:38', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (7, 2, '导出', NULL, 'sys:user:export', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:51', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (8, 1, '角色管理', '/system/role', 'sys:role', '0', '1', '0', 'el-icon-bell', 2, NULL, '2023-05-01 14:50:21', '2023-05-05 23:15:40', '0');
INSERT INTO `sys_menu`
VALUES (9, 8, '新增', NULL, 'sys:role:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:50:39', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (10, 8, '修改', NULL, 'sys:role:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:51:03', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (11, 8, '删除', NULL, 'sys:role:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:51:53', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (12, 1, '菜单管理', '/system/menu', 'sys:menu', '0', '1', '0', 'el-icon-s-data', 3, NULL, '2023-05-01 14:52:20', '2023-05-05 23:15:56', '0');
INSERT INTO `sys_menu`
VALUES (13, 12, '新增', NULL, 'sys:menu:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:52:34', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (14, 12, '修改', NULL, 'sys:menu:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:52:53', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (15, 12, '删除', NULL, 'sys:menu:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:53:08', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu`
VALUES (16, 1, '部门管理', '/system/dept', 'sys:dept', '0', '1', '0', 'el-icon-money', 4, NULL, '2023-05-07 14:46:15', '2023-05-07 14:46:15', '0');
INSERT INTO `sys_menu`
VALUES (17, 16, '新增', NULL, 'sys:dept:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:19:36', '2023-05-08 22:24:35', '0');
INSERT INTO `sys_menu`
VALUES (18, 16, '修改', NULL, 'sys:dept:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:20:00', '2023-05-08 22:24:33', '0');
INSERT INTO `sys_menu`
VALUES (19, 16, '删除', NULL, 'sys:dept:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:20:21', '2023-05-08 22:24:32', '0');
INSERT INTO `sys_menu`
VALUES (20, 2, '查看', NULL, 'sys:user:select', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:24:30', '2023-05-08 22:24:30', '0');
INSERT INTO `sys_menu`
VALUES (21, 0, '日志管理', '/logs', 'sys:log', '0', '0', '0', 'el-icon-question', 2, NULL, '2023-06-02 22:23:22', '2023-06-02 22:23:22', '0');
INSERT INTO `sys_menu`
VALUES (22, 21, '操作日志', '/logs/operate', 'logs:operate', '0', '1', '0', 'el-icon-info', 1, NULL, '2023-06-02 22:24:26', '2023-06-02 22:24:26', '0');
INSERT INTO `sys_menu`
VALUES (23, 21, '登录日志', '/logs/login', 'logs:login', '0', '1', '0', 'el-icon-circle-plus', 2, NULL, '2023-06-02 22:37:18', '2023-06-02 22:37:18', '0');
INSERT INTO `sys_menu`
VALUES (24, 0, '系统工具', '/tools', 'sys:tools', '0', '0', '1', 'el-icon-s-tools', 3, NULL, '2023-06-03 18:46:01', '2023-06-03 18:46:01', '0');
INSERT INTO `sys_menu`
VALUES (25, 24, '代码生成', '/tools/gen', 'tools:gen', '0', '1', '1', 'el-icon-setting', 1, NULL, '2023-06-03 18:46:42', '2023-06-03 18:46:42', '0');
INSERT INTO `sys_menu`
VALUES (26, 24, '修改配置', '/tools/gen/editTable', 'tools:table', '1', '1', '1', 'el-icon-video-camera-solid', 2, NULL, '2023-06-04 13:59:50', '2023-06-04 13:59:50', '0');
INSERT INTO `sys_menu`
VALUES (27, 24, '字典管理', '/tools/dict', 'tools/dict', '0', '1', '1', 'el-icon-s-help', 3, NULL, '2023-06-06 20:40:29', '2023-06-06 20:40:29', '0');
INSERT INTO `sys_menu`
VALUES (28, 24, '字典配置', '/tools/dict/data', 'dict:data', '1', '1', '1', 'el-icon-picture', 4, NULL, '2023-06-10 19:25:05', '2023-06-10 19:25:05', '0');
INSERT INTO `sys_menu`
VALUES (30, 0, '定制业务', '/subject', 'sub:index', '0', '0', '1', 'el-icon-s-shop', 4, NULL, '2023-07-06 21:42:31', '2023-07-06 21:42:31', '0');
INSERT INTO `sys_menu`
VALUES (31, 30, '业务模板', '/subject/subject', 'subject:subject', '0', '1', '1', 'el-icon-s-goods', 1, NULL, '2023-07-06 22:02:20', '2023-07-06 22:02:20', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `role_id`     bigint                                                       NOT NULL AUTO_INCREMENT COMMENT '主键',
    `role_name`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
    `role_auth`   varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '角色权限标识',
    `is_sort`     varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '排序字段',
    `remark`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '角色描述',
    `create_time` datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime                                                     NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `is_delete`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
    PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '角色表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role`
VALUES (1, '管理员', 'system', '1', '', '2023-05-01 14:46:19', '2023-05-04 22:26:06', '0');
INSERT INTO `sys_role`
VALUES (2, '操作员', 'opera', '2', NULL, '2023-05-05 21:47:54', '2023-05-05 21:48:15', '0');
INSERT INTO `sys_role`
VALUES (3, '审核员', 'check', '2', NULL, '2023-07-03 20:37:10', '2023-07-03 20:37:10', '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`
(
    `role_id` bigint NOT NULL COMMENT '角色id',
    `menu_id` bigint NOT NULL COMMENT '权限id',
    PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '角色权限表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu`
VALUES (1, 1);
INSERT INTO `sys_role_menu`
VALUES (1, 2);
INSERT INTO `sys_role_menu`
VALUES (1, 3);
INSERT INTO `sys_role_menu`
VALUES (1, 4);
INSERT INTO `sys_role_menu`
VALUES (1, 5);
INSERT INTO `sys_role_menu`
VALUES (1, 6);
INSERT INTO `sys_role_menu`
VALUES (1, 7);
INSERT INTO `sys_role_menu`
VALUES (1, 8);
INSERT INTO `sys_role_menu`
VALUES (1, 9);
INSERT INTO `sys_role_menu`
VALUES (1, 10);
INSERT INTO `sys_role_menu`
VALUES (1, 11);
INSERT INTO `sys_role_menu`
VALUES (1, 12);
INSERT INTO `sys_role_menu`
VALUES (1, 13);
INSERT INTO `sys_role_menu`
VALUES (1, 14);
INSERT INTO `sys_role_menu`
VALUES (1, 15);
INSERT INTO `sys_role_menu`
VALUES (1, 16);
INSERT INTO `sys_role_menu`
VALUES (1, 17);
INSERT INTO `sys_role_menu`
VALUES (1, 18);
INSERT INTO `sys_role_menu`
VALUES (1, 19);
INSERT INTO `sys_role_menu`
VALUES (1, 20);
INSERT INTO `sys_role_menu`
VALUES (1, 21);
INSERT INTO `sys_role_menu`
VALUES (1, 22);
INSERT INTO `sys_role_menu`
VALUES (1, 23);
INSERT INTO `sys_role_menu`
VALUES (1, 24);
INSERT INTO `sys_role_menu`
VALUES (1, 25);
INSERT INTO `sys_role_menu`
VALUES (1, 26);
INSERT INTO `sys_role_menu`
VALUES (1, 27);
INSERT INTO `sys_role_menu`
VALUES (1, 28);
INSERT INTO `sys_role_menu`
VALUES (1, 29);
INSERT INTO `sys_role_menu`
VALUES (1, 30);
INSERT INTO `sys_role_menu`
VALUES (1, 31);
INSERT INTO `sys_role_menu`
VALUES (2, 1);
INSERT INTO `sys_role_menu`
VALUES (2, 12);
INSERT INTO `sys_role_menu`
VALUES (2, 13);
INSERT INTO `sys_role_menu`
VALUES (2, 14);
INSERT INTO `sys_role_menu`
VALUES (2, 15);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `id`          bigint                                                        NOT NULL AUTO_INCREMENT COMMENT '主键',
    `username`    varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL COMMENT '用户名',
    `password`    varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '密码',
    `name`        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL COMMENT '姓名',
    `avatar`      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL     DEFAULT NULL COMMENT '头像',
    `telephone`   varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NOT NULL COMMENT '手机号',
    `age`         int                                                           NULL     DEFAULT NULL COMMENT '年龄',
    `sex`         varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci   NULL     DEFAULT NULL COMMENT '性别：0:男；1:女',
    `email`       varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NULL     DEFAULT NULL COMMENT '邮箱',
    `dept_id`     int                                                           NULL     DEFAULT NULL COMMENT '所属部门id',
    `is_use`      varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci   NULL     DEFAULT '1' COMMENT '是否启用：0未启用；1已启用',
    `is_delete`   varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci   NULL     DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
    `remark`      varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  NULL     DEFAULT NULL COMMENT '备注',
    `create_time` datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `login_time`  datetime                                                      NULL     DEFAULT NULL COMMENT '登录时间',
    `update_time` datetime                                                      NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `username` (`username`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '用户表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user`
VALUES (1, 'admin', '$2a$10$7MwTBtKeIDmSYoG40Zfdsu49ZQPwjKlI7qcLPjI1ij0CESLtanb4q', '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '17388888888', NULL, '1', NULL, 2, '1', '0', NULL, '2023-05-01 13:53:20',
        '2023-07-06 21:49:58', '2023-06-13 21:08:07');
INSERT INTO `sys_user`
VALUES (2, '222', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '17388888887', 22, '0', 'xx_2589@163.com', 1, '1', '0', NULL, '2023-05-01 13:53:20', NULL,
        '2023-05-03 18:10:13');
INSERT INTO `sys_user`
VALUES (3, '333', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '17388888888', 12, '1', 'xx_2589@163.com', 4, '1', '0', NULL, '2023-05-01 13:53:20', NULL,
        '2023-05-03 19:41:33');
INSERT INTO `sys_user`
VALUES (4, '444', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '17388888888', 12, '1', 'xx_2589@163.com', 2, '1', '0', NULL, '2023-05-01 13:53:20', NULL,
        '2023-05-03 22:28:45');
INSERT INTO `sys_user`
VALUES (5, '555', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '17388888888', NULL, '1', NULL, 2, '1', '0', NULL, '2023-05-01 13:53:20', NULL,
        '2023-06-13 18:48:27');
INSERT INTO `sys_user`
VALUES (6, '999', NULL, '李四', NULL, '18999999989', NULL, '0', NULL, 3, '0', '0', NULL, '2023-05-03 21:17:57', NULL, '2023-06-13 18:48:30');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `user_id` bigint NOT NULL COMMENT '用户id',
    `role_id` bigint NOT NULL COMMENT '角色id',
    PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT = '用户角色表'
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role`
VALUES (1, 1);
INSERT INTO `sys_user_role`
VALUES (2, 2);

CREATE TABLE `pro_inform`
(
    `id`             bigint NOT NULL AUTO_INCREMENT,
    `inform_user`    varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '接收通知人',
    `subject_id`     bigint                                                        DEFAULT NULL COMMENT '主题id',
    `inform_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '通知内容',
    `info_type`     char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT NULL COMMENT '消息类型：0收藏;1点赞;2评论;9系统',
    `inform_status`  char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT '0' COMMENT '状态：0未读；1已读',
    `create_user`    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '消息创建人',
    `create_time`    datetime                                                      DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `read_time`      datetime                                                      DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '已读时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;

CREATE TABLE `pro_record`
(
    `id`            bigint NOT NULL,
    `subject_id`    bigint                                                        DEFAULT NULL COMMENT '主题id',
    `like_comment`  char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci      DEFAULT NULL COMMENT '收藏0；点赞1；评论2',
    `comment`       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '评论内容',
    `create_user`   varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '创建人',
    `create_time`   datetime                                                      DEFAULT NULL COMMENT '创建时间',
    `subject_user`  varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci  DEFAULT NULL COMMENT '归属人',
    `comment_reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '评论回复',
    `reply_time`    datetime                                                      DEFAULT NULL COMMENT '回复时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci COMMENT ='点赞，评论，收藏记录表';

create view main_file_view as
(
SELECT si.id,
       sf.id as file_id,
       sf.file_name,
       sf.file_path,
       sf.file_type,
       sf.file_suffix,
       sf.file_size,
       sf.create_by,
       sf.create_time
FROM sys_index si
         INNER JOIN sys_file sf ON si.id = sf.index_id
    AND sf.file_type = '0'
    AND sf.is_delete = '0'
    );

SET FOREIGN_KEY_CHECKS = 1;

CREATE TABLE `pro_notice` (
                              `id` bigint NOT NULL AUTO_INCREMENT,
                              `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '通知标题',
                              `content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '通知内容',
                              `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '创建人',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='系统公告信息表';


-- ------------------v2------------------------
CREATE TABLE `uni_interval` (
                                `id` bigint NOT NULL AUTO_INCREMENT,
                                `time_interval` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '预约时间区间',
                                `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='预约时间区间表';

CREATE TABLE `uni_interval_his` (
                                    `id` bigint NOT NULL AUTO_INCREMENT,
                                    `subject_id` bigint DEFAULT NULL COMMENT 'subjectId',
                                    `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题',
                                    `app_day` datetime DEFAULT NULL COMMENT '预约的天',
                                    `time_interval` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '区间',
                                    `create_user` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '预约人',
                                    `user_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '预约人code',
                                    `appoint_status` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '预约状态：0已预约；1已完成；2已取消',
                                    `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                                    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='预约历史表';

-- ---------------------v3-----------------------
CREATE TABLE `uni_address` (
                               `id` bigint NOT NULL,
                               `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                               `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
                               `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '地址',
                               `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
                               `is_default` char(1) CHARACTER SET utf8 DEFAULT '0' COMMENT '是否默认地址：0否；1是',
                               `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='收货地址表';

CREATE TABLE `uni_order` (
                             `id` bigint NOT NULL,
                             `subject_id` bigint DEFAULT NULL COMMENT 'subject_id',
                             `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '订单编号',
                             `title` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '标题',
                             `username` bigint DEFAULT NULL COMMENT '购买人username',
                             `buy_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收货人',
                             `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '收货地址',
                             `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '手机号',
                             `unit_price` decimal(10,2) DEFAULT NULL COMMENT '单价',
                             `buy_num` int DEFAULT NULL COMMENT '购买数量',
                             `total_price` decimal(10,2) DEFAULT NULL COMMENT '总价',
                             `order_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '订单状态:0待付款；1待发货；2待评价；3完成；4取消',
                             `express_no` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '快递单号',
                             `evaluate_star` int DEFAULT NULL COMMENT '评价星级⭐',
                             `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                             `pay_time` datetime DEFAULT NULL COMMENT '付款时间',
                             `send_time` datetime DEFAULT NULL COMMENT '发货时间',
                             `shipped_time` datetime DEFAULT NULL COMMENT '收货时间',
                             `evaluated_time` datetime DEFAULT NULL COMMENT '评价时间',
                             `end_time` datetime DEFAULT NULL COMMENT '订单结束时间',
                             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='订单记录表';

CREATE TABLE `type_teach` (
                              `id` bigint NOT NULL AUTO_INCREMENT,
                              `index_id` bigint DEFAULT NULL,
                              `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '姓名',
                              `age` int DEFAULT NULL COMMENT '年龄',
                              `address` varchar(255) DEFAULT NULL COMMENT '地址',
                              `subejct` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '科目',
                              `create_time` datetime DEFAULT NULL COMMENT '创建时间',
                              `dict_code` varchar(255) DEFAULT NULL,
                              PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='类型案例表';