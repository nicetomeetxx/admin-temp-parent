/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : admin-temp

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 04/08/2023 09:17:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1665256079496237057, 'sys_menu', '菜单表', NULL, NULL, 'SysMenu', 'crud', 'cn.xu.test', 'system', 'menu', '菜单', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":\"2\"}', NULL, '2023-05-04 22:05:33', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1665256079567540238, 'sys_user', '用户表', NULL, NULL, 'SysUser', 'crud', 'com.ruoyi.system', 'system', 'user', '用户', 'ljxu', '0', '/', '{\"parentMenuId\":2}', NULL, '2023-05-04 22:05:51', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1668250038367584257, 'sys_logs', '日志表', NULL, NULL, 'SysLogs', 'crud', 'cn.xu.tools.logs', 'logs', 'logs', '操作日志', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":21}', NULL, '2023-04-23 20:41:19', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1676950524190740482, 'pro_subject', '首页业务表', NULL, NULL, 'ProSubject', 'crud', 'cn.xu.subject', 'subject', 'subject', '首页业务', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":\"30\"}', 'admin', '2023-07-06 21:36:49', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1684735487847464962, 'uni_interval', '预约时间区间表', NULL, NULL, 'UniInterval', 'crud', 'cn.xu.work', 'work', 'interval', '预约时间区间', 'ljxu', '0', '/', NULL, 'admin', '2023-07-27 14:51:25', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1684745278141530114, 'uni_interval_his', '预约历史表', NULL, NULL, 'UniIntervalHis', 'crud', 'cn.xu.work', 'work', 'intervalHis', '预约历史', 'ljxu', '0', '/', NULL, 'admin', '2023-07-28 09:57:07', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1686212273680723970, 'uni_address', '收货地址表', NULL, NULL, 'UniAddress', 'crud', 'cn.xu.work', 'work', 'address', '收货地址', 'ljxu', '0', '/', NULL, 'admin', '2023-08-01 09:15:39', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1686300402265665537, 'uni_order', '订单记录表', NULL, NULL, 'UniOrder', 'crud', 'cn.xu.work', 'work', 'order', '订单记录', 'ljxu', '0', '/', NULL, 'admin', '2023-08-01 15:25:01', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL COMMENT '编号',
  `table_id` bigint NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1665256079496237058, 1665256079496237057, 'menu_id', '主键', 'bigint', 'Long', 'menuId', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540225, 1665256079496237057, 'p_menu_id', '父权限id', 'bigint', 'Long', 'pMenuId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540226, 1665256079496237057, 'menu_name', '权限名称', 'varchar(20)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540227, 1665256079496237057, 'url', '权限路径', 'varchar(40)', 'String', 'url', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540228, 1665256079496237057, 'menu_auth', '权限标识', 'varchar(255)', 'String', 'menuAuth', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'fileUpload', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540229, 1665256079496237057, 'is_hidden', '是否隐藏:0否；1是', 'char(1)', 'String', 'isHidden', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540230, 1665256079496237057, 'menu_type', '菜单类型: 0目录 1菜单 2按钮', 'varchar(1)', 'String', 'menuType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540231, 1665256079496237057, 'show_type', '展示类型: 第一栏,第二栏,第三栏', 'char(1)', 'String', 'showType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540232, 1665256079496237057, 'menu_icon', '菜单图标', 'varchar(255)', 'String', 'menuIcon', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540233, 1665256079496237057, 'is_sort', '排序等级', 'int', 'Long', 'isSort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540234, 1665256079496237057, 'remark', '权限描述', 'varchar(50)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 11, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540235, 1665256079496237057, 'create_time', '', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 12, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540236, 1665256079496237057, 'update_time', '', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 13, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540237, 1665256079496237057, 'is_delete', '是否删除: 0未删除；1已删除', 'varchar(1)', 'String', 'isDelete', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 14, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649090, 1665256079567540238, 'id', '主键', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649091, 1665256079567540238, 'username', '用户名', 'varchar(30)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649092, 1665256079567540238, 'password', '密码', 'varchar(300)', 'String', 'password', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649093, 1665256079567540238, 'name', '姓名', 'varchar(20)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649094, 1665256079567540238, 'avatar', '头像', 'varchar(255)', 'String', 'avatar', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649095, 1665256079567540238, 'telephone', '手机号', 'varchar(11)', 'String', 'telephone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649096, 1665256079567540238, 'age', '年龄', 'int', 'Long', 'age', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649097, 1665256079567540238, 'sex', '性别：0:男；1:女', 'varchar(1)', 'String', 'sex', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649098, 1665256079567540238, 'email', '邮箱', 'varchar(50)', 'String', 'email', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649099, 1665256079567540238, 'dept_id', '所属部门id', 'int', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649100, 1665256079567540238, 'is_use', '是否启用：0未启用；1已启用', 'varchar(1)', 'String', 'isUse', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 11, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649101, 1665256079567540238, 'is_delete', '是否删除: 0未删除；1已删除', 'varchar(1)', 'String', 'isDelete', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 12, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649102, 1665256079567540238, 'remark', '备注', 'varchar(30)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 13, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649103, 1665256079567540238, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 14, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649104, 1665256079567540238, 'login_time', '登录时间', 'datetime', 'Date', 'loginTime', '0', '0', '1', '1', '1', '0', '0', 'EQ', 'datetime', '', 15, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649105, 1665256079567540238, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 16, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750081, 1668250038367584257, 'log_id', '主键', 'bigint', 'Long', 'logId', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750082, 1668250038367584257, 'operation', '操作类型', 'varchar(20)', 'String', 'operation', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750083, 1668250038367584257, 'method', '请求方法', 'varchar(255)', 'String', 'method', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750084, 1668250038367584257, 'params', '请求参数', 'varchar(500)', 'String', 'params', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664642, 1668250038367584257, 'username', '操作人', 'varchar(50)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664643, 1668250038367584257, 'ip', 'ip地址', 'varchar(255)', 'String', 'ip', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664644, 1668250038367584257, 'time', '执行时长', 'bigint', 'Long', 'time', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664645, 1668250038367584257, 'create_time', '操作时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849345, 1676950524190740482, 'id', '', 'bigint', 'Long', 'id', '1', '1', '0', NULL, '0', '0', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849346, 1676950524190740482, 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849347, 1676950524190740482, 'index_id', '图片', 'bigint', 'Long', 'indexId', '0', '0', '0', '1', '1', '0', '0', 'EQ', 'imageUpload', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849348, 1676950524190740482, 'main_text', '正文', 'text', 'String', 'mainText', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849349, 1676950524190740482, 'type', '类型', 'varchar(128)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'subject_type', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849350, 1676950524190740482, 'collect_num', '收藏数量', 'bigint', 'Long', 'collectNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849351, 1676950524190740482, 'like_num', '点赞数量', 'bigint', 'Long', 'likeNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849352, 1676950524190740482, 'comment_num', '评论数量', 'bigint', 'Long', 'commentNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 8, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849353, 1676950524190740482, 'create_by', '创建人', 'varchar(128)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849354, 1676950524190740482, 'create_time', ' 创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849355, 1676950524190740482, 'is_delete', '是否删除: 0未删除；1已删除', 'char(1)', 'String', 'isDelete', '0', '0', '1', '0', '0', '0', '0', 'EQ', 'input', '', 11, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684735488048791554, 1684735487847464962, 'id', '', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684735488111706113, 1684735487847464962, 'interval', '预约时间区间', 'varchar(255)', 'String', 'interval', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684735488111706114, 1684735487847464962, 'cteate_time', '创建时间', 'datetime', 'Date', 'cteateTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278208638977, 1684745278141530114, 'id', '', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278208638978, 1684745278141530114, 'subject_id', '主业务id', 'bigint', 'Long', 'subjectId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278208638979, 1684745278141530114, 'title', ' 预约业务标题', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278208638980, 1684745278141530114, 'app_day', '预约的天', 'datetime', 'Date', 'appDay', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278208638981, 1684745278141530114, 'interval', '区间', 'varchar(32)', 'String', 'interval', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278208638982, 1684745278141530114, 'create_user', '预约人', 'varchar(32)', 'String', 'createUser', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278292525057, 1684745278141530114, 'user_code', '预约人code', 'varchar(255)', 'String', 'userCode', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278292525058, 1684745278141530114, 'appoint_status', '预约状态：0已预约；1已完成；2已取消', 'char(1)', 'String', 'appointStatus', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', 8, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684745278292525059, 1684745278141530114, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 9, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273856884737, 1686212273680723970, 'id', '', 'bigint', 'Long', 'id', '1', '0', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273856884738, 1686212273680723970, 'username', '', 'varchar(32)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273856884739, 1686212273680723970, 'name', '姓名', 'varchar(32)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273949159425, 1686212273680723970, 'address', '地址', 'varchar(255)', 'String', 'address', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273949159426, 1686212273680723970, 'telephone', '手机号', 'varchar(11)', 'String', 'telephone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273949159427, 1686212273680723970, 'is_default', '是否默认地址：0否；1是', 'char(1)', 'String', 'isDefault', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686212273986908162, 1686212273680723970, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402466992130, 1686300402265665537, 'id', '', 'bigint', 'Long', 'id', '1', '0', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402466992131, 1686300402265665537, 'subject_id', 'subject_id', 'bigint', 'Long', 'subjectId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402466992132, 1686300402265665537, 'order_no', '订单编号', 'varchar(32)', 'String', 'orderNo', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402534100994, 1686300402265665537, 'title', '标题', 'varchar(24)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402534100995, 1686300402265665537, 'username', '购买人username', 'bigint', 'Long', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402534100996, 1686300402265665537, 'buy_user', '收货人', 'varchar(32)', 'String', 'buyUser', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402534100997, 1686300402265665537, 'address', '收货地址', 'varchar(255)', 'String', 'address', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402597015553, 1686300402265665537, 'telephone', '手机号', 'varchar(11)', 'String', 'telephone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 8, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402597015554, 1686300402265665537, 'unit_price', '单价', 'decimal(10,2)', 'BigDecimal', 'unitPrice', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402597015555, 1686300402265665537, 'buy_num', '购买数量', 'int', 'Long', 'buyNum', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402597015556, 1686300402265665537, 'total_price', '总价', 'decimal(10,2)', 'BigDecimal', 'totalPrice', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 11, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402668318722, 1686300402265665537, 'order_status', '订单状态:0待付款；1待发货；2待评价；3完成；4取消', 'char(1)', 'String', 'orderStatus', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', '', 12, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402668318723, 1686300402265665537, 'express_no', '快递单号', 'varchar(32)', 'String', 'expressNo', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 13, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402722844674, 1686300402265665537, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 14, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402722844675, 1686300402265665537, 'pay_time', '付款时间', 'datetime', 'Date', 'payTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 15, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402722844676, 1686300402265665537, 'send_time', '发货时间', 'datetime', 'Date', 'sendTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 16, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402722844677, 1686300402265665537, 'shipped_time', '收货时间', 'datetime', 'Date', 'shippedTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 17, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402722844678, 1686300402265665537, 'evaluated_time', '评价时间', 'datetime', 'Date', 'evaluatedTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 18, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1686300402722844679, 1686300402265665537, 'end_time', '订单结束时间', 'datetime', 'Date', 'endTime', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'datetime', '', 19, 'admin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pro_inform
-- ----------------------------
DROP TABLE IF EXISTS `pro_inform`;
CREATE TABLE `pro_inform`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `inform_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '接收通知人',
  `subject_id` bigint NULL DEFAULT NULL COMMENT '主题id',
  `inform_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '通知内容',
  `info_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '消息类型：0收藏;1点赞;2评论;9系统',
  `inform_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT '0' COMMENT '状态：0未读；1已读',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '消息创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `read_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '已读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_inform
-- ----------------------------
INSERT INTO `pro_inform` VALUES (2, 'admin', 4, '张三 @评论了您的:1111', '2', '1', 'admin', '2023-07-25 10:01:43', '2023-07-27 10:12:11');
INSERT INTO `pro_inform` VALUES (3, 'admin', 4, '张三 @评论了您的:1111', '2', '1', 'admin', '2023-07-25 10:05:55', '2023-07-27 10:12:12');
INSERT INTO `pro_inform` VALUES (4, 'admin', 4, '张三 @评论了您的:1111', '2', '1', 'admin', '2023-07-25 10:08:42', '2023-07-26 17:07:16');
INSERT INTO `pro_inform` VALUES (6, 'admin', 4, '张三 @收藏了您的:1111', '0', '1', 'admin', '2023-07-25 12:19:55', '2023-07-26 17:07:01');
INSERT INTO `pro_inform` VALUES (7, 'admin', 4, '张三 @评论了您的:1111', '2', '1', 'admin', '2023-07-25 12:22:51', '2023-07-26 13:52:32');
INSERT INTO `pro_inform` VALUES (12, 'admin', 4, '点赞了您的:1111', '1', '1', 'admin', '2023-07-25 18:59:48', '2023-07-26 13:52:24');
INSERT INTO `pro_inform` VALUES (13, 'admin', 4, '评论了您的:1111', '2', '1', 'admin', '2023-07-25 19:09:01', '2023-07-26 13:31:00');
INSERT INTO `pro_inform` VALUES (14, 'admin', 4, '点赞了您的:1111', '1', '1', 'qqqq', '2023-07-26 15:07:15', '2023-07-26 16:45:09');
INSERT INTO `pro_inform` VALUES (15, 'admin', 4, '点赞了您的:1111', '1', '1', 'qqqq', '2023-07-26 15:14:55', '2023-07-26 16:43:19');
INSERT INTO `pro_inform` VALUES (16, 'admin', 4, '收藏了您的:1111', '0', '1', 'qqqq', '2023-07-26 15:14:59', '2023-07-27 09:02:39');
INSERT INTO `pro_inform` VALUES (17, 'admin', 4, '评论了您的:1111', '2', '1', 'qqqq', '2023-07-26 15:15:29', '2023-07-26 16:01:23');

-- ----------------------------
-- Table structure for pro_notice
-- ----------------------------
DROP TABLE IF EXISTS `pro_notice`;
CREATE TABLE `pro_notice`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '通知标题',
  `content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '通知内容',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统公告信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_notice
-- ----------------------------
INSERT INTO `pro_notice` VALUES (1, '通知', '通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容通知内容', 'admin', '2023-07-26 11:03:33');

-- ----------------------------
-- Table structure for pro_record
-- ----------------------------
DROP TABLE IF EXISTS `pro_record`;
CREATE TABLE `pro_record`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `subject_id` bigint NULL DEFAULT NULL COMMENT '主题id',
  `like_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '收藏0；点赞1；评论2',
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论内容',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `subject_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '归属人',
  `comment_reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论回复',
  `reply_time` datetime NULL DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '点赞，评论，收藏记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_record
-- ----------------------------
INSERT INTO `pro_record` VALUES (1, 4, '0', NULL, 'admin', '2023-07-24 15:24:39', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (7, 4, '2', '111', 'admin', '2023-07-25 10:05:55', 'admin', '回复评论', '2023-07-27 09:38:22');
INSERT INTO `pro_record` VALUES (8, 4, '2', '333', 'admin', '2023-07-25 10:08:42', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (9, 4, '1', NULL, 'admin', '2023-07-25 12:19:50', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (11, 4, '2', '测试评论', 'admin', '2023-07-25 12:22:51', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (12, 4, '1', NULL, 'admin', '2023-07-25 14:43:48', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (13, 4, '1', NULL, 'admin', '2023-07-25 18:54:17', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (14, 4, '1', NULL, 'admin', '2023-07-25 18:56:15', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (15, 4, '1', NULL, 'admin', '2023-07-25 18:59:38', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (16, 4, '1', NULL, 'admin', '2023-07-25 18:59:48', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (17, 4, '2', '写评论', 'admin', '2023-07-25 19:09:01', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (18, 4, '1', NULL, 'qqqq', '2023-07-26 15:07:15', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (19, 4, '1', NULL, 'qqqq', '2023-07-26 15:14:55', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (20, 4, '0', NULL, 'qqqq', '2023-07-26 15:14:59', 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (21, 4, '2', '1112', 'qqqq', '2023-07-26 15:15:29', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for pro_subject
-- ----------------------------
DROP TABLE IF EXISTS `pro_subject`;
CREATE TABLE `pro_subject`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `index_id` bigint NULL DEFAULT NULL COMMENT 'index',
  `main_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '正文',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型',
  `collect_num` bigint NULL DEFAULT 0 COMMENT '收藏数量',
  `like_num` bigint NULL DEFAULT 0 COMMENT '点赞数量',
  `comment_num` bigint NULL DEFAULT 0 COMMENT '评论数量',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '首页业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pro_subject
-- ----------------------------
INSERT INTO `pro_subject` VALUES (4, '1111', 1679038805860241400, '222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\n### 测试主题\n## 标题\n正文内容\n\n1. 你好\n2. 在吗\n3. 在吗\n4. 在干嘛\n\n\n| hello  |在的  |哈哈  |哈哈  |\n| --- | --- | --- | ---|\n| 2222 |3331  |33  |  11|\n| 112 | 4444 | 12 |222  |\n| 222 | 3332|33  |11  |', 'JAVA', 124, 20, 20, 'admin', '2023-07-20 15:21:10', '0');

-- ----------------------------
-- Table structure for pro_subject_order
-- ----------------------------
DROP TABLE IF EXISTS `pro_subject_order`;
CREATE TABLE `pro_subject_order`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `index_id` bigint NULL DEFAULT NULL COMMENT 'index',
  `main_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '正文',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型',
  `collect_num` bigint NULL DEFAULT 0 COMMENT '收藏数量',
  `like_num` bigint NULL DEFAULT 0 COMMENT '点赞数量',
  `comment_num` bigint NULL DEFAULT 0 COMMENT '评论数量',
  `store` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `unit_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '首页业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pro_subject_order
-- ----------------------------
INSERT INTO `pro_subject_order` VALUES (4, '1111', 1679038805860241400, '222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\n### 测试主题\n## 标题\n正文内容\n\n1. 你好\n2. 在吗\n3. 在吗\n4. 在干嘛\n\n\n| hello  |在的  |哈哈  |哈哈  |\n| --- | --- | --- | ---|\n| 2222 |3331  |33  |  11|\n| 112 | 4444 | 12 |222  |\n| 222 | 3332|33  |11  |', 'JAVA', 124, 20, 20, NULL, NULL, 'admin', '2023-07-20 15:21:10', '0');

-- ----------------------------
-- Table structure for pro_subject_reserve
-- ----------------------------
DROP TABLE IF EXISTS `pro_subject_reserve`;
CREATE TABLE `pro_subject_reserve`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `index_id` bigint NULL DEFAULT NULL COMMENT 'index',
  `main_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '正文',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '类型',
  `collect_num` bigint NULL DEFAULT 0 COMMENT '收藏数量',
  `like_num` bigint NULL DEFAULT 0 COMMENT '点赞数量',
  `comment_num` bigint NULL DEFAULT 0 COMMENT '评论数量',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '首页业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pro_subject_reserve
-- ----------------------------
INSERT INTO `pro_subject_reserve` VALUES (4, '1111', 1679038805860241400, '222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\n### 测试主题\n## 标题\n正文内容\n\n1. 你好\n2. 在吗\n3. 在吗\n4. 在干嘛\n\n\n| hello  |在的  |哈哈  |哈哈  |\n| --- | --- | --- | ---|\n| 2222 |3331  |33  |  11|\n| 112 | 4444 | 12 |222  |\n| 222 | 3332|33  |11  |', 'JAVA', 124, 20, 20, 'admin', '2023-07-20 15:21:10', '0');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `sys_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父id',
  `config_key` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'key',
  `config_value` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'value',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '注释',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`sys_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '部门名称',
  `p_dept_id` bigint NULL DEFAULT NULL COMMENT '上级部门id',
  `leader` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '部门负责人',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人电话',
  `is_sort` int NULL DEFAULT NULL COMMENT '显示顺序',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`dept_id`) USING BTREE,
  UNIQUE INDEX `dept_name`(`dept_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '案例公司', 0, '张三', '17388888888', NULL, NULL, '2023-05-03 16:40:34', '2023-05-03 16:48:02', '1');
INSERT INTO `sys_dept` VALUES (2, '研发中心', 1, '李四', '13333333333', NULL, NULL, '2023-05-03 16:40:48', '2023-05-03 16:48:03', '1');
INSERT INTO `sys_dept` VALUES (3, '研发部', 2, NULL, NULL, NULL, NULL, '2023-05-03 16:40:56', '2023-05-03 16:48:04', '1');
INSERT INTO `sys_dept` VALUES (4, '测试部', 2, NULL, NULL, NULL, NULL, '2023-05-03 16:41:27', '2023-05-03 16:48:04', '1');
INSERT INTO `sys_dept` VALUES (5, '需求基地', 1, '王五', '18999999999', NULL, NULL, '2023-05-03 16:41:44', '2023-05-03 16:48:05', '1');
INSERT INTO `sys_dept` VALUES (6, '需求部', 5, '赵六', '15855552525', NULL, NULL, '2023-05-03 16:41:53', '2023-05-03 16:48:06', '1');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '1', 'sys_normal_disable', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '0', 'sys_normal_disable', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (29, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (30, 0, 'JAVA', 'JAVA', 'subject_type', NULL, 'default', 'N', '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (31, 1, 'python', 'python', 'subject_type', NULL, 'default', 'N', '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '业务类型', 'subject_type', '1', '', NULL, '', NULL, '业务类型列表');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `index_id` bigint NULL DEFAULT NULL COMMENT '索引id',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件地址(url)',
  `file_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件类型(0预览；1图片；2附件；3富文本)',
  `file_suffix` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件后缀名',
  `file_size` double NULL DEFAULT NULL COMMENT '文件大小',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 183 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件(图片)表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES (172, 1679038805860241408, '309844.jpg', '/static/309844.jpg', '0', 'jpg', 78579, 'admin', '2023-07-12 16:03:48', '0');
INSERT INTO `sys_file` VALUES (173, 1679038805860241400, '321686.jpg', '/static/321686.jpg', '0', 'jpg', 44130, 'admin', '2023-07-12 16:04:06', '0');
INSERT INTO `sys_file` VALUES (182, 1679038805860241400, '2059369.jpg', '/static/2059369.jpg', '1', 'jpg', 766008, 'admin', '2023-07-20 15:18:15', '0');

-- ----------------------------
-- Table structure for sys_index
-- ----------------------------
DROP TABLE IF EXISTS `sys_index`;
CREATE TABLE `sys_index`  (
  `id` bigint NOT NULL,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件索引表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_index
-- ----------------------------
INSERT INTO `sys_index` VALUES (1679038805860241400, NULL, '2023-07-19 16:20:20');

-- ----------------------------
-- Table structure for sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE `sys_logs`  (
  `log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `log_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `operation` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作类型',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '请求方法',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '请求参数',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作人',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `time` bigint NOT NULL COMMENT '执行时长',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1686918846640070658 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logs
-- ----------------------------
INSERT INTO `sys_logs` VALUES (1668257096047480833, '新增用户', '1', 'SysUserController.update()', '[{\"age\":22,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":1,\"email\":\"xx_2589@163.com\",\"id\":2,\"isDelete\":\"0\",\"isUse\":\"1\",\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"opera\",\"roleId\":2,\"roleName\":\"操作员\"}],\"sex\":\"0\",\"telephone\":\"17388888887\",\"updateTime\":1683108613000,\"username\":\"222\"}]', 'admin', '127.0.0.1', 72, '2023-06-12 22:01:01');
INSERT INTO `sys_logs` VALUES (1668598193651109890, '修改用户数据', '2', 'SysUserController.update()', '[{\"age\":22,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":1,\"email\":\"xx_2589@163.com\",\"id\":2,\"isDelete\":\"0\",\"isUse\":\"1\",\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"opera\",\"roleId\":2,\"roleName\":\"操作员\"}],\"sex\":\"0\",\"telephone\":\"17388888887\",\"updateTime\":1683108613000,\"username\":\"222\"}]', 'admin', '127.0.0.1', 76, '2023-06-13 20:36:25');
INSERT INTO `sys_logs` VALUES (1677238127500242945, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1677238070315102208,\"mainText\":\"1222\",\"title\":\"测试标题\",\"type\":\"111\"}]', 'admin', '127.0.0.1', 142, '2023-07-07 16:48:25');
INSERT INTO `sys_logs` VALUES (1677239177053483009, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1677239154379075584,\"mainText\":\"222\",\"title\":\"111\",\"type\":\"111\"}]', 'admin', '127.0.0.1', 135, '2023-07-07 16:52:36');
INSERT INTO `sys_logs` VALUES (1677240780020019202, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1677240753772064768,\"mainText\":\"22\",\"title\":\"1222\",\"type\":\"122\"}]', 'admin', '127.0.0.1', 191, '2023-07-07 16:58:58');
INSERT INTO `sys_logs` VALUES (1677241323606011905, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1677241301200039936,\"mainText\":\"111\",\"title\":\"11\",\"type\":\"22\"}]', 'admin', '127.0.0.1', 11, '2023-07-07 17:01:07');
INSERT INTO `sys_logs` VALUES (1678297613379862529, '首页业务', '3', 'ProSubjectController.remove()', '[[2]]', 'admin', '127.0.0.1', 5928, '2023-07-10 14:58:26');
INSERT INTO `sys_logs` VALUES (1678297630907858946, '首页业务', '3', 'ProSubjectController.remove()', '[[2]]', 'admin', '127.0.0.1', 6, '2023-07-10 14:58:31');
INSERT INTO `sys_logs` VALUES (1678297642626744321, '首页业务', '3', 'ProSubjectController.remove()', '[[1]]', 'admin', '127.0.0.1', 13, '2023-07-10 14:58:33');
INSERT INTO `sys_logs` VALUES (1678299838713987074, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1678299714822635520,\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\"}]', 'admin', '127.0.0.1', 54, '2023-07-10 15:07:17');
INSERT INTO `sys_logs` VALUES (1678305618536202241, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1688972827000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":141,\"indexId\":\"1678299714822635520\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 23, '2023-07-10 15:30:15');
INSERT INTO `sys_logs` VALUES (1678593880572092418, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileList\":[],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 162, '2023-07-11 10:35:42');
INSERT INTO `sys_logs` VALUES (1678593956140867585, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689042939000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":155,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 10, '2023-07-11 10:36:00');
INSERT INTO `sys_logs` VALUES (1678605535913754625, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileList\":[],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 128, '2023-07-11 11:22:01');
INSERT INTO `sys_logs` VALUES (1678605792462553089, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileList\":[],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 37487, '2023-07-11 11:23:02');
INSERT INTO `sys_logs` VALUES (1678606720972406785, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileList\":[],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 8, '2023-07-11 11:26:43');
INSERT INTO `sys_logs` VALUES (1678609387929030658, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[155],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689042939000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":155,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 137, '2023-07-11 11:37:19');
INSERT INTO `sys_logs` VALUES (1678653293085413378, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689054328000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":161,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054331000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":162,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054334000,\"fileName\":\"324051.jpg\",\"filePath\":\"/static/324051.jpg\",\"fileSize\":1379256,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":163,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 5, '2023-07-11 14:31:47');
INSERT INTO `sys_logs` VALUES (1678653548560470017, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689054328000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":161,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054331000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":162,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054334000,\"fileName\":\"324051.jpg\",\"filePath\":\"/static/324051.jpg\",\"fileSize\":1379256,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":163,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"JAVA全球第一\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 6, '2023-07-11 14:32:48');
INSERT INTO `sys_logs` VALUES (1678654717999534082, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689054328000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":161,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054331000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":162,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054334000,\"fileName\":\"324051.jpg\",\"filePath\":\"/static/324051.jpg\",\"fileSize\":1379256,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":163,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"# 测试标题\\n## 二级标题\\n### 三级标题\\n正文内容\\n1. 排序1\\n2. 牌序2\\n3. 牌序3\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 8, '2023-07-11 14:37:27');
INSERT INTO `sys_logs` VALUES (1678668618711519234, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[162,163,161],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689054328000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":161,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054331000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":162,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689054334000,\"fileName\":\"324051.jpg\",\"filePath\":\"/static/324051.jpg\",\"fileSize\":1379256,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":163,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"# 测试标题\\n## 二级标题\\n### 三级标题\\n正文内容\\n1. 排序1\\n2. 牌序2\\n3. 牌序3\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 70, '2023-07-11 15:32:41');
INSERT INTO `sys_logs` VALUES (1678668661216595969, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"# 测试标题\\n## 二级标题\\n### 三级标题\\n正文内容\\n1. 排序1\\n2. 牌序2\\n3. 牌序3\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 5, '2023-07-11 15:32:51');
INSERT INTO `sys_logs` VALUES (1678668740786737153, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689060770000,\"fileName\":\"2052084.jpg\",\"filePath\":\"/static/2052084.jpg\",\"fileSize\":864821,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":170,\"indexId\":\"1678299714822635500\",\"isDelete\":\"0\"}],\"id\":3,\"indexId\":1678299714822635500,\"isDelete\":\"0\",\"mainText\":\"# 测试标题\\n## 二级标题\\n### 三级标题\\n正文内容\\n1. 排序1\\n2. 牌序2\\n3. 牌序3\",\"title\":\"JAVA最好用\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 5, '2023-07-11 15:33:10');
INSERT INTO `sys_logs` VALUES (1679038846624681985, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1679038805860241408,\"mainText\":\"222\",\"title\":\"1111\",\"type\":\"JAVA\"}]', 'admin', '127.0.0.1', 49, '2023-07-12 16:03:50');
INSERT INTO `sys_logs` VALUES (1679038916396929026, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149028000,\"fileName\":\"309844.jpg\",\"filePath\":\"/static/309844.jpg\",\"fileSize\":78579,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":172,\"indexId\":\"1679038805860241408\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 29, '2023-07-12 16:04:07');
INSERT INTO `sys_logs` VALUES (1679041426243276801, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 9, '2023-07-12 16:14:05');
INSERT INTO `sys_logs` VALUES (1681223857998204929, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[176],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689149627000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":174,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689669373000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":175,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689669645000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":176,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 376, '2023-07-18 16:46:18');
INSERT INTO `sys_logs` VALUES (1681223889635840002, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[175,174],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689149627000,\"fileName\":\"314956.jpg\",\"filePath\":\"/static/314956.jpg\",\"fileSize\":441347,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":174,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689669373000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":175,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 17, '2023-07-18 16:46:25');
INSERT INTO `sys_logs` VALUES (1681551262004613122, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[179,180,178,177],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689670809000,\"fileName\":\"324051.jpg\",\"filePath\":\"/static/324051.jpg\",\"fileSize\":1379256,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":177,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689670828000,\"fileName\":\"2052968.jpg\",\"filePath\":\"/static/2052968.jpg\",\"fileSize\":723773,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":178,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689670883000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"3\",\"id\":179,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689671051000,\"fileName\":\"324051.jpg\",\"filePath\":\"/static/324051.jpg\",\"fileSize\":1379256,\"fileSuffix\":\"jpg\",\"fileType\":\"3\",\"id\":180,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 51, '2023-07-19 14:27:17');
INSERT INTO `sys_logs` VALUES (1681551401079345153, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 20, '2023-07-19 14:27:50');
INSERT INTO `sys_logs` VALUES (1681551681028165634, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[181],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689748068000,\"fileName\":\"360wallpaper.jpg\",\"filePath\":\"/static/360wallpaper.jpg\",\"fileSize\":808052,\"fileSuffix\":\"jpg\",\"fileType\":\"3\",\"id\":181,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"isDelete\":\"0\",\"mainText\":\"222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 18, '2023-07-19 14:28:57');
INSERT INTO `sys_logs` VALUES (1681926482129383426, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":122,\"commentNum\":14,\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"likeNum\":11,\"mainText\":\"222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 155, '2023-07-20 15:18:16');
INSERT INTO `sys_logs` VALUES (1684088757678260225, '修改用户数据', '2', 'SysUserController.update()', '[{\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":2,\"email\":\"xx_23@1.com\",\"id\":1,\"isUse\":\"1\",\"loginTime\":1690349457000,\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"system\",\"roleId\":1,\"roleName\":\"管理员\"}],\"sex\":\"1\",\"telephone\":\"17388888888\",\"updateTime\":1686661687000,\"username\":\"admin\"}]', 'admin', '127.0.0.1', 271, '2023-07-26 14:30:23');
INSERT INTO `sys_logs` VALUES (1684088779983568897, '修改用户数据', '2', 'SysUserController.update()', '[{\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":2,\"email\":\"xx_23@1.com\",\"id\":1,\"isUse\":\"1\",\"loginTime\":1690349457000,\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"system\",\"roleId\":1,\"roleName\":\"管理员\"}],\"sex\":\"1\",\"telephone\":\"17388888188\",\"updateTime\":1686661687000,\"username\":\"admin\"}]', 'admin', '127.0.0.1', 43, '2023-07-26 14:30:28');
INSERT INTO `sys_logs` VALUES (1684368667164573698, '评论状态', '2', 'ProInformController.readMessage()', '[16]', 'admin', '127.0.0.1', 118, '2023-07-27 09:02:38');
INSERT INTO `sys_logs` VALUES (1684377656631787522, '回复评论', '2', 'ProRecordController.replyComment()', '[{\"commentReply\":\"回复评论\",\"id\":7,\"subjectId\":4}]', 'admin', '127.0.0.1', 91, '2023-07-27 09:38:22');
INSERT INTO `sys_logs` VALUES (1684378577176657922, '修改用户数据', '2', 'SysUserController.update()', '[{\"avatar\":\"/static/avatar.gif\",\"createTime\":1690355086000,\"id\":7,\"isUse\":\"1\",\"loginTime\":1690356516000,\"name\":\"测试用户\",\"roleList\":[],\"telephone\":\"18566656236\",\"updateTime\":1690355086000,\"username\":\"qqqq\"}]', 'admin', '127.0.0.1', 38, '2023-07-27 09:42:01');
INSERT INTO `sys_logs` VALUES (1684428908480733185, '修改用户数据', '2', 'SysUserController.update()', '[{\"age\":23,\"email\":\"xx_23@Q.com\",\"id\":1,\"name\":\"张三\",\"sex\":\"1\",\"telephone\":\"17388888188\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 419, '2023-07-27 13:02:01');
INSERT INTO `sys_logs` VALUES (1684448848797540354, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":124,\"commentNum\":20,\"createTime\":1689837670000,\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689149046000,\"fileName\":\"321686.jpg\",\"filePath\":\"/static/321686.jpg\",\"fileSize\":44130,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":173,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1689837495000,\"fileName\":\"2059369.jpg\",\"filePath\":\"/static/2059369.jpg\",\"fileSize\":766008,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":182,\"indexId\":\"1679038805860241400\",\"isDelete\":\"0\"}],\"id\":4,\"indexId\":1679038805860241400,\"likeNum\":20,\"mainText\":\"222![](http://127.0.0.1:8081/admin/static/360wallpaper.jpg)\\n### 测试主题\\n## 标题\\n正文内容\\n\\n1. 你好\\n2. 在吗\\n3. 在吗\\n4. 在干嘛\\n\\n\\n| hello  |在的  |哈哈  |哈哈  |\\n| --- | --- | --- | ---|\\n| 2222 |3331  |33  |  11|\\n| 112 | 4444 | 12 |222  |\\n| 222 | 3332|33  |11  |\",\"title\":\"1111\",\"type\":\"JAVA\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 115, '2023-07-27 14:21:15');
INSERT INTO `sys_logs` VALUES (1686918846640070657, '收货地址', '1', 'UniAddressController.add()', '[{\"address\":\"上海市浦东新区\",\"name\":\"zhangsan\",\"telephone\":\"15588885858\"}]', 'admin', '127.0.0.1', 34, '2023-08-03 09:56:09');

-- ----------------------------
-- Table structure for sys_logs_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs_login`;
CREATE TABLE `sys_logs_login`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `ip_addr` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `login_mac` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录设备',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1686979777432657922 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logs_login
-- ----------------------------
INSERT INTO `sys_logs_login` VALUES (1668606173377449985, 'admin', '127.0.0.1', '2023-06-13 21:08:08', 'Chrome', '2023-06-13 21:08:07');
INSERT INTO `sys_logs_login` VALUES (1668617709655293953, 'admin', '127.0.0.1', '2023-06-13 21:53:58', 'Chrome', '2023-06-13 21:53:58');
INSERT INTO `sys_logs_login` VALUES (1668617980569595905, 'admin', '127.0.0.1', '2023-06-13 21:55:03', 'Chrome', '2023-06-13 21:55:02');
INSERT INTO `sys_logs_login` VALUES (1675846024193323009, 'admin', '127.0.0.1', '2023-07-03 20:36:43', 'Chrome', '2023-07-03 20:36:42');
INSERT INTO `sys_logs_login` VALUES (1676190058011140097, 'admin', '127.0.0.1', '2023-07-04 19:23:47', 'Chrome', '2023-07-04 19:23:46');
INSERT INTO `sys_logs_login` VALUES (1676553936062611458, 'admin', '127.0.0.1', '2023-07-05 19:29:42', 'Chrome', '2023-07-05 19:29:41');
INSERT INTO `sys_logs_login` VALUES (1676940421148311554, 'admin', '127.0.0.1', '2023-07-06 21:05:27', 'Chrome', '2023-07-06 21:05:27');
INSERT INTO `sys_logs_login` VALUES (1676941975045730305, 'admin', '127.0.0.1', '2023-07-06 21:11:38', 'Chrome', '2023-07-06 21:11:37');
INSERT INTO `sys_logs_login` VALUES (1676942914901192706, 'admin', '127.0.0.1', '2023-07-06 21:15:22', 'Chrome', '2023-07-06 21:15:21');
INSERT INTO `sys_logs_login` VALUES (1676943154790215681, 'admin', '127.0.0.1', '2023-07-06 21:16:19', 'Chrome', '2023-07-06 21:16:18');
INSERT INTO `sys_logs_login` VALUES (1676943789522636801, 'admin', '127.0.0.1', '2023-07-06 21:18:50', 'Chrome', '2023-07-06 21:18:50');
INSERT INTO `sys_logs_login` VALUES (1676951151826391041, 'admin', '127.0.0.1', '2023-07-06 21:48:05', 'Chrome', '2023-07-06 21:48:05');
INSERT INTO `sys_logs_login` VALUES (1676951157409009665, 'admin', '127.0.0.1', '2023-07-06 21:48:07', 'Chrome', '2023-07-06 21:48:06');
INSERT INTO `sys_logs_login` VALUES (1676951198777430018, 'admin', '127.0.0.1', '2023-07-06 21:48:17', 'Chrome', '2023-07-06 21:48:16');
INSERT INTO `sys_logs_login` VALUES (1676951623375134721, 'admin', '127.0.0.1', '2023-07-06 21:49:58', 'Chrome', '2023-07-06 21:49:57');
INSERT INTO `sys_logs_login` VALUES (1677234876981784577, 'admin', '127.0.0.1', '2023-07-07 16:35:31', 'Chrome', '2023-07-07 16:35:30');
INSERT INTO `sys_logs_login` VALUES (1677238052569001985, 'admin', '127.0.0.1', '2023-07-07 16:48:08', 'Chrome', '2023-07-07 16:48:07');
INSERT INTO `sys_logs_login` VALUES (1677239135815086081, 'admin', '127.0.0.1', '2023-07-07 16:52:26', 'Chrome', '2023-07-07 16:52:26');
INSERT INTO `sys_logs_login` VALUES (1677240729805811713, 'admin', '127.0.0.1', '2023-07-07 16:58:46', 'Chrome', '2023-07-07 16:58:46');
INSERT INTO `sys_logs_login` VALUES (1678284740242972674, 'admin', '127.0.0.1', '2023-07-10 14:07:18', 'Chrome', '2023-07-10 14:07:17');
INSERT INTO `sys_logs_login` VALUES (1678296572743647234, 'admin', '127.0.0.1', '2023-07-10 14:54:19', 'Chrome', '2023-07-10 14:54:18');
INSERT INTO `sys_logs_login` VALUES (1678297565912924161, 'admin', '127.0.0.1', '2023-07-10 14:58:16', 'Chrome', '2023-07-10 14:58:15');
INSERT INTO `sys_logs_login` VALUES (1678315794668494849, 'admin', '127.0.0.1', '2023-07-10 16:10:42', 'Chrome', '2023-07-10 16:10:41');
INSERT INTO `sys_logs_login` VALUES (1678317200158801921, 'admin', '127.0.0.1', '2023-07-10 16:16:17', 'Chrome', '2023-07-10 16:16:16');
INSERT INTO `sys_logs_login` VALUES (1678318641334222849, 'admin', '127.0.0.1', '2023-07-10 16:22:00', 'Chrome', '2023-07-10 16:22:00');
INSERT INTO `sys_logs_login` VALUES (1678321779055284225, 'admin', '127.0.0.1', '2023-07-10 16:34:28', 'Chrome', '2023-07-10 16:34:28');
INSERT INTO `sys_logs_login` VALUES (1678334173911457794, 'admin', '127.0.0.1', '2023-07-10 17:23:44', 'Chrome', '2023-07-10 17:23:43');
INSERT INTO `sys_logs_login` VALUES (1678335593129742338, 'admin', '127.0.0.1', '2023-07-10 17:29:22', 'Chrome', '2023-07-10 17:29:22');
INSERT INTO `sys_logs_login` VALUES (1678335914677645313, 'admin', '127.0.0.1', '2023-07-10 17:30:39', 'Chrome', '2023-07-10 17:30:38');
INSERT INTO `sys_logs_login` VALUES (1678593606524657665, 'admin', '127.0.0.1', '2023-07-11 10:34:37', 'Chrome', '2023-07-11 10:34:37');
INSERT INTO `sys_logs_login` VALUES (1678605509783240705, 'admin', '127.0.0.1', '2023-07-11 11:21:55', 'Chrome', '2023-07-11 11:21:55');
INSERT INTO `sys_logs_login` VALUES (1678609347110064129, 'admin', '127.0.0.1', '2023-07-11 11:37:10', 'Chrome', '2023-07-11 11:37:10');
INSERT INTO `sys_logs_login` VALUES (1678652565650493441, 'admin', '127.0.0.1', '2023-07-11 14:28:54', 'Chrome', '2023-07-11 14:28:54');
INSERT INTO `sys_logs_login` VALUES (1678932996287451138, 'admin', '127.0.0.1', '2023-07-12 09:03:14', 'Chrome', '2023-07-12 09:03:14');
INSERT INTO `sys_logs_login` VALUES (1678993073950248962, 'admin', '127.0.0.1', '2023-07-12 13:01:58', 'Chrome', '2023-07-12 13:01:57');
INSERT INTO `sys_logs_login` VALUES (1679038782560882690, 'admin', '127.0.0.1', '2023-07-12 16:03:35', 'Chrome', '2023-07-12 16:03:35');
INSERT INTO `sys_logs_login` VALUES (1681187847654412290, 'admin', '127.0.0.1', '2023-07-18 14:23:12', 'Chrome', '2023-07-18 14:23:12');
INSERT INTO `sys_logs_login` VALUES (1681537602213449729, 'admin', '127.0.0.1', '2023-07-19 13:33:00', 'Chrome', '2023-07-19 13:33:00');
INSERT INTO `sys_logs_login` VALUES (1681564142749888514, 'admin', '127.0.0.1', '2023-07-19 15:18:28', 'Chrome', '2023-07-19 15:18:28');
INSERT INTO `sys_logs_login` VALUES (1681843040104566786, 'admin', '127.0.0.1', '2023-07-20 09:46:42', 'Chrome', '2023-07-20 09:46:42');
INSERT INTO `sys_logs_login` VALUES (1681844248026320897, 'admin', '127.0.0.1', '2023-07-20 09:51:30', 'Chrome', '2023-07-20 09:51:30');
INSERT INTO `sys_logs_login` VALUES (1681926409106550786, 'admin', '127.0.0.1', '2023-07-20 15:17:59', 'Chrome', '2023-07-20 15:17:59');
INSERT INTO `sys_logs_login` VALUES (1683311348448313345, 'admin', '127.0.0.1', '2023-07-24 11:01:14', 'MicroMessenger', '2023-07-24 11:01:14');
INSERT INTO `sys_logs_login` VALUES (1683311502614151169, 'admin', '127.0.0.1', '2023-07-24 11:01:51', 'MicroMessenger', '2023-07-24 11:01:51');
INSERT INTO `sys_logs_login` VALUES (1683374593141981186, 'admin', '127.0.0.1', '2023-07-24 15:12:33', 'MicroMessenger', '2023-07-24 15:12:33');
INSERT INTO `sys_logs_login` VALUES (1683377069853286401, 'admin', '127.0.0.1', '2023-07-24 15:22:24', 'MicroMessenger', '2023-07-24 15:22:23');
INSERT INTO `sys_logs_login` VALUES (1683646899453956098, 'admin', '127.0.0.1', '2023-07-25 09:14:36', 'MicroMessenger', '2023-07-25 09:14:36');
INSERT INTO `sys_logs_login` VALUES (1683652375403061249, 'admin', '127.0.0.1', '2023-07-25 09:36:22', 'MicroMessenger', '2023-07-25 09:36:21');
INSERT INTO `sys_logs_login` VALUES (1683655006724841474, 'admin', '127.0.0.1', '2023-07-25 09:46:49', 'MicroMessenger', '2023-07-25 09:46:49');
INSERT INTO `sys_logs_login` VALUES (1683693383310409730, 'admin', '127.0.0.1', '2023-07-25 12:19:19', 'MicroMessenger', '2023-07-25 12:19:18');
INSERT INTO `sys_logs_login` VALUES (1683742155268718594, 'admin', '127.0.0.1', '2023-07-25 15:33:07', 'MicroMessenger', '2023-07-25 15:33:06');
INSERT INTO `sys_logs_login` VALUES (1683743708893458434, 'admin', '127.0.0.1', '2023-07-25 15:39:17', 'MicroMessenger', '2023-07-25 15:39:17');
INSERT INTO `sys_logs_login` VALUES (1683744796782673922, 'admin', '127.0.0.1', '2023-07-25 15:43:37', 'MicroMessenger', '2023-07-25 15:43:36');
INSERT INTO `sys_logs_login` VALUES (1683746195276857346, 'admin', '127.0.0.1', '2023-07-25 15:49:10', 'MicroMessenger', '2023-07-25 15:49:10');
INSERT INTO `sys_logs_login` VALUES (1683746634927976449, 'admin', '127.0.0.1', '2023-07-25 15:50:55', 'MicroMessenger', '2023-07-25 15:50:54');
INSERT INTO `sys_logs_login` VALUES (1683760308098957313, 'admin', '127.0.0.1', '2023-07-25 16:45:15', 'MicroMessenger', '2023-07-25 16:45:14');
INSERT INTO `sys_logs_login` VALUES (1683763804881842177, 'admin', '127.0.0.1', '2023-07-25 16:59:08', 'MicroMessenger', '2023-07-25 16:59:08');
INSERT INTO `sys_logs_login` VALUES (1683764157782208513, 'admin', '127.0.0.1', '2023-07-25 17:00:33', 'MicroMessenger', '2023-07-25 17:00:32');
INSERT INTO `sys_logs_login` VALUES (1683764487546802177, 'admin', '127.0.0.1', '2023-07-25 17:01:51', 'MicroMessenger', '2023-07-25 17:01:51');
INSERT INTO `sys_logs_login` VALUES (1683773808586317825, 'admin', '127.0.0.1', '2023-07-25 17:38:54', 'MicroMessenger', '2023-07-25 17:38:53');
INSERT INTO `sys_logs_login` VALUES (1683781201588166657, 'admin', '127.0.0.1', '2023-07-25 18:08:16', 'MicroMessenger', '2023-07-25 18:08:16');
INSERT INTO `sys_logs_login` VALUES (1683794057083813890, 'admin', '127.0.0.1', '2023-07-25 18:59:21', 'MicroMessenger', '2023-07-25 18:59:21');
INSERT INTO `sys_logs_login` VALUES (1683796277426720769, 'admin', '127.0.0.1', '2023-07-25 19:08:10', 'MicroMessenger', '2023-07-25 19:08:10');
INSERT INTO `sys_logs_login` VALUES (1684007693672828929, 'admin', '127.0.0.1', '2023-07-26 09:08:16', 'MicroMessenger', '2023-07-26 09:08:16');
INSERT INTO `sys_logs_login` VALUES (1684023625476521985, 'admin', '127.0.0.1', '2023-07-26 10:11:35', 'MicroMessenger', '2023-07-26 10:11:34');
INSERT INTO `sys_logs_login` VALUES (1684037819017994242, 'admin', '127.0.0.1', '2023-07-26 11:07:59', 'MicroMessenger', '2023-07-26 11:07:58');
INSERT INTO `sys_logs_login` VALUES (1684045983146901505, 'admin', '127.0.0.1', '2023-07-26 11:40:25', 'MicroMessenger', '2023-07-26 11:40:25');
INSERT INTO `sys_logs_login` VALUES (1684067969491353601, 'admin', '127.0.0.1', '2023-07-26 13:07:47', 'MicroMessenger', '2023-07-26 13:07:47');
INSERT INTO `sys_logs_login` VALUES (1684072691543621633, 'admin', '127.0.0.1', '2023-07-26 13:26:33', 'MicroMessenger', '2023-07-26 13:26:32');
INSERT INTO `sys_logs_login` VALUES (1684073798349111297, 'admin', '127.0.0.1', '2023-07-26 13:30:57', 'MicroMessenger', '2023-07-26 13:30:56');
INSERT INTO `sys_logs_login` VALUES (1684097866364440578, 'qqqq', '127.0.0.1', '2023-07-26 15:06:35', 'MicroMessenger', '2023-07-26 15:06:35');
INSERT INTO `sys_logs_login` VALUES (1684099953093627905, 'qqqq', '127.0.0.1', '2023-07-26 15:14:52', 'MicroMessenger', '2023-07-26 15:14:52');
INSERT INTO `sys_logs_login` VALUES (1684103408524840962, 'qqqq', '127.0.0.1', '2023-07-26 15:28:36', 'MicroMessenger', '2023-07-26 15:28:36');
INSERT INTO `sys_logs_login` VALUES (1684111590458511362, 'admin', '127.0.0.1', '2023-07-26 16:01:07', 'MicroMessenger', '2023-07-26 16:01:07');
INSERT INTO `sys_logs_login` VALUES (1684118039070887938, 'admin', '127.0.0.1', '2023-07-26 16:26:45', 'MicroMessenger', '2023-07-26 16:26:44');
INSERT INTO `sys_logs_login` VALUES (1684368034235711490, 'admin', '127.0.0.1', '2023-07-27 09:00:08', 'MicroMessenger', '2023-07-27 09:00:08');
INSERT INTO `sys_logs_login` VALUES (1684370635840847873, 'admin', '127.0.0.1', '2023-07-27 09:10:28', 'MicroMessenger', '2023-07-27 09:10:28');
INSERT INTO `sys_logs_login` VALUES (1684372533696299009, 'admin', '127.0.0.1', '2023-07-27 09:18:01', 'Chrome', '2023-07-27 09:18:00');
INSERT INTO `sys_logs_login` VALUES (1684410312983617538, 'admin', '127.0.0.1', '2023-07-27 11:48:08', 'Chrome', '2023-07-27 11:48:08');
INSERT INTO `sys_logs_login` VALUES (1684459982023860225, 'admin', '127.0.0.1', '2023-07-27 15:05:30', 'Chrome', '2023-07-27 15:05:30');
INSERT INTO `sys_logs_login` VALUES (1684478354367225857, 'admin', '127.0.0.1', '2023-07-27 16:18:30', 'MicroMessenger', '2023-07-27 16:18:30');
INSERT INTO `sys_logs_login` VALUES (1684735332813406209, 'admin', '127.0.0.1', '2023-07-28 09:19:39', 'Chrome', '2023-07-28 09:19:38');
INSERT INTO `sys_logs_login` VALUES (1684745256951906305, 'admin', '127.0.0.1', '2023-07-28 09:59:05', 'Chrome', '2023-07-28 09:59:04');
INSERT INTO `sys_logs_login` VALUES (1684761834569367554, 'admin', '127.0.0.1', '2023-07-28 11:04:57', 'Chrome', '2023-07-28 11:04:57');
INSERT INTO `sys_logs_login` VALUES (1684765287165227009, 'admin', '127.0.0.1', '2023-07-28 11:18:40', 'Chrome', '2023-07-28 11:18:40');
INSERT INTO `sys_logs_login` VALUES (1684805473651683329, 'admin', '127.0.0.1', '2023-07-28 13:58:22', 'Chrome', '2023-07-28 13:58:21');
INSERT INTO `sys_logs_login` VALUES (1684806179624374273, 'admin', '127.0.0.1', '2023-07-28 14:01:10', 'MicroMessenger', '2023-07-28 14:01:10');
INSERT INTO `sys_logs_login` VALUES (1684807605373161473, 'admin', '127.0.0.1', '2023-07-28 14:06:50', 'MicroMessenger', '2023-07-28 14:06:49');
INSERT INTO `sys_logs_login` VALUES (1684809234625433601, 'admin', '127.0.0.1', '2023-07-28 14:13:18', 'MicroMessenger', '2023-07-28 14:13:18');
INSERT INTO `sys_logs_login` VALUES (1684809837518860290, 'admin', '127.0.0.1', '2023-07-28 14:15:42', 'MicroMessenger', '2023-07-28 14:15:42');
INSERT INTO `sys_logs_login` VALUES (1684814898374377474, 'admin', '127.0.0.1', '2023-07-28 14:35:49', 'MicroMessenger', '2023-07-28 14:35:48');
INSERT INTO `sys_logs_login` VALUES (1684816886055145474, 'admin', '127.0.0.1', '2023-07-28 14:43:43', 'MicroMessenger', '2023-07-28 14:43:42');
INSERT INTO `sys_logs_login` VALUES (1684818024070414337, 'admin', '127.0.0.1', '2023-07-28 14:48:14', 'MicroMessenger', '2023-07-28 14:48:14');
INSERT INTO `sys_logs_login` VALUES (1684847685609488385, 'admin', '127.0.0.1', '2023-07-28 16:46:06', 'MicroMessenger', '2023-07-28 16:46:05');
INSERT INTO `sys_logs_login` VALUES (1685893275298643969, 'admin', '127.0.0.1', '2023-07-31 14:00:54', 'MicroMessenger', '2023-07-31 14:00:53');
INSERT INTO `sys_logs_login` VALUES (1685904040902606849, 'admin', '127.0.0.1', '2023-07-31 14:43:41', 'MicroMessenger', '2023-07-31 14:43:40');
INSERT INTO `sys_logs_login` VALUES (1686178886295269377, 'admin', '127.0.0.1', '2023-08-01 08:55:49', 'MicroMessenger', '2023-08-01 08:55:48');
INSERT INTO `sys_logs_login` VALUES (1686201000331096066, 'admin', '127.0.0.1', '2023-08-01 10:23:41', 'MicroMessenger', '2023-08-01 10:23:41');
INSERT INTO `sys_logs_login` VALUES (1686212215006605313, 'admin', '127.0.0.1', '2023-08-01 11:08:15', 'Chrome', '2023-08-01 11:08:15');
INSERT INTO `sys_logs_login` VALUES (1686300348582768642, 'admin', '127.0.0.1', '2023-08-01 16:58:28', 'Chrome', '2023-08-01 16:58:27');
INSERT INTO `sys_logs_login` VALUES (1686579285066027010, 'admin', '127.0.0.1', '2023-08-02 11:26:51', 'MicroMessenger', '2023-08-02 11:26:51');
INSERT INTO `sys_logs_login` VALUES (1686606175004823554, 'admin', '127.0.0.1', '2023-08-02 13:13:42', 'MicroMessenger', '2023-08-02 13:13:42');
INSERT INTO `sys_logs_login` VALUES (1686914501215297537, 'admin', '127.0.0.1', '2023-08-03 09:38:53', 'MicroMessenger', '2023-08-03 09:38:53');
INSERT INTO `sys_logs_login` VALUES (1686969475542589442, 'admin', '127.0.0.1', '2023-08-03 13:17:20', 'MicroMessenger', '2023-08-03 13:17:20');
INSERT INTO `sys_logs_login` VALUES (1686970362402979841, 'admin', '127.0.0.1', '2023-08-03 13:20:51', 'MicroMessenger', '2023-08-03 13:20:51');
INSERT INTO `sys_logs_login` VALUES (1686976936584462338, 'admin', '127.0.0.1', '2023-08-03 13:46:59', 'MicroMessenger', '2023-08-03 13:46:58');
INSERT INTO `sys_logs_login` VALUES (1686978634522632194, 'admin', '127.0.0.1', '2023-08-03 13:53:44', 'MicroMessenger', '2023-08-03 13:53:43');
INSERT INTO `sys_logs_login` VALUES (1686979777432657921, 'admin', '127.0.0.1', '2023-08-03 13:58:16', 'MicroMessenger', '2023-08-03 13:58:16');
INSERT INTO `sys_logs_login` VALUES (1687011007456878594, 'admin', '127.0.0.1', '2023-08-03 16:02:22', 'Chrome', '2023-08-03 16:02:21');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `p_menu_id` bigint NULL DEFAULT NULL COMMENT '父权限id',
  `menu_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限名称',
  `url` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限路径',
  `menu_auth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限标识',
  `is_hidden` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '是否隐藏:0否；1是',
  `menu_type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单类型: 0目录 1菜单 2按钮',
  `show_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '展示类型: 第一栏,第二栏,第三栏',
  `menu_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `is_sort` int NULL DEFAULT NULL COMMENT '排序等级',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '/system', 'sys', '0', '0', '0', 'el-icon-s-home', 1, NULL, '2023-05-01 14:47:21', '2023-05-05 23:15:26', '0');
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', '/system/user', 'sys:user', '0', '1', '0', 'el-icon-user', 1, NULL, '2023-05-01 14:47:56', '2023-05-05 23:14:41', '0');
INSERT INTO `sys_menu` VALUES (3, 2, '新增', NULL, 'sys:user:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:48:27', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (4, 2, '修改', NULL, 'sys:user:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:48:43', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (5, 2, '删除', NULL, 'sys:user:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:00', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (6, 2, '导入', NULL, 'sys:user:import', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:38', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (7, 2, '导出', NULL, 'sys:user:export', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:51', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (8, 1, '角色管理', '/system/role', 'sys:role', '0', '1', '0', 'el-icon-bell', 2, NULL, '2023-05-01 14:50:21', '2023-05-05 23:15:40', '0');
INSERT INTO `sys_menu` VALUES (9, 8, '新增', NULL, 'sys:role:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:50:39', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (10, 8, '修改', NULL, 'sys:role:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:51:03', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (11, 8, '删除', NULL, 'sys:role:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:51:53', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (12, 1, '菜单管理', '/system/menu', 'sys:menu', '0', '1', '0', 'el-icon-s-data', 3, NULL, '2023-05-01 14:52:20', '2023-05-05 23:15:56', '0');
INSERT INTO `sys_menu` VALUES (13, 12, '新增', NULL, 'sys:menu:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:52:34', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (14, 12, '修改', NULL, 'sys:menu:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:52:53', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (15, 12, '删除', NULL, 'sys:menu:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:53:08', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (16, 1, '部门管理', '/system/dept', 'sys:dept', '0', '1', '0', 'el-icon-money', 4, NULL, '2023-05-07 14:46:15', '2023-05-07 14:46:15', '0');
INSERT INTO `sys_menu` VALUES (17, 16, '新增', NULL, 'sys:dept:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:19:36', '2023-05-08 22:24:35', '0');
INSERT INTO `sys_menu` VALUES (18, 16, '修改', NULL, 'sys:dept:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:20:00', '2023-05-08 22:24:33', '0');
INSERT INTO `sys_menu` VALUES (19, 16, '删除', NULL, 'sys:dept:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:20:21', '2023-07-28 11:18:50', '1');
INSERT INTO `sys_menu` VALUES (20, 2, '查看', NULL, 'sys:user:select', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:24:30', '2023-05-08 22:24:30', '0');
INSERT INTO `sys_menu` VALUES (21, 0, '日志管理', '/logs', 'sys:log', '0', '0', '0', 'el-icon-question', 2, NULL, '2023-06-02 22:23:22', '2023-06-02 22:23:22', '0');
INSERT INTO `sys_menu` VALUES (22, 21, '操作日志', '/logs/operate', 'logs:operate', '0', '1', '0', 'el-icon-info', 1, NULL, '2023-06-02 22:24:26', '2023-06-02 22:24:26', '0');
INSERT INTO `sys_menu` VALUES (23, 21, '登录日志', '/logs/login', 'logs:login', '0', '1', '0', 'el-icon-circle-plus', 2, NULL, '2023-06-02 22:37:18', '2023-06-02 22:37:18', '0');
INSERT INTO `sys_menu` VALUES (24, 0, '系统工具', '/tools', 'sys:tools', '0', '0', '1', 'el-icon-s-tools', 3, NULL, '2023-06-03 18:46:01', '2023-06-03 18:46:01', '0');
INSERT INTO `sys_menu` VALUES (25, 24, '代码生成', '/tools/gen', 'tools:gen', '0', '1', '1', 'el-icon-setting', 1, NULL, '2023-06-03 18:46:42', '2023-06-03 18:46:42', '0');
INSERT INTO `sys_menu` VALUES (26, 24, '修改配置', '/tools/gen/editTable', 'tools:table', '1', '1', '1', 'el-icon-video-camera-solid', 2, NULL, '2023-06-04 13:59:50', '2023-06-04 13:59:50', '0');
INSERT INTO `sys_menu` VALUES (27, 24, '字典管理', '/tools/dict', 'tools/dict', '0', '1', '1', 'el-icon-s-help', 3, NULL, '2023-06-06 20:40:29', '2023-06-06 20:40:29', '0');
INSERT INTO `sys_menu` VALUES (28, 24, '字典配置', '/tools/dict/data', 'dict:data', '1', '1', '1', 'el-icon-picture', 4, NULL, '2023-06-10 19:25:05', '2023-06-10 19:25:05', '0');
INSERT INTO `sys_menu` VALUES (30, 0, '定制业务', '/subject', 'sub:index', '0', '0', '1', 'el-icon-s-shop', 4, NULL, '2023-07-06 21:42:31', '2023-07-06 21:42:31', '0');
INSERT INTO `sys_menu` VALUES (31, 30, '业务模板', '/subject/subject', 'subject:subject', '0', '1', '1', 'el-icon-s-goods', 1, NULL, '2023-07-06 22:02:20', '2023-07-06 22:02:20', '0');
INSERT INTO `sys_menu` VALUES (32, 30, '评论管理', '/subject/comment', 'subject:comment', '1', '1', '1', 'el-icon-s-order', 2, NULL, '2023-07-27 09:28:52', '2023-07-27 09:28:52', '0');
INSERT INTO `sys_menu` VALUES (33, 0, '消息管理', '/info', 'info', '0', '0', '1', 'el-icon-s-goods', 5, NULL, '2023-07-27 09:29:57', '2023-07-27 09:29:57', '0');
INSERT INTO `sys_menu` VALUES (34, 33, '公告管理', '/info/notice', 'info:notice', '0', '1', '1', 'el-icon-s-promotion', 1, NULL, '2023-07-27 09:30:39', '2023-07-27 09:30:39', '0');
INSERT INTO `sys_menu` VALUES (35, 33, '消息通知', '/info/inform', 'info:inform', '0', '1', '1', 'el-icon-s-platform', 2, NULL, '2023-07-27 09:32:16', '2023-07-27 09:32:16', '0');
INSERT INTO `sys_menu` VALUES (36, 1, '个人中心', '/system/user/profile', 'system:profile', '1', '1', '0', 'el-icon-upload', 5, NULL, '2023-07-27 10:46:46', '2023-07-27 10:46:46', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
  `role_auth` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色权限标识',
  `is_sort` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '排序字段',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'system', '1', '', '2023-05-01 14:46:19', '2023-05-04 22:26:06', '0');
INSERT INTO `sys_role` VALUES (2, '操作员', 'opera', '2', NULL, '2023-05-05 21:47:54', '2023-05-05 21:48:15', '0');
INSERT INTO `sys_role` VALUES (3, '审核员', 'check', '2', NULL, '2023-07-03 20:37:10', '2023-07-03 20:37:10', '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色id',
  `menu_id` bigint NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 19);
INSERT INTO `sys_role_menu` VALUES (1, 20);
INSERT INTO `sys_role_menu` VALUES (1, 21);
INSERT INTO `sys_role_menu` VALUES (1, 22);
INSERT INTO `sys_role_menu` VALUES (1, 23);
INSERT INTO `sys_role_menu` VALUES (1, 24);
INSERT INTO `sys_role_menu` VALUES (1, 25);
INSERT INTO `sys_role_menu` VALUES (1, 26);
INSERT INTO `sys_role_menu` VALUES (1, 27);
INSERT INTO `sys_role_menu` VALUES (1, 28);
INSERT INTO `sys_role_menu` VALUES (1, 30);
INSERT INTO `sys_role_menu` VALUES (1, 31);
INSERT INTO `sys_role_menu` VALUES (1, 32);
INSERT INTO `sys_role_menu` VALUES (1, 33);
INSERT INTO `sys_role_menu` VALUES (1, 34);
INSERT INTO `sys_role_menu` VALUES (1, 35);
INSERT INTO `sys_role_menu` VALUES (1, 36);
INSERT INTO `sys_role_menu` VALUES (1, 37);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 12);
INSERT INTO `sys_role_menu` VALUES (2, 13);
INSERT INTO `sys_role_menu` VALUES (2, 14);
INSERT INTO `sys_role_menu` VALUES (2, 15);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `password` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '性别：0:男；1:女',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `dept_id` int NULL DEFAULT NULL COMMENT '所属部门id',
  `is_use` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '1' COMMENT '是否启用：0未启用；1已启用',
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$7MwTBtKeIDmSYoG40Zfdsu49ZQPwjKlI7qcLPjI1ij0CESLtanb4q', '张三', '/static/avatar.gif', '17388888188', NULL, '1', NULL, 2, '1', '0', NULL, '2023-05-01 13:53:20', '2023-08-03 16:02:22', '2023-07-27 13:02:02');
INSERT INTO `sys_user` VALUES (2, '222', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888887', 22, '0', 'xx_2589@163.com', 1, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-05-03 18:10:13');
INSERT INTO `sys_user` VALUES (3, '333', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888888', 12, '1', 'xx_2589@163.com', 4, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-05-03 19:41:33');
INSERT INTO `sys_user` VALUES (4, '444', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888888', 12, '1', 'xx_2589@163.com', 2, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-05-03 22:28:45');
INSERT INTO `sys_user` VALUES (5, '555', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888888', NULL, '1', NULL, 2, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-06-13 18:48:27');
INSERT INTO `sys_user` VALUES (6, '999', NULL, '李四', '/static/avatar.gif', '18999999989', NULL, '0', NULL, 3, '0', '0', NULL, '2023-05-03 21:17:57', NULL, '2023-06-13 18:48:30');
INSERT INTO `sys_user` VALUES (7, 'qqqq', '$2a$10$7MwTBtKeIDmSYoG40Zfdsu49ZQPwjKlI7qcLPjI1ij0CESLtanb4q', '测试用户', '/static/avatar.gif', '18566656236', NULL, NULL, NULL, NULL, '1', '0', NULL, '2023-07-26 15:04:46', '2023-07-26 15:28:36', '2023-07-27 09:42:02');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

-- ----------------------------
-- Table structure for uni_address
-- ----------------------------
DROP TABLE IF EXISTS `uni_address`;
CREATE TABLE `uni_address`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '姓名',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '地址',
  `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '手机号',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '是否默认地址：0否；1是',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '收货地址表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of uni_address
-- ----------------------------
INSERT INTO `uni_address` VALUES (1, 'admin', 'zhangsan', '上海市浦东新区', '15588885858', '0', '2023-08-03 09:56:09');

-- ----------------------------
-- Table structure for uni_interval
-- ----------------------------
DROP TABLE IF EXISTS `uni_interval`;
CREATE TABLE `uni_interval`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `time_interval` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '预约时间区间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '预约时间区间表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of uni_interval
-- ----------------------------
INSERT INTO `uni_interval` VALUES (1, '08:00-10:00', NULL);
INSERT INTO `uni_interval` VALUES (2, '10:00-12:00', NULL);
INSERT INTO `uni_interval` VALUES (3, '13:00-15:00', NULL);
INSERT INTO `uni_interval` VALUES (4, '15:00-17:00', NULL);

-- ----------------------------
-- Table structure for uni_interval_his
-- ----------------------------
DROP TABLE IF EXISTS `uni_interval_his`;
CREATE TABLE `uni_interval_his`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `subject_id` bigint NOT NULL COMMENT '主业务id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT ' 预约业务标题',
  `app_day` date NULL DEFAULT NULL COMMENT '预约的天',
  `time_interval` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '区间',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '预约人',
  `user_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '预约人code',
  `appoint_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '预约状态：0已预约；1已完成；2已取消',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '预约历史表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of uni_interval_his
-- ----------------------------
INSERT INTO `uni_interval_his` VALUES (1, 4, '1', '2023-07-29', '08:00-10:00', 'admin', NULL, '0', NULL);

-- ----------------------------
-- Table structure for uni_order
-- ----------------------------
DROP TABLE IF EXISTS `uni_order`;
CREATE TABLE `uni_order`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `subject_id` bigint NULL DEFAULT NULL COMMENT 'subject_id',
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '订单编号',
  `title` varchar(24) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '标题',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '购买人username',
  `buy_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '收货人',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '收货地址',
  `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '手机号',
  `unit_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '单价',
  `buy_num` int NULL DEFAULT NULL COMMENT '购买数量',
  `total_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价',
  `order_status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单状态:1待付款；2待发货；3待收货；4待评价；5完成；6取消',
  `express_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '快递单号',
  `evaluate_star` int NULL DEFAULT NULL COMMENT '评价星级⭐',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `pay_time` datetime NULL DEFAULT NULL COMMENT '付款时间',
  `send_time` datetime NULL DEFAULT NULL COMMENT '发货时间',
  `shipped_time` datetime NULL DEFAULT NULL COMMENT '收货时间',
  `evaluated_time` datetime NULL DEFAULT NULL COMMENT '评价时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '订单结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '订单记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of uni_order
-- ----------------------------
INSERT INTO `uni_order` VALUES (1, 4, '1686919163712675840', '测试测试测试测试', 'admin', 'zhangsan', '上海市浦东新区', '15588885858', 113.00, 2, 226.00, '2', NULL, 0, '2023-08-03 09:57:25', '2023-08-03 09:57:27', NULL, NULL, NULL, NULL);
INSERT INTO `uni_order` VALUES (2, 4, '1686988165663170560', '1111', 'admin', 'zhangsan', '上海市浦东新区', '15588885858', 113.00, 3, 339.00, '3', NULL, 0, '2023-08-03 14:31:36', '2023-08-03 14:31:38', NULL, NULL, NULL, NULL);
INSERT INTO `uni_order` VALUES (3, 4, '1686988683487748096', '1111', 'admin', 'zhangsan', '上海市浦东新区', '15588885858', 113.00, 1, 113.00, '2', NULL, 0, '2023-08-03 14:33:40', '2023-08-03 14:33:41', NULL, NULL, NULL, NULL);

-- ----------------------------
-- View structure for main_file_view
-- ----------------------------
DROP VIEW IF EXISTS `main_file_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `main_file_view` AS select `si`.`id` AS `id`,`sf`.`id` AS `file_id`,`sf`.`file_name` AS `file_name`,`sf`.`file_path` AS `file_path`,`sf`.`file_type` AS `file_type`,`sf`.`file_suffix` AS `file_suffix`,`sf`.`file_size` AS `file_size`,`sf`.`create_by` AS `create_by`,`sf`.`create_time` AS `create_time` from (`sys_index` `si` join `sys_file` `sf` on(((`si`.`id` = `sf`.`index_id`) and (`sf`.`file_type` = '0') and (`sf`.`is_delete` = '0'))));

SET FOREIGN_KEY_CHECKS = 1;
