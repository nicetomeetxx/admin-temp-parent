/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : admin-temp

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 27/07/2023 20:18:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint NOT NULL COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1665256079496237057, 'sys_menu', '菜单表', NULL, NULL, 'SysMenu', 'crud', 'cn.xu.test', 'system', 'menu', '菜单', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":\"2\"}', NULL, '2023-05-04 22:05:33', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1665256079567540238, 'sys_user', '用户表', NULL, NULL, 'SysUser', 'crud', 'com.ruoyi.system', 'system', 'user', '用户', 'ljxu', '0', '/', '{\"parentMenuId\":2}', NULL, '2023-05-04 22:05:51', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1668250038367584257, 'sys_logs', '日志表', NULL, NULL, 'SysLogs', 'crud', 'cn.xu.tools.logs', 'logs', 'logs', '操作日志', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":21}', NULL, '2023-04-23 20:41:19', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1676950524190740482, 'pro_subject', '首页业务表', NULL, NULL, 'ProSubject', 'crud', 'cn.xu.subject', 'subject', 'subject', '首页业务', 'ljxu', '1', 'E:\\grad-template-admin\\admin-temp-parent\\spring-admin-temp\\src', '{\"parentMenuId\":\"30\"}', 'admin', '2023-07-06 21:36:49', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1684167579778674690, 'pro_inform', '系统消息(点赞评论收藏)表', NULL, NULL, 'ProInform', 'crud', 'cn.xu.work', 'work', 'inform', '系统消息(点赞评论收藏)', 'ljxu', '0', '/', '{}', 'admin', '2023-07-26 19:43:14', NULL, NULL, NULL);
INSERT INTO `gen_table` VALUES (1684167579912892426, 'pro_notice', '系统公告信息表', NULL, NULL, 'ProNotice', 'crud', 'cn.xu.work', 'work', 'notice', '系统公告信息', 'ljxu', '0', '/', NULL, 'admin', '2023-07-26 18:27:33', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint NOT NULL COMMENT '编号',
  `table_id` bigint NULL DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1665256079496237058, 1665256079496237057, 'menu_id', '主键', 'bigint', 'Long', 'menuId', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540225, 1665256079496237057, 'p_menu_id', '父权限id', 'bigint', 'Long', 'pMenuId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540226, 1665256079496237057, 'menu_name', '权限名称', 'varchar(20)', 'String', 'menuName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540227, 1665256079496237057, 'url', '权限路径', 'varchar(40)', 'String', 'url', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540228, 1665256079496237057, 'menu_auth', '权限标识', 'varchar(255)', 'String', 'menuAuth', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'fileUpload', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540229, 1665256079496237057, 'is_hidden', '是否隐藏:0否；1是', 'char(1)', 'String', 'isHidden', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540230, 1665256079496237057, 'menu_type', '菜单类型: 0目录 1菜单 2按钮', 'varchar(1)', 'String', 'menuType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540231, 1665256079496237057, 'show_type', '展示类型: 第一栏,第二栏,第三栏', 'char(1)', 'String', 'showType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540232, 1665256079496237057, 'menu_icon', '菜单图标', 'varchar(255)', 'String', 'menuIcon', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540233, 1665256079496237057, 'is_sort', '排序等级', 'int', 'Long', 'isSort', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540234, 1665256079496237057, 'remark', '权限描述', 'varchar(50)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 11, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540235, 1665256079496237057, 'create_time', '', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 12, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540236, 1665256079496237057, 'update_time', '', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 13, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079567540237, 1665256079496237057, 'is_delete', '是否删除: 0未删除；1已删除', 'varchar(1)', 'String', 'isDelete', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 14, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649090, 1665256079567540238, 'id', '主键', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649091, 1665256079567540238, 'username', '用户名', 'varchar(30)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649092, 1665256079567540238, 'password', '密码', 'varchar(300)', 'String', 'password', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649093, 1665256079567540238, 'name', '姓名', 'varchar(20)', 'String', 'name', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649094, 1665256079567540238, 'avatar', '头像', 'varchar(255)', 'String', 'avatar', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649095, 1665256079567540238, 'telephone', '手机号', 'varchar(11)', 'String', 'telephone', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649096, 1665256079567540238, 'age', '年龄', 'int', 'Long', 'age', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649097, 1665256079567540238, 'sex', '性别：0:男；1:女', 'varchar(1)', 'String', 'sex', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649098, 1665256079567540238, 'email', '邮箱', 'varchar(50)', 'String', 'email', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 9, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649099, 1665256079567540238, 'dept_id', '所属部门id', 'int', 'Long', 'deptId', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 10, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649100, 1665256079567540238, 'is_use', '是否启用：0未启用；1已启用', 'varchar(1)', 'String', 'isUse', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 11, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649101, 1665256079567540238, 'is_delete', '是否删除: 0未删除；1已删除', 'varchar(1)', 'String', 'isDelete', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 12, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649102, 1665256079567540238, 'remark', '备注', 'varchar(30)', 'String', 'remark', '0', '0', '1', '1', '1', '1', NULL, 'EQ', 'input', '', 13, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649103, 1665256079567540238, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 14, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649104, 1665256079567540238, 'login_time', '登录时间', 'datetime', 'Date', 'loginTime', '0', '0', '1', '1', '1', '0', '0', 'EQ', 'datetime', '', 15, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1665256079634649105, 1665256079567540238, 'update_time', '修改时间', 'datetime', 'Date', 'updateTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 16, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750081, 1668250038367584257, 'log_id', '主键', 'bigint', 'Long', 'logId', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750082, 1668250038367584257, 'operation', '操作类型', 'varchar(20)', 'String', 'operation', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750083, 1668250038367584257, 'method', '请求方法', 'varchar(255)', 'String', 'method', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 3, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038392750084, 1668250038367584257, 'params', '请求参数', 'varchar(500)', 'String', 'params', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664642, 1668250038367584257, 'username', '操作人', 'varchar(50)', 'String', 'username', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', '', 5, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664643, 1668250038367584257, 'ip', 'ip地址', 'varchar(255)', 'String', 'ip', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 6, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664644, 1668250038367584257, 'time', '执行时长', 'bigint', 'Long', 'time', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 7, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1668250038455664645, 1668250038367584257, 'create_time', '操作时间', 'datetime', 'Date', 'createTime', '0', '0', '1', NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 8, NULL, NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849345, 1676950524190740482, 'id', '', 'bigint', 'Long', 'id', '1', '1', '0', NULL, '0', '0', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849346, 1676950524190740482, 'title', '标题', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849347, 1676950524190740482, 'index_id', 'index', 'bigint', 'Long', 'indexId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849348, 1676950524190740482, 'main_text', '正文', 'text', 'String', 'mainText', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'textarea', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849349, 1676950524190740482, 'type', '类型', 'varchar(128)', 'String', 'type', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', 'sub_project_type', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849350, 1676950524190740482, 'collect_num', '收藏数量', 'bigint', 'Long', 'collectNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849351, 1676950524190740482, 'like_num', '点赞数量', 'bigint', 'Long', 'likeNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849352, 1676950524190740482, 'comment_num', '评论数量', 'bigint', 'Long', 'commentNum', '0', '0', '0', '0', '0', '1', '0', 'EQ', 'input', '', 8, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849353, 1676950524190740482, 'create_by', '创建人', 'varchar(128)', 'String', 'createBy', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'input', '', 9, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849354, 1676950524190740482, 'create_time', ' 创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 10, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1676950524257849355, 1676950524190740482, 'is_delete', '是否删除: 0未删除；1已删除', 'char(1)', 'String', 'isDelete', '0', '0', '1', '0', '0', '0', '0', 'EQ', 'input', '', 11, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579845783553, 1684167579778674690, 'id', '', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '0', '1', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892418, 1684167579778674690, 'inform_user', '接收通知人', 'varchar(32)', 'String', 'informUser', '0', '0', '1', '0', '0', '1', '1', 'LIKE', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892419, 1684167579778674690, 'subject_id', '主题id', 'bigint', 'Long', 'subjectId', '0', '0', '1', '0', '0', '0', '0', 'EQ', 'input', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892420, 1684167579778674690, 'inform_content', '通知内容', 'varchar(255)', 'String', 'informContent', '0', '0', '1', '0', '0', '1', '0', 'EQ', 'textarea', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892421, 1684167579778674690, 'info_type', '消息类型：0收藏;1点赞;2评论;9系统', 'char(1)', 'String', 'infoType', '0', '0', '1', '0', '0', '1', '1', 'EQ', 'select', '', 5, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892422, 1684167579778674690, 'inform_status', '状态：0未读；1已读', 'char(1)', 'String', 'informStatus', '0', '0', '1', '0', '0', '1', '1', 'EQ', 'select', '', 6, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892423, 1684167579778674690, 'create_user', '消息创建人', 'varchar(255)', 'String', 'createUser', '0', '0', '1', '0', '0', '1', '1', 'LIKE', 'input', '', 7, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892424, 1684167579778674690, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579912892425, 1684167579778674690, 'read_time', '已读时间', 'datetime', 'Date', 'readTime', '0', '0', '1', '0', '0', '1', '0', 'EQ', 'datetime', '', 9, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579980001282, 1684167579912892426, 'id', '', 'bigint', 'Long', 'id', '1', '1', '1', NULL, '1', '1', NULL, 'EQ', 'input', '', 1, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579980001283, 1684167579912892426, 'title', '通知标题', 'varchar(255)', 'String', 'title', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 2, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579980001284, 1684167579912892426, 'content', '通知内容', 'varchar(1000)', 'String', 'content', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'editor', '', 3, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579980001285, 1684167579912892426, 'create_user', '创建人', 'varchar(32)', 'String', 'createUser', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'input', '', 4, 'admin', NULL, NULL, NULL);
INSERT INTO `gen_table_column` VALUES (1684167579980001286, 1684167579912892426, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, NULL, NULL, NULL, NULL, 'EQ', 'datetime', '', 5, 'admin', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for pro_inform
-- ----------------------------
DROP TABLE IF EXISTS `pro_inform`;
CREATE TABLE `pro_inform`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `inform_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '接收通知人',
  `subject_id` bigint NULL DEFAULT NULL COMMENT '主题id',
  `inform_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '通知内容',
  `info_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '消息类型：0收藏;1点赞;2评论;9系统',
  `inform_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '状态：0未读；1已读',
  `create_user` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '消息创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `read_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '已读时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统消息(点赞评论收藏)表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_inform
-- ----------------------------
INSERT INTO `pro_inform` VALUES (1, 'admin', 5, '点赞了您的:测试内容', '1', '1', 'admin', '2023-07-26 18:31:12', '2023-07-26 18:35:42');
INSERT INTO `pro_inform` VALUES (2, 'admin', 5, '点赞了您的:测试内容', '1', '1', 'admin', '2023-07-26 18:35:20', '2023-07-26 18:44:40');
INSERT INTO `pro_inform` VALUES (3, 'admin', 5, '评论了您的:测试内容', '2', '1', 'admin', '2023-07-26 22:30:25', '2023-07-27 19:45:48');
INSERT INTO `pro_inform` VALUES (4, 'admin', 5, '收藏了您的:测试内容', '0', '1', 'admin', '2023-07-26 22:34:50', '2023-07-27 19:47:13');

-- ----------------------------
-- Table structure for pro_notice
-- ----------------------------
DROP TABLE IF EXISTS `pro_notice`;
CREATE TABLE `pro_notice`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '通知标题',
  `content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '通知内容',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1684174869076287492 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '系统公告信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_notice
-- ----------------------------
INSERT INTO `pro_notice` VALUES (1, '111', '你好', 'admin', '2023-07-26 08:16:29');
INSERT INTO `pro_notice` VALUES (1684174869076287491, '2', '21', '张三', '2023-07-26 20:22:25');

-- ----------------------------
-- Table structure for pro_record
-- ----------------------------
DROP TABLE IF EXISTS `pro_record`;
CREATE TABLE `pro_record`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `subject_id` bigint NULL DEFAULT NULL COMMENT '主题id',
  `like_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '收藏0；点赞1；评论2',
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论内容',
  `create_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `subject_user` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '归属人',
  `comment_reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论回复',
  `reply_time` datetime NULL DEFAULT NULL COMMENT '回复时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '点赞，评论，收藏记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_record
-- ----------------------------
INSERT INTO `pro_record` VALUES (1, 5, '2', '你好', 'test', '2023-07-23 14:43:56', 'admin', '1111', '2023-07-23 14:44:02');
INSERT INTO `pro_record` VALUES (2, 5, '2', 'com', 'test', '2023-07-26 21:58:58', 'admin', '2', '2023-07-23 15:34:38');
INSERT INTO `pro_record` VALUES (3, 5, '1', NULL, 'admin', NULL, 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (4, 5, '1', NULL, 'admin', NULL, 'admin', NULL, NULL);
INSERT INTO `pro_record` VALUES (5, 5, '2', '2221', 'admin', '2023-07-26 22:30:57', 'admin', '211', '2023-07-26 22:32:54');
INSERT INTO `pro_record` VALUES (6, 5, '0', NULL, 'admin', '2023-07-26 22:34:50', 'admin', NULL, NULL);

-- ----------------------------
-- Table structure for pro_subject
-- ----------------------------
DROP TABLE IF EXISTS `pro_subject`;
CREATE TABLE `pro_subject`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `index_id` bigint NULL DEFAULT NULL COMMENT 'index',
  `main_text` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '正文',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `collect_num` bigint NULL DEFAULT 0 COMMENT '收藏数量',
  `like_num` bigint NULL DEFAULT 0 COMMENT '点赞数量',
  `comment_num` bigint NULL DEFAULT 0 COMMENT '评论数量',
  `username` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT ' 创建时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '首页业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pro_subject
-- ----------------------------
INSERT INTO `pro_subject` VALUES (5, '测试内容', 1681636937954103296, '测试内容\n![](http://127.0.0.1:8081/admin/static/317952.jpg)', 'no_1', 13, 13, 12, 'admin', '2023-07-19 22:05:37', '0');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `sys_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父id',
  `config_key` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'key',
  `config_value` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'value',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '注释',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '修改人',
  PRIMARY KEY (`sys_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统配置表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_config
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '部门名称',
  `p_dept_id` bigint NULL DEFAULT NULL COMMENT '上级部门id',
  `leader` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '部门负责人',
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '负责人电话',
  `is_sort` int NULL DEFAULT NULL COMMENT '显示顺序',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`dept_id`) USING BTREE,
  UNIQUE INDEX `dept_name`(`dept_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '部门表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, '案例公司', 0, '张三', '17388888888', NULL, NULL, '2023-05-03 16:40:34', '2023-05-03 16:48:02', '1');
INSERT INTO `sys_dept` VALUES (2, '研发中心', 1, '李四', '13333333333', NULL, NULL, '2023-05-03 16:40:48', '2023-05-03 16:48:03', '1');
INSERT INTO `sys_dept` VALUES (3, '研发部', 2, NULL, NULL, NULL, NULL, '2023-05-03 16:40:56', '2023-05-03 16:48:04', '1');
INSERT INTO `sys_dept` VALUES (4, '测试部', 2, NULL, NULL, NULL, NULL, '2023-05-03 16:41:27', '2023-05-03 16:48:04', '1');
INSERT INTO `sys_dept` VALUES (5, '需求基地', 1, '王五', '18999999999', NULL, NULL, '2023-05-03 16:41:44', '2023-05-03 16:48:05', '1');
INSERT INTO `sys_dept` VALUES (6, '需求部', 5, '赵六', '15855552525', NULL, NULL, '2023-05-03 16:41:53', '2023-05-03 16:48:06', '1');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '1', 'sys_normal_disable', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '0', 'sys_normal_disable', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (29, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (30, 0, '初一', 'no_1', 'subject_type', '', 'default', 'N', '1', '', NULL, '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (31, 1, '初二', 'no_2', 'subject_type', NULL, 'default', 'N', '1', '', NULL, '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '1', 'admin', '2023-06-06 21:06:23', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '业务类型', 'subject_type', '1', '', '2023-07-08 17:16:35', '', NULL, '业务类型列表');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `index_id` bigint NULL DEFAULT NULL COMMENT '索引id',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件地址(url)',
  `file_type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件类型(0预览；1图片；2附件)',
  `file_suffix` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件后缀名',
  `file_size` double NULL DEFAULT NULL COMMENT '文件大小',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件(图片)表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES (145, 1681636937954103296, '315747.jpg', '/static/315747.jpg', '0', 'jpg', 40224, 'admin', '2023-07-19 20:08:27', '1');
INSERT INTO `sys_file` VALUES (146, 1681636937954103296, '317952.jpg', '/static/317952.jpg', '3', 'jpg', 416170, 'admin', '2023-07-19 20:08:34', '0');
INSERT INTO `sys_file` VALUES (149, 1681636937954103296, '2049047.jpg', '/static/2049047.jpg', '0', 'jpg', 1009239, 'admin', '2023-07-27 19:58:21', '0');
INSERT INTO `sys_file` VALUES (150, 1681636937954103296, '1000705.jpg', '/static/1000705.jpg', '1', 'jpg', 2064210, 'admin', '2023-07-27 20:08:14', '0');
INSERT INTO `sys_file` VALUES (151, 1681636937954103296, 'back.jpg', '/static/back.jpg', '1', 'jpg', 182765, 'admin', '2023-07-27 20:09:25', '0');

-- ----------------------------
-- Table structure for sys_index
-- ----------------------------
DROP TABLE IF EXISTS `sys_index`;
CREATE TABLE `sys_index`  (
  `id` bigint NOT NULL,
  `create_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '附件索引表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_index
-- ----------------------------
INSERT INTO `sys_index` VALUES (1681636937954103296, NULL, '2023-07-19 20:08:43');

-- ----------------------------
-- Table structure for sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs`;
CREATE TABLE `sys_logs`  (
  `log_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `log_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '描述',
  `operation` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作类型',
  `method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '请求方法',
  `params` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '请求参数',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '操作人',
  `ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `time` bigint NOT NULL COMMENT '执行时长',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1684182297410957314 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '日志表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_logs
-- ----------------------------
INSERT INTO `sys_logs` VALUES (1668257096047480833, '新增用户', '1', 'SysUserController.update()', '[{\"age\":22,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":1,\"email\":\"xx_2589@163.com\",\"id\":2,\"isDelete\":\"0\",\"isUse\":\"1\",\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"opera\",\"roleId\":2,\"roleName\":\"操作员\"}],\"sex\":\"0\",\"telephone\":\"17388888887\",\"updateTime\":1683108613000,\"username\":\"222\"}]', 'admin', '127.0.0.1', 72, '2023-06-12 22:01:01');
INSERT INTO `sys_logs` VALUES (1668598193651109890, '修改用户数据', '2', 'SysUserController.update()', '[{\"age\":22,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":1,\"email\":\"xx_2589@163.com\",\"id\":2,\"isDelete\":\"0\",\"isUse\":\"1\",\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"opera\",\"roleId\":2,\"roleName\":\"操作员\"}],\"sex\":\"0\",\"telephone\":\"17388888887\",\"updateTime\":1683108613000,\"username\":\"222\"}]', 'admin', '127.0.0.1', 76, '2023-06-13 20:36:25');
INSERT INTO `sys_logs` VALUES (1677608143262048258, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1677608117790040064,\"mainText\":\"222\",\"title\":\"11\",\"type\":\"no_2\"}]', 'admin', '127.0.0.1', 74, '2023-07-08 17:18:44');
INSERT INTO `sys_logs` VALUES (1678724370592161793, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[],\"id\":1,\"indexId\":1677608117790040000,\"isDelete\":\"0\",\"mainText\":\"222\",\"title\":\"11\",\"type\":\"no_2\"}]', 'admin', '127.0.0.1', 75, '2023-07-11 19:14:13');
INSERT INTO `sys_logs` VALUES (1681629647062659073, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689074048000,\"fileName\":\"315747.jpg\",\"filePath\":\"/static/315747.jpg\",\"fileSize\":40224,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":140,\"indexId\":\"1677608117790040000\",\"isDelete\":\"0\"}],\"id\":1,\"indexId\":1677608117790040000,\"mainText\":\"222\",\"title\":\"11\",\"type\":\"no_1\"}]', 'admin', '127.0.0.1', 86, '2023-07-19 19:38:45');
INSERT INTO `sys_logs` VALUES (1681634959849123841, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689074048000,\"fileName\":\"315747.jpg\",\"filePath\":\"/static/315747.jpg\",\"fileSize\":40224,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":140,\"indexId\":\"1677608117790040000\",\"isDelete\":\"0\"}],\"id\":1,\"indexId\":1677608117790040000,\"mainText\":\"222![](http://127.0.0.1:8081/admin/static/323014.jpg)\",\"title\":\"11\",\"type\":\"no_1\"}]', 'admin', '127.0.0.1', 5, '2023-07-19 19:59:52');
INSERT INTO `sys_logs` VALUES (1681635125771595778, '首页业务', '2', 'ProSubjectController.edit()', '[{\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689074048000,\"fileName\":\"315747.jpg\",\"filePath\":\"/static/315747.jpg\",\"fileSize\":40224,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":140,\"indexId\":\"1677608117790040000\",\"isDelete\":\"0\"}],\"id\":1,\"indexId\":1677608117790040000,\"mainText\":\"222![](http://127.0.0.1:8081/admin/static/323014.jpg)\",\"title\":\"11\",\"type\":\"no_1\"}]', 'admin', '127.0.0.1', 2, '2023-07-19 20:00:31');
INSERT INTO `sys_logs` VALUES (1681635278305849346, '首页业务', '3', 'ProSubjectController.remove()', '[[1]]', 'admin', '127.0.0.1', 8, '2023-07-19 20:01:08');
INSERT INTO `sys_logs` VALUES (1681635604127772673, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1681635499920289792,\"mainText\":\"测试正文内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试数据\",\"type\":\"no_1\"}]', 'admin', '127.0.0.1', 3, '2023-07-19 20:02:25');
INSERT INTO `sys_logs` VALUES (1681636931394211841, '首页业务', '3', 'ProSubjectController.remove()', '[[4]]', 'admin', '127.0.0.1', 51, '2023-07-19 20:07:42');
INSERT INTO `sys_logs` VALUES (1681637187028652034, '首页业务', '1', 'ProSubjectController.add()', '[{\"indexId\":1681636937954103296,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\"}]', 'admin', '127.0.0.1', 33, '2023-07-19 20:08:43');
INSERT INTO `sys_logs` VALUES (1683086242925727746, '修改用户数据', '2', 'SysUserController.update()', '[{\"age\":21,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":2,\"email\":\"xx\",\"id\":1,\"isDelete\":\"0\",\"isUse\":\"1\",\"loginTime\":1690108543000,\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"system\",\"roleId\":1,\"roleName\":\"管理员\"}],\"sex\":\"1\",\"telephone\":\"17388888888\",\"updateTime\":1686661687000,\"username\":\"admin\"}]', 'admin', '127.0.0.1', 88, '2023-07-23 20:06:45');
INSERT INTO `sys_logs` VALUES (1683086265981816833, '修改用户数据', '2', 'SysUserController.update()', '[{\"age\":21,\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1682920400000,\"deptId\":2,\"email\":\"xx\",\"id\":1,\"isDelete\":\"0\",\"isUse\":\"1\",\"loginTime\":1690108543000,\"name\":\"张三\",\"roleList\":[{\"roleAuth\":\"system\",\"roleId\":1,\"roleName\":\"管理员\"}],\"sex\":\"1\",\"telephone\":\"17388888882\",\"updateTime\":1686661687000,\"username\":\"admin\"}]', 'admin', '127.0.0.1', 9, '2023-07-23 20:06:50');
INSERT INTO `sys_logs` VALUES (1684174869076287491, '系统公告信息', '1', 'ProNoticeController.add()', '[{\"content\":\"222\",\"title\":\"111\"}]', 'admin', '127.0.0.1', 77, '2023-07-26 20:12:33');
INSERT INTO `sys_logs` VALUES (1684176263237795842, '系统公告信息', '2', 'ProNoticeController.edit()', '[{\"content\":\"222333\",\"createTime\":1690373789000,\"createUser\":\"admin\",\"id\":1,\"title\":\"111\"}]', 'admin', '127.0.0.1', 65, '2023-07-26 20:18:06');
INSERT INTO `sys_logs` VALUES (1684177351466795010, '系统公告信息', '1', 'ProNoticeController.add()', '[{\"content\":\"21\",\"title\":\"2\"}]', 'admin', '127.0.0.1', 74, '2023-07-26 20:22:25');
INSERT INTO `sys_logs` VALUES (1684180363857784834, '系统公告信息', '2', 'ProNoticeController.edit()', '[{\"content\":\"1112\"}]', 'admin', '127.0.0.1', 62, '2023-07-26 20:34:23');
INSERT INTO `sys_logs` VALUES (1684182297410957313, '系统公告信息', '2', 'ProNoticeController.edit()', '[{\"content\":\"你好\",\"createTime\":1690330589000,\"createUser\":\"admin\",\"id\":1,\"title\":\"111\"}]', 'admin', '127.0.0.1', 3, '2023-07-26 20:42:04');
INSERT INTO `sys_logs` VALUES (1684203149808562178, '修改用户数据', '2', 'SysUserController.update()', '[{\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1690368764000,\"id\":8,\"isUse\":\"1\",\"loginTime\":1690368775000,\"name\":\"1690368764190\",\"roleList\":[],\"sex\":\"0\",\"telephone\":\"15555555556\",\"updateTime\":1690368764000,\"username\":\"15555555556\"}]', 'admin', '127.0.0.1', 81, '2023-07-26 22:04:56');
INSERT INTO `sys_logs` VALUES (1684203193177665538, '修改用户数据', '2', 'SysUserController.update()', '[{\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1690368764000,\"id\":8,\"isUse\":\"1\",\"loginTime\":1690368775000,\"name\":\"1690368764190\",\"roleList\":[],\"sex\":\"1\",\"telephone\":\"15555555556\",\"updateTime\":1690380297000,\"username\":\"15555555556\"}]', 'admin', '127.0.0.1', 6, '2023-07-26 22:05:06');
INSERT INTO `sys_logs` VALUES (1684203629456584705, '修改用户数据', '2', 'SysUserController.update()', '[{\"avatar\":\"https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif\",\"createTime\":1690368764000,\"id\":8,\"isUse\":\"1\",\"loginTime\":1690368775000,\"name\":\"1690368764190\",\"roleList\":[],\"sex\":\"0\",\"telephone\":\"15555555556\",\"updateTime\":1690380307000,\"username\":\"15555555556\"}]', 'admin', '127.0.0.1', 4, '2023-07-26 22:06:50');
INSERT INTO `sys_logs` VALUES (1684209563209777154, '收藏/点赞/评论', '1', 'ProRecordController.collectOrLikeOrCommentSubject()', '[{\"comment\":\"2221\",\"likeType\":\"2\",\"subjectId\":5}]', 'admin', '127.0.0.1', 80, '2023-07-26 22:30:25');
INSERT INTO `sys_logs` VALUES (1684209693841375234, '回复评论', '2', 'ProRecordController.replyComment()', '[{\"commentReply\":\"2\",\"id\":5,\"subjectId\":5}]', 'admin', '127.0.0.1', 4, '2023-07-26 22:30:56');
INSERT INTO `sys_logs` VALUES (1684210187523555330, '回复评论', '2', 'ProRecordController.replyComment()', '[{\"commentReply\":\"211\",\"id\":5,\"subjectId\":5}]', 'admin', '127.0.0.1', 66, '2023-07-26 22:32:54');
INSERT INTO `sys_logs` VALUES (1684210675971227650, '收藏/点赞/评论', '1', 'ProRecordController.collectOrLikeOrCommentSubject()', '[{\"likeType\":\"0\",\"subjectId\":5}]', 'admin', '127.0.0.1', 483, '2023-07-26 22:34:50');
INSERT INTO `sys_logs` VALUES (1684211049440444417, '评论状态', '2', 'ProInformController.readMessage()', '[4]', 'admin', '127.0.0.1', 4, '2023-07-26 22:36:19');
INSERT INTO `sys_logs` VALUES (1684530521015513089, '评论状态', '2', 'ProInformController.readMessage()', '[3]', 'admin', '127.0.0.1', 48, '2023-07-27 19:45:47');
INSERT INTO `sys_logs` VALUES (1684530876486950914, '评论状态', '2', 'ProInformController.readMessage()', '[4]', 'admin', '127.0.0.1', 41, '2023-07-27 19:47:12');
INSERT INTO `sys_logs` VALUES (1684531408962232322, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":13,\"commentNum\":12,\"createTime\":1689775537000,\"fileIdList\":[145],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1689768507000,\"fileName\":\"315747.jpg\",\"filePath\":\"/static/315747.jpg\",\"fileSize\":40224,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":145,\"indexId\":\"1681636937954103296\",\"isDelete\":\"0\"}],\"id\":5,\"indexId\":1681636937954103300,\"likeNum\":13,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 51, '2023-07-27 19:49:19');
INSERT INTO `sys_logs` VALUES (1684531867198332929, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":13,\"commentNum\":12,\"createTime\":1689775537000,\"fileIdList\":[147],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1690458557000,\"fileName\":\"2049047.jpg\",\"filePath\":\"/static/2049047.jpg\",\"fileSize\":1009239,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":147,\"indexId\":\"1681636937954103300\",\"isDelete\":\"0\"}],\"id\":5,\"indexId\":1681636937954103300,\"likeNum\":13,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 6, '2023-07-27 19:51:08');
INSERT INTO `sys_logs` VALUES (1684531922466676737, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":13,\"commentNum\":12,\"createTime\":1689775537000,\"fileIdList\":[],\"fileList\":[],\"id\":5,\"indexId\":1681636937954103300,\"likeNum\":13,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 2, '2023-07-27 19:51:22');
INSERT INTO `sys_logs` VALUES (1684533695323537409, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":13,\"commentNum\":12,\"createTime\":1689775537000,\"fileIdList\":[],\"fileList\":[],\"id\":5,\"indexId\":1681636937954103296,\"likeNum\":13,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 72, '2023-07-27 19:58:24');
INSERT INTO `sys_logs` VALUES (1684536185477627905, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":13,\"commentNum\":12,\"createTime\":1689775537000,\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1690459101000,\"fileName\":\"2049047.jpg\",\"filePath\":\"/static/2049047.jpg\",\"fileSize\":1009239,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":149,\"indexId\":\"1681636937954103296\",\"isDelete\":\"0\"}],\"id\":5,\"indexId\":1681636937954103296,\"likeNum\":13,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 5, '2023-07-27 20:08:18');
INSERT INTO `sys_logs` VALUES (1684536749678624769, '首页业务', '2', 'ProSubjectController.edit()', '[{\"collectNum\":13,\"commentNum\":12,\"createTime\":1689775537000,\"fileIdList\":[],\"fileList\":[{\"createBy\":\"admin\",\"createTime\":1690459101000,\"fileName\":\"2049047.jpg\",\"filePath\":\"/static/2049047.jpg\",\"fileSize\":1009239,\"fileSuffix\":\"jpg\",\"fileType\":\"0\",\"id\":149,\"indexId\":\"1681636937954103296\",\"isDelete\":\"0\"},{\"createBy\":\"admin\",\"createTime\":1690459694000,\"fileName\":\"1000705.jpg\",\"filePath\":\"/static/1000705.jpg\",\"fileSize\":2064210,\"fileSuffix\":\"jpg\",\"fileType\":\"1\",\"id\":150,\"indexId\":\"1681636937954103296\",\"isDelete\":\"0\"}],\"id\":5,\"indexId\":1681636937954103296,\"likeNum\":13,\"mainText\":\"测试内容\\n![](http://127.0.0.1:8081/admin/static/317952.jpg)\",\"title\":\"测试内容\",\"type\":\"no_1\",\"username\":\"admin\"}]', 'admin', '127.0.0.1', 3, '2023-07-27 20:10:32');

-- ----------------------------
-- Table structure for sys_logs_login
-- ----------------------------
DROP TABLE IF EXISTS `sys_logs_login`;
CREATE TABLE `sys_logs_login`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录账号',
  `ip_addr` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'ip地址',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `login_mac` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录设备',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1684192179770036227 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logs_login
-- ----------------------------
INSERT INTO `sys_logs_login` VALUES (1668606173377449985, 'admin', '127.0.0.1', '2023-06-13 21:08:08', 'Chrome', '2023-06-13 21:08:07');
INSERT INTO `sys_logs_login` VALUES (1668617709655293953, 'admin', '127.0.0.1', '2023-06-13 21:53:58', 'Chrome', '2023-06-13 21:53:58');
INSERT INTO `sys_logs_login` VALUES (1668617980569595905, 'admin', '127.0.0.1', '2023-06-13 21:55:03', 'Chrome', '2023-06-13 21:55:02');
INSERT INTO `sys_logs_login` VALUES (1675846024193323009, 'admin', '127.0.0.1', '2023-07-03 20:36:43', 'Chrome', '2023-07-03 20:36:42');
INSERT INTO `sys_logs_login` VALUES (1676190058011140097, 'admin', '127.0.0.1', '2023-07-04 19:23:47', 'Chrome', '2023-07-04 19:23:46');
INSERT INTO `sys_logs_login` VALUES (1676553936062611458, 'admin', '127.0.0.1', '2023-07-05 19:29:42', 'Chrome', '2023-07-05 19:29:41');
INSERT INTO `sys_logs_login` VALUES (1676940421148311554, 'admin', '127.0.0.1', '2023-07-06 21:05:27', 'Chrome', '2023-07-06 21:05:27');
INSERT INTO `sys_logs_login` VALUES (1676941975045730305, 'admin', '127.0.0.1', '2023-07-06 21:11:38', 'Chrome', '2023-07-06 21:11:37');
INSERT INTO `sys_logs_login` VALUES (1676942914901192706, 'admin', '127.0.0.1', '2023-07-06 21:15:22', 'Chrome', '2023-07-06 21:15:21');
INSERT INTO `sys_logs_login` VALUES (1676943154790215681, 'admin', '127.0.0.1', '2023-07-06 21:16:19', 'Chrome', '2023-07-06 21:16:18');
INSERT INTO `sys_logs_login` VALUES (1676943789522636801, 'admin', '127.0.0.1', '2023-07-06 21:18:50', 'Chrome', '2023-07-06 21:18:50');
INSERT INTO `sys_logs_login` VALUES (1676951151826391041, 'admin', '127.0.0.1', '2023-07-06 21:48:05', 'Chrome', '2023-07-06 21:48:05');
INSERT INTO `sys_logs_login` VALUES (1676951157409009665, 'admin', '127.0.0.1', '2023-07-06 21:48:07', 'Chrome', '2023-07-06 21:48:06');
INSERT INTO `sys_logs_login` VALUES (1676951198777430018, 'admin', '127.0.0.1', '2023-07-06 21:48:17', 'Chrome', '2023-07-06 21:48:16');
INSERT INTO `sys_logs_login` VALUES (1676951623375134721, 'admin', '127.0.0.1', '2023-07-06 21:49:58', 'Chrome', '2023-07-06 21:49:57');
INSERT INTO `sys_logs_login` VALUES (1677606701126115329, 'admin', '127.0.0.1', '2023-07-08 17:13:01', 'Chrome', '2023-07-08 17:13:00');
INSERT INTO `sys_logs_login` VALUES (1678724281177989122, 'admin', '127.0.0.1', '2023-07-11 19:13:52', 'Chrome', '2023-07-11 19:13:52');
INSERT INTO `sys_logs_login` VALUES (1679089635477626881, 'admin', '127.0.0.1', '2023-07-12 19:25:40', 'Chrome', '2023-07-12 19:25:39');
INSERT INTO `sys_logs_login` VALUES (1679115163509186561, 'admin', '127.0.0.1', '2023-07-12 21:07:06', 'Chrome', '2023-07-12 21:07:06');
INSERT INTO `sys_logs_login` VALUES (1681629197097725954, 'admin', '127.0.0.1', '2023-07-19 19:36:58', 'Chrome', '2023-07-19 19:36:58');
INSERT INTO `sys_logs_login` VALUES (1681636915753652225, 'admin', '127.0.0.1', '2023-07-19 20:07:39', 'Chrome', '2023-07-19 20:07:38');
INSERT INTO `sys_logs_login` VALUES (1681667243717419010, 'admin', '127.0.0.1', '2023-07-19 22:08:09', 'Chrome', '2023-07-19 22:08:09');
INSERT INTO `sys_logs_login` VALUES (1681855702620938242, 'admin', '127.0.0.1', '2023-07-20 10:37:02', 'Chrome', '2023-07-20 10:37:01');
INSERT INTO `sys_logs_login` VALUES (1681985108827852801, 'admin', '127.0.0.1', '2023-07-20 19:11:14', 'Chrome', '2023-07-20 19:11:14');
INSERT INTO `sys_logs_login` VALUES (1682983855309393922, 'admin', '127.0.0.1', '2023-07-23 13:19:54', 'Chrome', '2023-07-23 13:19:54');
INSERT INTO `sys_logs_login` VALUES (1683051208239988737, 'admin', '127.0.0.1', '2023-07-23 17:47:32', 'MicroMessenger', '2023-07-23 17:47:32');
INSERT INTO `sys_logs_login` VALUES (1683051298761457665, 'admin', '127.0.0.1', '2023-07-23 17:47:54', 'MicroMessenger', '2023-07-23 17:47:53');
INSERT INTO `sys_logs_login` VALUES (1683051317757460482, 'admin', '127.0.0.1', '2023-07-23 17:47:58', 'MicroMessenger', '2023-07-23 17:47:58');
INSERT INTO `sys_logs_login` VALUES (1683051349063745538, 'admin', '127.0.0.1', '2023-07-23 17:48:06', 'MicroMessenger', '2023-07-23 17:48:05');
INSERT INTO `sys_logs_login` VALUES (1683051468215533570, 'admin', '127.0.0.1', '2023-07-23 17:48:34', 'MicroMessenger', '2023-07-23 17:48:34');
INSERT INTO `sys_logs_login` VALUES (1683051604241006594, 'admin', '127.0.0.1', '2023-07-23 17:49:07', 'MicroMessenger', '2023-07-23 17:49:06');
INSERT INTO `sys_logs_login` VALUES (1683051874245132289, 'admin', '127.0.0.1', '2023-07-23 17:50:11', 'MicroMessenger', '2023-07-23 17:50:11');
INSERT INTO `sys_logs_login` VALUES (1683051923410763778, 'admin', '127.0.0.1', '2023-07-23 17:50:23', 'MicroMessenger', '2023-07-23 17:50:22');
INSERT INTO `sys_logs_login` VALUES (1683052134124208130, 'admin', '127.0.0.1', '2023-07-23 17:51:13', 'MicroMessenger', '2023-07-23 17:51:13');
INSERT INTO `sys_logs_login` VALUES (1683052243884949505, 'admin', '127.0.0.1', '2023-07-23 17:51:39', 'MicroMessenger', '2023-07-23 17:51:39');
INSERT INTO `sys_logs_login` VALUES (1683052641882456065, 'admin', '127.0.0.1', '2023-07-23 17:53:14', 'MicroMessenger', '2023-07-23 17:53:14');
INSERT INTO `sys_logs_login` VALUES (1683052660068958209, 'admin', '127.0.0.1', '2023-07-23 17:53:18', 'MicroMessenger', '2023-07-23 17:53:18');
INSERT INTO `sys_logs_login` VALUES (1683052955192770562, 'admin', '127.0.0.1', '2023-07-23 17:54:29', 'MicroMessenger', '2023-07-23 17:54:28');
INSERT INTO `sys_logs_login` VALUES (1683053378960080898, 'admin', '127.0.0.1', '2023-07-23 17:56:10', 'MicroMessenger', '2023-07-23 17:56:09');
INSERT INTO `sys_logs_login` VALUES (1683053581414895618, 'admin', '127.0.0.1', '2023-07-23 17:56:58', 'MicroMessenger', '2023-07-23 17:56:58');
INSERT INTO `sys_logs_login` VALUES (1683054519487762434, 'admin', '127.0.0.1', '2023-07-23 18:00:42', 'MicroMessenger', '2023-07-23 18:00:41');
INSERT INTO `sys_logs_login` VALUES (1683055687207161858, 'admin', '127.0.0.1', '2023-07-23 18:05:20', 'MicroMessenger', '2023-07-23 18:05:20');
INSERT INTO `sys_logs_login` VALUES (1683056113415557121, 'admin', '127.0.0.1', '2023-07-23 18:07:02', 'MicroMessenger', '2023-07-23 18:07:01');
INSERT INTO `sys_logs_login` VALUES (1683057616888991745, 'admin', '127.0.0.1', '2023-07-23 18:13:00', 'MicroMessenger', '2023-07-23 18:13:00');
INSERT INTO `sys_logs_login` VALUES (1683062763723640833, 'admin', '127.0.0.1', '2023-07-23 18:33:27', 'MicroMessenger', '2023-07-23 18:33:27');
INSERT INTO `sys_logs_login` VALUES (1683063235796750337, 'admin', '127.0.0.1', '2023-07-23 18:35:20', 'MicroMessenger', '2023-07-23 18:35:19');
INSERT INTO `sys_logs_login` VALUES (1683063330910982146, 'admin', '127.0.0.1', '2023-07-23 18:35:43', 'MicroMessenger', '2023-07-23 18:35:42');
INSERT INTO `sys_logs_login` VALUES (1683111164150456321, 'admin', '127.0.0.1', '2023-07-23 21:45:47', 'MicroMessenger', '2023-07-23 21:45:46');
INSERT INTO `sys_logs_login` VALUES (1683111721007226882, 'admin', '127.0.0.1', '2023-07-23 21:48:00', 'MicroMessenger', '2023-07-23 21:47:59');
INSERT INTO `sys_logs_login` VALUES (1683112472739749890, 'admin', '127.0.0.1', '2023-07-23 21:50:59', 'MicroMessenger', '2023-07-23 21:50:58');
INSERT INTO `sys_logs_login` VALUES (1683115969786888193, 'admin', '127.0.0.1', '2023-07-23 22:04:53', 'MicroMessenger', '2023-07-23 22:04:52');
INSERT INTO `sys_logs_login` VALUES (1683115984215293953, 'admin', '127.0.0.1', '2023-07-23 22:04:56', 'MicroMessenger', '2023-07-23 22:04:56');
INSERT INTO `sys_logs_login` VALUES (1683115989378482177, 'admin', '127.0.0.1', '2023-07-23 22:04:57', 'MicroMessenger', '2023-07-23 22:04:57');
INSERT INTO `sys_logs_login` VALUES (1683116293092245505, 'admin', '127.0.0.1', '2023-07-23 22:06:10', 'MicroMessenger', '2023-07-23 22:06:09');
INSERT INTO `sys_logs_login` VALUES (1684148901917974530, 'admin', '127.0.0.1', '2023-07-26 18:29:23', 'MicroMessenger', '2023-07-26 18:29:22');
INSERT INTO `sys_logs_login` VALUES (1684150380137508865, 'admin', '127.0.0.1', '2023-07-26 18:35:15', 'MicroMessenger', '2023-07-26 18:35:15');
INSERT INTO `sys_logs_login` VALUES (1684152624958386178, 'admin', '127.0.0.1', '2023-07-26 18:44:10', 'MicroMessenger', '2023-07-26 18:44:10');
INSERT INTO `sys_logs_login` VALUES (1684154825479024642, '15555555556', '127.0.0.1', '2023-07-26 18:52:55', 'MicroMessenger', '2023-07-26 18:52:55');
INSERT INTO `sys_logs_login` VALUES (1684166367712899074, 'admin', '127.0.0.1', '2023-07-26 19:38:47', 'Chrome', '2023-07-26 19:38:47');
INSERT INTO `sys_logs_login` VALUES (1684173576320491521, 'admin', '127.0.0.1', '2023-07-26 20:07:26', 'Chrome', '2023-07-26 20:07:25');
INSERT INTO `sys_logs_login` VALUES (1684175463321083905, 'admin', '127.0.0.1', '2023-07-26 20:14:56', 'MicroMessenger', '2023-07-26 20:14:55');
INSERT INTO `sys_logs_login` VALUES (1684176048967581697, 'admin', '127.0.0.1', '2023-07-26 20:17:15', 'Chrome', '2023-07-26 20:17:15');
INSERT INTO `sys_logs_login` VALUES (1684176360814084098, 'admin', '127.0.0.1', '2023-07-26 20:18:30', 'MicroMessenger', '2023-07-26 20:18:29');
INSERT INTO `sys_logs_login` VALUES (1684177329467670530, 'admin', '127.0.0.1', '2023-07-26 20:22:20', 'Chrome', '2023-07-26 20:22:20');
INSERT INTO `sys_logs_login` VALUES (1684177727087616001, 'admin', '127.0.0.1', '2023-07-26 20:23:55', 'Chrome', '2023-07-26 20:23:55');
INSERT INTO `sys_logs_login` VALUES (1684179403890651138, 'admin', '127.0.0.1', '2023-07-26 20:30:35', 'Chrome', '2023-07-26 20:30:35');
INSERT INTO `sys_logs_login` VALUES (1684182365056692225, 'admin', '127.0.0.1', '2023-07-26 20:42:21', 'MicroMessenger', '2023-07-26 20:42:21');
INSERT INTO `sys_logs_login` VALUES (1684182560733556738, 'admin', '127.0.0.1', '2023-07-26 20:43:08', 'Chrome', '2023-07-26 20:43:07');
INSERT INTO `sys_logs_login` VALUES (1684183112863322114, 'admin', '127.0.0.1', '2023-07-26 20:45:19', 'Chrome', '2023-07-26 20:45:19');
INSERT INTO `sys_logs_login` VALUES (1684183143817285633, 'admin', '127.0.0.1', '2023-07-26 20:45:27', 'MicroMessenger', '2023-07-26 20:45:26');
INSERT INTO `sys_logs_login` VALUES (1684191305467322370, 'admin', '127.0.0.1', '2023-07-26 21:17:53', 'Chrome', '2023-07-26 21:17:52');
INSERT INTO `sys_logs_login` VALUES (1684192179770036226, 'admin', '127.0.0.1', '2023-07-26 21:21:21', 'Chrome', '2023-07-26 21:21:21');
INSERT INTO `sys_logs_login` VALUES (1684201315417124865, 'admin', '127.0.0.1', '2023-07-26 21:57:39', 'Chrome', '2023-07-26 21:57:39');
INSERT INTO `sys_logs_login` VALUES (1684205478804930561, 'admin', '127.0.0.1', '2023-07-26 22:14:12', 'Chrome', '2023-07-26 22:14:11');
INSERT INTO `sys_logs_login` VALUES (1684207357735682050, 'admin', '127.0.0.1', '2023-07-26 22:21:40', 'MicroMessenger', '2023-07-26 22:21:39');
INSERT INTO `sys_logs_login` VALUES (1684207383924916225, 'admin', '127.0.0.1', '2023-07-26 22:21:46', 'MicroMessenger', '2023-07-26 22:21:46');
INSERT INTO `sys_logs_login` VALUES (1684210151238631425, 'admin', '127.0.0.1', '2023-07-26 22:32:46', 'Chrome', '2023-07-26 22:32:45');
INSERT INTO `sys_logs_login` VALUES (1684210268008054785, 'admin', '127.0.0.1', '2023-07-26 22:33:14', 'MicroMessenger', '2023-07-26 22:33:13');
INSERT INTO `sys_logs_login` VALUES (1684527185281318914, 'admin', '127.0.0.1', '2023-07-27 19:32:33', 'Chrome', '2023-07-27 19:32:32');
INSERT INTO `sys_logs_login` VALUES (1684529482023469057, 'admin', '127.0.0.1', '2023-07-27 19:41:40', 'MicroMessenger', '2023-07-27 19:41:40');
INSERT INTO `sys_logs_login` VALUES (1684530514862469122, 'admin', '127.0.0.1', '2023-07-27 19:45:46', 'MicroMessenger', '2023-07-27 19:45:46');
INSERT INTO `sys_logs_login` VALUES (1684530819444416513, 'admin', '127.0.0.1', '2023-07-27 19:46:59', 'MicroMessenger', '2023-07-27 19:46:59');
INSERT INTO `sys_logs_login` VALUES (1684531176987860993, 'admin', '127.0.0.1', '2023-07-27 19:48:24', 'Chrome', '2023-07-27 19:48:24');
INSERT INTO `sys_logs_login` VALUES (1684532552119824386, 'admin', '127.0.0.1', '2023-07-27 19:53:52', 'Chrome', '2023-07-27 19:53:52');
INSERT INTO `sys_logs_login` VALUES (1684533234147229698, 'admin', '127.0.0.1', '2023-07-27 19:56:35', 'Chrome', '2023-07-27 19:56:34');
INSERT INTO `sys_logs_login` VALUES (1684534025717252097, 'admin', '127.0.0.1', '2023-07-27 19:59:44', 'MicroMessenger', '2023-07-27 19:59:43');
INSERT INTO `sys_logs_login` VALUES (1684537876058906625, 'admin', '127.0.0.1', '2023-07-27 20:15:01', 'MicroMessenger', '2023-07-27 20:15:01');
INSERT INTO `sys_logs_login` VALUES (1684538041389981697, 'admin', '127.0.0.1', '2023-07-27 20:15:41', 'MicroMessenger', '2023-07-27 20:15:40');
INSERT INTO `sys_logs_login` VALUES (1684538084117356546, 'admin', '127.0.0.1', '2023-07-27 20:15:51', 'Chrome', '2023-07-27 20:15:51');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `p_menu_id` bigint NULL DEFAULT NULL COMMENT '父权限id',
  `menu_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '权限名称',
  `url` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限路径',
  `menu_auth` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限标识',
  `is_hidden` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '是否隐藏:0否；1是',
  `menu_type` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单类型: 0目录 1菜单 2按钮',
  `show_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '展示类型: 第一栏,第二栏,第三栏',
  `menu_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `is_sort` int NULL DEFAULT NULL COMMENT '排序等级',
  `remark` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '权限描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '菜单表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, '系统管理', '/system', 'sys', '0', '0', '0', 'el-icon-s-home', 1, NULL, '2023-05-01 14:47:21', '2023-05-05 23:15:26', '0');
INSERT INTO `sys_menu` VALUES (2, 1, '用户管理', '/system/user', 'sys:user', '0', '1', '0', 'el-icon-user', 1, NULL, '2023-05-01 14:47:56', '2023-05-05 23:14:41', '0');
INSERT INTO `sys_menu` VALUES (3, 2, '新增', NULL, 'sys:user:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:48:27', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (4, 2, '修改', NULL, 'sys:user:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:48:43', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (5, 2, '删除', NULL, 'sys:user:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:00', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (6, 2, '导入', NULL, 'sys:user:import', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:38', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (7, 2, '导出', NULL, 'sys:user:export', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:49:51', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (8, 1, '角色管理', '/system/role', 'sys:role', '0', '1', '0', 'el-icon-bell', 2, NULL, '2023-05-01 14:50:21', '2023-05-05 23:15:40', '0');
INSERT INTO `sys_menu` VALUES (9, 8, '新增', NULL, 'sys:role:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:50:39', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (10, 8, '修改', NULL, 'sys:role:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:51:03', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (11, 8, '删除', NULL, 'sys:role:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:51:53', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (12, 1, '菜单管理', '/system/menu', 'sys:menu', '0', '1', '0', 'el-icon-s-data', 3, NULL, '2023-05-01 14:52:20', '2023-05-05 23:15:56', '0');
INSERT INTO `sys_menu` VALUES (13, 12, '新增', NULL, 'sys:menu:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:52:34', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (14, 12, '修改', NULL, 'sys:menu:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:52:53', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (15, 12, '删除', NULL, 'sys:menu:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-01 14:53:08', '2023-05-05 23:09:34', '0');
INSERT INTO `sys_menu` VALUES (16, 1, '部门管理', '/system/dept', 'sys:dept', '0', '1', '0', 'el-icon-money', 4, NULL, '2023-05-07 14:46:15', '2023-05-07 14:46:15', '0');
INSERT INTO `sys_menu` VALUES (17, 16, '新增', NULL, 'sys:dept:insert', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:19:36', '2023-05-08 22:24:35', '0');
INSERT INTO `sys_menu` VALUES (18, 16, '修改', NULL, 'sys:dept:update', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:20:00', '2023-05-08 22:24:33', '0');
INSERT INTO `sys_menu` VALUES (19, 16, '删除', NULL, 'sys:dept:delete', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:20:21', '2023-05-08 22:24:32', '0');
INSERT INTO `sys_menu` VALUES (20, 2, '查看', NULL, 'sys:user:select', '0', '2', '0', NULL, NULL, NULL, '2023-05-08 22:24:30', '2023-05-08 22:24:30', '0');
INSERT INTO `sys_menu` VALUES (21, 0, '日志管理', '/logs', 'sys:log', '0', '0', '0', 'el-icon-question', 2, NULL, '2023-06-02 22:23:22', '2023-06-02 22:23:22', '0');
INSERT INTO `sys_menu` VALUES (22, 21, '操作日志', '/logs/operate', 'logs:operate', '0', '1', '0', 'el-icon-info', 1, NULL, '2023-06-02 22:24:26', '2023-06-02 22:24:26', '0');
INSERT INTO `sys_menu` VALUES (23, 21, '登录日志', '/logs/login', 'logs:login', '0', '1', '0', 'el-icon-circle-plus', 2, NULL, '2023-06-02 22:37:18', '2023-06-02 22:37:18', '0');
INSERT INTO `sys_menu` VALUES (24, 0, '系统工具', '/tools', 'sys:tools', '0', '0', '1', 'el-icon-s-tools', 3, NULL, '2023-06-03 18:46:01', '2023-06-03 18:46:01', '0');
INSERT INTO `sys_menu` VALUES (25, 24, '代码生成', '/tools/gen', 'tools:gen', '0', '1', '1', 'el-icon-setting', 1, NULL, '2023-06-03 18:46:42', '2023-06-03 18:46:42', '0');
INSERT INTO `sys_menu` VALUES (26, 24, '修改配置', '/tools/gen/editTable', 'tools:table', '1', '1', '1', 'el-icon-video-camera-solid', 2, NULL, '2023-06-04 13:59:50', '2023-06-04 13:59:50', '0');
INSERT INTO `sys_menu` VALUES (27, 24, '字典管理', '/tools/dict', 'tools/dict', '0', '1', '1', 'el-icon-s-help', 3, NULL, '2023-06-06 20:40:29', '2023-06-06 20:40:29', '0');
INSERT INTO `sys_menu` VALUES (28, 24, '字典配置', '/tools/dict/data', 'dict:data', '1', '1', '1', 'el-icon-picture', 4, NULL, '2023-06-10 19:25:05', '2023-06-10 19:25:05', '0');
INSERT INTO `sys_menu` VALUES (30, 0, '定制业务', '/subject', 'sub:index', '0', '0', '1', 'el-icon-s-shop', 4, NULL, '2023-07-06 21:42:31', '2023-07-06 21:42:31', '0');
INSERT INTO `sys_menu` VALUES (31, 30, '业务模板', '/subject/subject', 'subject:subject', '0', '1', '1', 'el-icon-s-goods', 1, NULL, '2023-07-06 22:02:20', '2023-07-06 22:02:20', '0');
INSERT INTO `sys_menu` VALUES (32, 0, '消息管理', '/info', 'info:msg', '0', '0', '1', 'el-icon-phone', 5, NULL, '2023-07-26 19:39:56', '2023-07-26 19:39:56', '0');
INSERT INTO `sys_menu` VALUES (33, 32, '公告信息', '/info/notice', 'info:notice', '0', '1', '1', 'el-icon-s-order', 1, NULL, '2023-07-26 19:40:42', '2023-07-26 19:40:42', '0');
INSERT INTO `sys_menu` VALUES (34, 32, '留言消息', '/info/inform', 'info:inform', '0', '1', '1', 'el-icon-message-solid', 2, NULL, '2023-07-26 19:41:45', '2023-07-26 19:41:45', '0');
INSERT INTO `sys_menu` VALUES (35, 30, '评论列表', '/subject/comment', 'subject:comment', '1', '1', '1', 'el-icon-medal', 2, NULL, '2023-07-26 20:50:37', '2023-07-26 20:50:37', '0');
INSERT INTO `sys_menu` VALUES (36, 1, '个人中心', '/system/user/profile', 'user:profile', '0', '1', '0', 'el-icon-s-check', 5, NULL, '2023-07-27 20:06:18', '2023-07-27 20:06:18', '0');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '角色名称',
  `role_auth` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色权限标识',
  `is_sort` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '排序字段',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '角色描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '管理员', 'system', '1', '', '2023-05-01 14:46:19', '2023-05-04 22:26:06', '0');
INSERT INTO `sys_role` VALUES (2, '操作员', 'opera', '2', NULL, '2023-05-05 21:47:54', '2023-05-05 21:48:15', '0');
INSERT INTO `sys_role` VALUES (3, '审核员', 'check', '2', NULL, '2023-07-03 20:37:10', '2023-07-03 20:37:10', '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色id',
  `menu_id` bigint NOT NULL COMMENT '权限id',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '角色权限表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 19);
INSERT INTO `sys_role_menu` VALUES (1, 20);
INSERT INTO `sys_role_menu` VALUES (1, 21);
INSERT INTO `sys_role_menu` VALUES (1, 22);
INSERT INTO `sys_role_menu` VALUES (1, 23);
INSERT INTO `sys_role_menu` VALUES (1, 24);
INSERT INTO `sys_role_menu` VALUES (1, 25);
INSERT INTO `sys_role_menu` VALUES (1, 26);
INSERT INTO `sys_role_menu` VALUES (1, 27);
INSERT INTO `sys_role_menu` VALUES (1, 28);
INSERT INTO `sys_role_menu` VALUES (1, 30);
INSERT INTO `sys_role_menu` VALUES (1, 31);
INSERT INTO `sys_role_menu` VALUES (1, 32);
INSERT INTO `sys_role_menu` VALUES (1, 33);
INSERT INTO `sys_role_menu` VALUES (1, 34);
INSERT INTO `sys_role_menu` VALUES (1, 35);
INSERT INTO `sys_role_menu` VALUES (1, 36);
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 12);
INSERT INTO `sys_role_menu` VALUES (2, 13);
INSERT INTO `sys_role_menu` VALUES (2, 14);
INSERT INTO `sys_role_menu` VALUES (2, 15);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `password` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '姓名',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '头像',
  `telephone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '手机号',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `sex` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '性别：0:男；1:女',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `dept_id` int NULL DEFAULT NULL COMMENT '所属部门id',
  `is_use` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '1' COMMENT '是否启用：0未启用；1已启用',
  `is_delete` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '是否删除: 0未删除；1已删除',
  `remark` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin', '$2a$10$7MwTBtKeIDmSYoG40Zfdsu49ZQPwjKlI7qcLPjI1ij0CESLtanb4q', '张三', '/static/avatar.gif', '17388888882', NULL, '1', NULL, 2, '1', '0', NULL, '2023-05-01 13:53:20', '2023-07-27 20:15:51', '2023-07-23 20:06:51');
INSERT INTO `sys_user` VALUES (2, 'test', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '测试', '/static/avatar.gif', '17388888887', 22, '0', 'xx_2589@163.com', 1, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-05-03 18:10:13');
INSERT INTO `sys_user` VALUES (3, '333', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888888', 12, '1', 'xx_2589@163.com', 4, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-05-03 19:41:33');
INSERT INTO `sys_user` VALUES (4, '444', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888888', 12, '1', 'xx_2589@163.com', 2, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-05-03 22:28:45');
INSERT INTO `sys_user` VALUES (5, '555', '$2a$10$QQ5CDMURixB5hwVcvDzWFe.XijIS6GaAcHhvkgy3p2iY/B1nn/Wl6', '张三', '/static/avatar.gif', '17388888888', NULL, '1', NULL, 2, '1', '0', NULL, '2023-05-01 13:53:20', NULL, '2023-06-13 18:48:27');
INSERT INTO `sys_user` VALUES (6, '999', NULL, '李四', '/static/avatar.gif', '18999999989', NULL, '0', NULL, 3, '0', '0', NULL, '2023-05-03 21:17:57', NULL, '2023-06-13 18:48:30');
INSERT INTO `sys_user` VALUES (7, '15555555555', '$2a$10$I8xeLtgwpAHFRv/qxE8q5Ow93ZeS7VrAtznDttAcXPULNOmMYVIBG', '1690368361987', '/static/back.jpg', '15555555555', NULL, NULL, NULL, NULL, '1', '0', NULL, '2023-07-26 18:46:02', NULL, '2023-07-26 18:46:02');
INSERT INTO `sys_user` VALUES (8, '15555555556', '$2a$10$i73FCpqWObfgFki2c472KODoSXclKSW4B5hRDaQLGPENHnNi7wVj6', '1690368764190', '/static/avatar.gif', '15555555556', NULL, '0', NULL, NULL, '1', '0', NULL, '2023-07-26 18:52:44', '2023-07-26 18:52:55', '2023-07-26 22:06:51');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户id',
  `role_id` bigint NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 2);

-- ----------------------------
-- View structure for main_file_view
-- ----------------------------
DROP VIEW IF EXISTS `main_file_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `main_file_view` AS select `si`.`id` AS `id`,`sf`.`id` AS `file_id`,`sf`.`file_name` AS `file_name`,`sf`.`file_path` AS `file_path`,`sf`.`file_type` AS `file_type`,`sf`.`file_suffix` AS `file_suffix`,`sf`.`file_size` AS `file_size`,`sf`.`create_by` AS `create_by`,`sf`.`create_time` AS `create_time` from (`sys_index` `si` join `sys_file` `sf` on(((`si`.`id` = `sf`.`index_id`) and (`sf`.`file_type` = '0') and (`sf`.`is_delete` = '0'))));

SET FOREIGN_KEY_CHECKS = 1;
