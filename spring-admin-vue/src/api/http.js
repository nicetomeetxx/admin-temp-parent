import request from '@/utils/request'

/**
 * post请求
 * @param url 地址
 * @param data 数据
 * @returns {Promise<any>}
 */
const post = async function(url, data) {
  return await new Promise((resolve, reject) => {
    request.post(url, data)
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * get请求
 * @param url
 * @returns {Promise<any>}
 */
const get = async function(url) {
  return await new Promise((resolve, reject) => {
    request.get(url).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

/**
 * put请求
 * @param url
 * @param params
 * @returns {Promise<any>}
 */
const put = async function(url, params) {
  return await new Promise((resolve, reject) => {
    request.put(url, params)
      .then(res => {
        resolve(res)
      })
      .catch(err => {
        reject(err)
      })
  })
}

/**
 * delete请求
 * @param url
 * @param params
 * @returns {Promise<any>}
 * @constructor
 */
const del = async function(url, params) {
  return await new Promise((resolve, reject) => {
    request.delete(url, {
      data: params
    }).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}
/**
 * 根据id删除
 * @param url
 * @returns {Promise<unknown>}
 */
const deleteId = async function(url) {
  return await new Promise((resolve, reject) => {
    request.delete(url).then(res => {
      resolve(res)
    }).catch(err => {
      reject(err)
    })
  })
}

export default { post, get, put, del, deleteId }

