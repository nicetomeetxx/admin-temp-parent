import http from "@/api/http";
import download from "@/utils/download";

const subject = {
  //主题
  selectProSubjectAll: (data) => {
    return http.post('/subject/subject/page', data)
  },
  selectById: (id) => {
    return http.get('/subject/subject/' + id)
  },
  insert: (form) => {
    return http.post('/subject/subject', form)
  },
  update: (form) => {
    return http.put('/subject/subject', form)
  },
  deleteProSubjectBatch: (data) => {
    return http.del('/subject/subject', data)
  },
  exportProSubjectList: () => {
    return download.resource('/subject/subject/export')
  },
  //公告信息
  selectProNoticeAll: (data) => {
    return http.post('/wx/notice/page', data)
  },
  deleteProNoticeBatch: (data) => {
    return http.del('/wx/notice', data)
  },
  selectNoticeById: (id) => {
    return http.get('/wx/notice/' + id)
  },
  updateNotice: (form) => {
    return http.put('/wx/notice', form)
  },
  insertNotice: (form) => {
    return http.post('/wx/notice', form)
  },
  //消息通知
  selectProInformAll: (data) => {
    return http.post('/wx/info/page', data)
  },
  deleteProInformBatch: (data) => {
    return http.del('/wx/info', data)
  },
  //评论列表
  selectProRecordAll: (data) => {
    return http.post('/wx/like/page', data)
  },
  //回复评论
  replyRecord: (data) => {
    return http.post('/wx/like/reply', data)
  },
  deleteProRecordBatch: (data) => {
    return http.del('/wx/like', data)
  },
  //查询时间列表
  selectIntervalList() {
    return http.get('/interval/page')
  },
  //查询时间详情
  selectIntervalById: (id) => {
    return http.get('/interval/' + id)
  },
  deleteUniIntervalBatch: (data) => {
    return http.del('/interval', data)
  },
  //新增时间
  insertInterval: (data) => {
    return http.post('/interval', data)
  },
  //修改预约时间
  updateInterval: (data) => {
    return http.put('/interval', data)
  },
  //搜索历史
  selectUniIntervalHis: (data) => {
    return http.post('/interval/history', data)
  },
  //查询数量
  getNoticeNum: () => {
    return http.get('/wx/notice/num')
  }
}

export default subject
