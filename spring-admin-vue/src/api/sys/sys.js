import request from '@/utils/request'
import http from '@/api/http'
import download from '@/utils/download'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/info',
    method: 'get',
    params: {token}
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

const sys = {
  // 用户模块
  selectUserAll: (data) => {
    return http.post('/sysUser/page', data)
  },
  selectUserById: (id) => {
    return http.get('/sysUser/' + id)
  },
  insertUser: (form) => {
    return http.post('/sysUser', form)
  },
  updateUser: (form) => {
    return http.put('/sysUser', form)
  },
  updatePassword: (form) => {
    return http.put('/sysUser/password', form)
  },
  deleteUserById: (id) => {
    return http.deleteId('/sysUser/' + id)
  },
  deleteUserBatch: (data) => {
    return http.del('/sysUser', data)
  },
  selectDeptTree: (data) => {
    return http.post('/sysDept/tree', data)
  },
  grantRoleByUserId: (data) => {
    return http.post('/sysUser/grant', data)
  },
  // 角色模块
  selectRoleAll: (data) => {
    return http.post('/sysRole/page', data)
  },
  selectRoleById: (id) => {
    return http.get('/sysRole/' + id)
  },
  insertRole: (form) => {
    return http.post('/sysRole', form)
  },
  updateRole: (form) => {
    return http.put('/sysRole', form)
  },
  deleteRoleById: (id) => {
    return http.deleteId('/sysRole/' + id)
  },
  deleteRoleBatch: (data) => {
    return http.del('/sysRole', data)
  },
  grantMenuByRoleId: (data) => {
    return http.post('/sysRole/grant', data)
  },
  // 菜单模块
  selectMenuAll: (data) => {
    return http.post('/sysMenu/page', data)
  },
  selectMenuById: (id) => {
    return http.get('/sysMenu/' + id)
  },
  insertMenu: (form) => {
    return http.post('/sysMenu', form)
  },
  updateMenu: (form) => {
    return http.put('/sysMenu', form)
  },
  deleteMenuById: (id) => {
    return http.deleteId('/sysMenu/' + id)
  },
  deleteMenuBatch: (data) => {
    return http.del('/sysMenu', data)
  },
  selectDeptHidden: () => {
    return http.get('/sysMenu/dept')
  },
  // 部门管理
  selectDeptAll: (data) => {
    return http.post('/sysDept/page', data)
  },
  selectDeptById: (id) => {
    return http.get('/sysDept/' + id)
  },
  insertDept: (form) => {
    return http.post('/sysDept', form)
  },
  updateDept: (form) => {
    return http.put('/sysDept', form)
  },
  // 导入导出
  exportUserTemplate: () => {
    return download.resource('/sysUser/exportTemplate')
  },
  exportUserList: () => {
    return download.resource('/sysUser/export')
  },
  getUserNumber: () => {
    return http.get('/sysUser/number')
  },
  selectIndex: () => {
    return http.get('/sys/index')
  },
  uploadFile: (url, file) => {
    return http.post(url, file)
  }
}

export default sys
