import request from '@/utils/request'


// 查询字典数据列表
export function listData(query) {
  return request({
    url: '/sys/dictData/page',
    method: 'post',
    data: query
  })
}

// 查询字典数据详细
export function getData(dictCode) {
  return request({
    url: '/sys/dictData/' + dictCode,
    method: 'get'
  })
}

// 根据字典类型查询字典数据信息
export function getDict(dictType) {
  return request({
    url: '/sys/dictData/type/' + dictType,
    method: 'get'
  })
}

// 新增字典数据
export function addData(data) {
  return request({
    url: '/sys/dictData',
    method: 'post',
    data: data
  })
}

// 修改字典数据
export function updateData(data) {
  return request({
    url: 'sys/dictData',
    method: 'put',
    data: data
  })
}

// 删除字典数据
export function delData(dictCode) {
  return request({
    url: '/sys/dictData/' + dictCode,
    method: 'delete'
  })
}

// 查询字典类型列表
export function listType(data) {
  return request({
    url: 'sys/dictType/page',
    method: 'post',
    data: data
  })
}

// 查询字典类型详细
export function getType(dictId) {
  return request({
    url: '/sys/dictType/' + dictId,
    method: 'get'
  })
}

// 新增字典类型
export function addType(data) {
  return request({
    url: '/sys/dictType',
    method: 'post',
    data: data
  })
}

// 修改字典类型
export function updateType(data) {
  return request({
    url: '/sys/dictType',
    method: 'put',
    data: data
  })
}

// 删除字典类型
export function delType(dictId) {
  return request({
    url: '/sys/dictType/' + dictId,
    method: 'delete'
  })
}


// 获取字典选择框列表
export function optionSelect() {
  return request({
    url: '/sys/dictType/optionSelect',
    method: 'get'
  })
}

