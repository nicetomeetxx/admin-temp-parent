import http from "@/api/http";
import download from "@/utils/download";

const logs = {
  selectSysLogsAll: (data) => {
    return http.post('/logs/logs/page', data)
  },
  deleteSysLogsBatch: (data) => {
    return http.del('/logs/logs', data)
  },
  exportSysLogsList: (data) => {
    return http.post('/logs/logs/export', data)
  },
  selectById: (id) => {
    return http.get('/logs/logs/' + id)
  },
  update: (data) => {
    return http.put('/logs/logs', data)
  },
  insert: (data) => {
    return http.post('/logs/logs', data)
  },
  selectLoginAll:(data) =>{
    return http.post('/logs/login/page', data)
  }
}

export default logs
