import http from "@/api/http";

const test = {
  insert: (data) => {
    return http.post('/work/teach', data)
  },
  selectById: (id) => {
    return http.get('/work/teach/' + id)
  },
  update: (data) => {
    return http.put('/work/teach', data)
  },
  selectTypeTeachAll:(data)=> {
    return http.post('/work/teach/page', data)
  },
  deleteTypeTeachBatch:(data)=> {
    return http.del('/work/teach', data)
  }
}

export default test
