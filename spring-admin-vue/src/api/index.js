import sys from '@/api/sys/sys'
import file from '@/api/sys/file'
import logs from '@/api/sys/logs'
import subject from '@/api/sys/sub'
import test from '@/api/test/test'

export default {sys, file, logs, subject, test}
