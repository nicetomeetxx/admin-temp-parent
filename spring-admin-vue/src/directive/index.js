import hasRole from './permission/hasRole'
import hasAuth from './permission/hasAuth'


const install = function (Vue) {
  Vue.directive('Role', hasRole)
  Vue.directive('Auth', hasAuth)
}

if (window.Vue) {
  window['Role'] = hasRole
  window['Auth'] = hasAuth
  Vue.use(install);
}

export default install
