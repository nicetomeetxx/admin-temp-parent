import Vue from 'vue'
import DataDict from '@/utils/dict'
import { getDict as getDictList } from '@/api/sys/dict'

function install() {
  Vue.use(DataDict, {
    metas: {
      '*': {
        labelField: 'dictLabel',
        valueField: 'dictValue',
        request(dictMeta) {
          return getDictList(dictMeta.type).then(res => res.data)
        },
      },
    },
  })
}

export default {
  install,
}
