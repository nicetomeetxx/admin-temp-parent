const g_object_name_type = 'local_name'

// init里获取文件名
export function get_suffix(filename) {
  var pos = filename.lastIndexOf('.')
  var suffix = ''
  if (pos !== -1) {
    suffix = filename.substring(pos)
  }
  return suffix
}

export function calculate_object_name(filename, path) {
  if (g_object_name_type === 'local_name') {
    // eslint-disable-next-line no-unused-vars
    var g_object_name = path
    g_object_name += '${filename}'
  } else if (g_object_name_type === 'random_name') {
    var suffix = this.get_suffix(filename)
    g_object_name = path + this.random_string(10) + suffix
  }
  return ''
}

export function random_string(len) {
  len = len || 32
  var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
  var maxPos = chars.length
  var pwd = ''
  for (var i = 0; i < len; i++) {
    pwd += chars.charAt(Math.floor(Math.random() * maxPos))
  }
  return pwd
}

// 获取上传文件的文件名
export function get_uploaded_object_name(filename) {
  if (g_object_name_type === 'local_name') {
    var tmp_name = filename
    tmp_name = tmp_name.replace('${filename}', filename)
    return tmp_name
  } else if (g_object_name_type === 'random_name') {
    return filename
  }
}
