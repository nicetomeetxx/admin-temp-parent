import CryptoJS from 'crypto-js'

const AESKey = '1qazZSE$$ESZzaq1' // 此处为密钥

// AES加密
export function encryptByAES(data) {
  const key = CryptoJS.enc.Utf8.parse(AESKey)
  const cipherText = CryptoJS.AES.encrypt(data, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  return cipherText.toString()
}

// AES解密
export function decryptedByAES(data) {
  const key = CryptoJS.enc.Utf8.parse(AESKey)
  const decryptedData = CryptoJS.AES.decrypt(data, key, {
    mode: CryptoJS.mode.ECB,
    padding: CryptoJS.pad.Pkcs7
  })
  return decryptedData.toString(CryptoJS.enc.Utf8)
}
