import axios from 'axios'
import { saveAs } from 'file-saver'
import { getToken } from '@/utils/auth'

const baseURL = process.env.VUE_APP_BASE_API

export default {
  name(name, isDelete = true) {
    var url = baseURL + '/common/download?fileName=' + encodeURI(name) + '&delete=' + isDelete
    axios({
      method: 'get',
      url: url,
      responseType: 'blob',
      headers: {
        'X-Token': getToken()
      }
    }).then(async(res) => {
      const isLogin = await blobValidate(res.data)
      if (isLogin) {
        const blob = new Blob([res.data])
        this.saveAs(blob, decodeURI(res.headers['download-filename']))
      }
    })
  },
  // 下载资源文件，目前excel用的这个
  resource(resource, data) {
    var url = baseURL + resource
    axios({
      method: 'post',
      url: url,
      responseType: 'blob',
      headers: {
        'X-Token': getToken()
      },
      data: data
    }).then(async(res) => {
      const isLogin = await blobValidate(res.data)
      if (isLogin) {
        const blob = new Blob([res.data])
        this.saveAs(blob, decodeURI(res.headers['content-disposition'].split('=')[1]))
      } else {
        this.$modal.msgSuccess('下载文件错误')
      }
    })
  },
  zip(url, name) {
    url = baseURL + url
    axios({
      method: 'get',
      url: url,
      responseType: 'blob',
      headers: {
        'X-Token': getToken()
      }
    }).then(async(res) => {
      const isLogin = await blobValidate(res.data)
      if (isLogin) {
        const blob = new Blob([res.data], { type: 'application/zip' })
        this.saveAs(blob, name)
      } else {
        this.$modal.msgSuccess('下载文件错误')
      }
    })
  },
  // 下载文件
  saveAs(text, name, opts) {
    saveAs(text, name, opts)
  }
}

// 验证是否为blob格式
export async function blobValidate(data) {
  try {
    const text = await data.text()
    JSON.parse(text)
    return false
  } catch (error) {
    return true
  }
}
