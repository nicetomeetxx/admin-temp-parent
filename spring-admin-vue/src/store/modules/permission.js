import {constantRoutes} from '@/router'
import Layout from '@/layout'

/**
 * Use meta.role to determine if the current sys has permission
 * @param auth
 * @param route
 */
function hasPermission(auth, route) {
  if (route.meta && route.meta.roles) {
    return auth.some(role => route.meta.roles.includes(role))
  } else {
    return true
  }
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param auth
 */
export function filterAsyncRoutes(routes, auth) {
  const res = []
  routes.forEach(route => {
    const tmp = {...route}
    if (hasPermission(auth, tmp)) {
      // 动态找到页面路径
      const component = tmp.component
      if (route.component) {
        // 判断是否是一级菜单
        if (component === 'Layout') {
          tmp.component = Layout
        } else {
          tmp.component = (resolve) => require([`@/views${component}`], resolve)
        }
      }
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, auth)
      }
      res.push(tmp)
    }
  })

  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes
    state.routes = constantRoutes.concat(routes)
  }
}

const actions = {
  generateRoutes({commit}, {routes, auth}) {
    return new Promise(resolve => {
      let accessedRoutes = filterAsyncRoutes(routes, auth)
      commit('SET_ROUTES', accessedRoutes)
      resolve(accessedRoutes)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
