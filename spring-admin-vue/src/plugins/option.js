import elementIcons from '../icons/element-icons'

export default {
  elementIcon: elementIcons,
  option: [{
    value: 0,
    label: '禁用'
  }, {
    value: 1,
    label: '启用'
  }],

  useOption: [{
    value: '0',
    label: '禁用'
  }, {
    value: '1',
    label: '启用'
  }],

  statusOptions: [{
    value: 0,
    label: '正常'
  }, {
    value: 1,
    label: '暂停'
  }],

  startStatusOptions: [{
    value: 0,
    label: '成功'
  }, {
    value: 1,
    label: '失败'
  }],

  sexOptions: [{
    value: '0',
    label: '男'
  }, {
    value: '1',
    label: '女'
  }],

  trueOptions: [{
    value: '0',
    label: '否'
  }, {
    value: '1',
    label: '是'
  }],

  menuOptions: [{
    value: 0,
    label: '目录'
  }, {
    value: 1,
    label: '菜单'
  }],

  showOptions: [{
    value: '0',
    label: '用户管理'
  }, {
    value: '1',
    label: '系统设置'
  }, {
    value: '2',
    label: '流程中心'
  }],
  appointOptions: [{
    value: '0',
    label: '已预约'
  }, {
    value: '1',
    label: '已完成'
  }, {
    value: '2',
    label: '已取消'
  }]
}
