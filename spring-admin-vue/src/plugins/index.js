import cache from './cache'
import modal from './modal'
import api from '@/api'
import option from './option'
import verify from './verify'
import param from './param'
import tab from './tab'

export default {
  install(Vue) {
    // 查询接口js
    Vue.prototype.$api = api
    // 缓存对象
    Vue.prototype.$cache = cache
    // 模态框对象
    Vue.prototype.$modal = modal
    // 常用枚举值
    Vue.prototype.$option = option
    // 表单校验
    Vue.prototype.$verify = verify
    // 分页参数
    Vue.prototype.$param = param
    // 导航栏
    Vue.prototype.$tab = tab
  }
}
