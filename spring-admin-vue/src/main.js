import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/display.css'
import './styles/element-variables.scss'
import '../src/styles/iconfont.css' // 需要网络

import '@/styles/index.scss' // global css
import directive from './directive' // directive
import App from './App'
import store from './store'
import router from './router'

import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log
import plugins from './plugins'

import * as filters from './filters' // global filters
// 按需引入
import * as echarts from 'echarts'
// 字典数据组件
import DictData from '@/components/DictData'
// 字典标签
import DictTag from '@/components/DictTag'
// 显示隐藏
import XuTooltip from '@/components/XuTooltip'
// 分页插件
import xuPagination from '@/components/XuPagination'
// 富文本组件
import Editor from "@/components/Editor"
// 文件上传组件
import FileUpload from "@/components/FileUpload"
// 图片上传组件
import ImageUpload from "@/components/ImageUpload"
// 图片预览组件
import ImagePreview from "@/components/ImagePreview"

Vue.prototype.$echarts = echarts
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// 挂载组件
Vue.component('DictTag', DictTag)
Vue.component('XuTooltip', XuTooltip)
Vue.component('Editor', Editor)
// Vue.component('FileUpload', FileUpload)
Vue.component('ImageUpload', ImageUpload)
// Vue.component('ImagePreview', ImagePreview)
Vue.component('xuPagination', xuPagination)

Vue.use(directive)
Vue.use(plugins)
Vue.use(Element, {
  size: Cookies.get('size') || 'small' // set element-ui default size
})
DictData.install()

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// 取消非生产环境浏览器提示
Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
