## 开源后台管理系统加小程序模板

1. 集点赞，评论，收藏，预约，购买，代码生成于一体
2. 完善的用户角色RBAC后台管理系统，自定义菜单按钮权限
3. 对计算机大四学生：可快速构建属于自己的B业设计，协同过滤算法、markdown、点赞收藏等完美契合老师意图
4. 对sideline-JAVAer：基于ruoyi代码生成器快速构建前后台，B设管理系统博客系统商城系统快速构建，代码拿来即用

### 技术栈：

```
 Springboot3.0、mybatisPlus、satoken、ruoyi-generator、easyPoi、hutools
 vue2、vue-admin-templete
 uniapp、tuiniaoUI    
```

<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue01.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue02.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue03.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue04.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue05.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue06.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue07.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue08.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue09.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue10.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue11.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue12.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue13.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue14.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/vue15.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp01.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp02.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp03.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp04.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp05.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp06.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp07.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp08.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp09.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp10.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp11.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp12.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp13.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp14.png"></img>
<img src="https://xuadmin.oss-cn-shanghai.aliyuncs.com/uniapp/uniapp15.png"></img>